import itertools
from abc import abstractmethod
from functools import partial
from itertools import chain, takewhile
from logging import getLogger as get_logger
from typing import TypeVar, Generic, Type, get_type_hints, Mapping, Union, Any, List, get_origin, get_args, \
    Sequence, Tuple, Dict, Iterable, Set, Collection, Optional, Callable

from . import codecs
from .util import invert, ensure_byte

T = TypeVar('T')
log = get_logger(__name__)


class Field(Generic[T]):
    """
    A field is a descriptor attribute of a type that can be (de)serialized from/to a section of bytes.

    The field is responsible to determine the correct offset in a large byte array when reading and writing.
    Encoding and decoding are typically delegated to a `snesedit.codecs.Codec`
    """
    __slots__ = ("_name", "_owner")

    def __init__(self):
        self._name = None
        self._owner = None

    def __set_name__(self, owner, name):
        self._name = name
        self._owner = owner

    @property
    def name(self) -> str:
        """
        :return: the name of this field as defined in the declaring type
        """
        return self._name

    @property
    def owner(self) -> Type:
        """
        :return: the class that declared this field
        """
        return self._owner

    @property
    def annotation(self) -> Optional[Type[T]]:
        """
        :return: the type hint  with which the field is annotated in its defining class
        """
        return get_type_hints(self._owner).get(self._name)

    @abstractmethod
    def load(self, data: bytearray, instance: object) -> T:
        """
        Load the attribute value for an instance of the owner type from a source of bytes
        and decode it to annotated runtime type. The field is responsible for determining
        the correct offset(s) in the data

        **Note**: This method should not modify the instance unless this is a data descriptor.
                  Setting the attribute on instance is explicitly done outside of this method.

        :param data:        the source bytes from
        :param instance:    an instance of the declaring type for which a value is read
        :return:            the designated attribute value for this field in `instance`
        """
        ...

    @abstractmethod
    def save(self, data: bytearray, instance: object):
        """
        Write the attribute value for this field in `instance` to the target byte sequence.

        The field is responsible for determining the correct offset(s) in the data. the attribute value to write
        should be read by getattr(instance, self.name) unless this field is a data-descriptor

        :param data:         target sequence to write the value to
        :param instance:     instance holding the value to write
        """
        ...


def get_fields(cls: Type) -> Mapping[str, Field]:
    """
    inspect a class for `Field` descriptors.

    :param cls: type to inspect
    :return:    a dictionary with all field names and associated field descriptors in type `cls.
                Does not include inherited fields
    """
    return {name: attr for name, attr in cls.__dict__.items()
            if isinstance(attr, Field)}


def get_field_values(instance: Any) -> Mapping[str, Any]:
    return {name: getattr(instance, name) for name in get_fields(type(instance))}


def load_instance(data: bytearray, instance: T) -> T:
    for name, field in get_fields(type(instance)).items():
        try:
            value = field.load(data, instance)
        except ValueError as v:
            raise ValueError('failed to load field ' + field.name + ': ' + str(v))
        log.debug("loaded attribute '%s' for instance %s, value = %s", name, instance, value)
        if not hasattr(field, '__set__') and not hasattr(field, '__get__'):
            setattr(instance, name, value)
    return instance


def save_instance(data: bytearray, instance: object) -> bool:
    modified = False
    for name, field in get_fields(type(instance)).items():
        current_value = getattr(instance, name)
        written_value = field.load(data, instance)
        if current_value != written_value:
            log.debug("field %s has changed in %s, saving", name, instance)
            field.save(data, instance)
            modified = True
        else:
            log.debug("field %s unchanged in %s, skipping", name, instance)
    return modified


class Seekable:

    # noqa F301
    # noinspection PyMethodMayBeStatic
    def __seek__(self, offset: int, attribute_name: str = None) -> int:
        return offset  # pragma: no cover


def get_slice(offset: int, size: int, instance: object, name: str):
    if isinstance(instance, Seekable):
        start = instance.__seek__(offset, name)
    else:
        start = offset
    return slice(start, start + size)


class SimpleField(Field[T], codecs.Codec[T]):
    __slots__ = ("_offset", "_size", "_codec")

    def __init__(self, offset: int, size: int, codec: codecs.Codec[T] = None):
        super().__init__()
        self._offset = offset
        self._size = size
        self._codec = codec

    def encode_to_bytes(self, value: T) -> bytes:
        if self._codec is None:
            raise NotImplementedError
        return self._codec.encode_to_bytes(value)

    def decode_from_bytes(self, source: bytes) -> T:
        if self._codec is None:
            raise NotImplementedError
        return self._codec.decode_from_bytes(source)

    @property
    def offset(self) -> int:
        return self._offset

    @property
    def size(self) -> int:
        return self._size

    @property
    def codec(self) -> codecs.Codec:
        return self._codec if self._codec is not None else self

    def _slice(self, instance: object):
        return get_slice(self.offset, self.size, instance, self.name)

    def load_slice(self, data: bytearray, instance: object) -> bytes:
        slc = self._slice(instance)
        return bytes(data[slc])

    def load(self, data: bytearray, instance: object) -> T:
        bites = self.load_slice(data, instance)
        return self.decode_from_bytes(bites)

    def save(self, data: bytearray, instance: object):
        value = getattr(instance, self.name)
        if value == self.load(data, instance):
            log.debug("field %s in type %s is unchanged, not saving", self.name, self.owner)
            return False
        try:
            bites = self.encode_to_bytes(value)
        except NotImplementedError:
            log.debug("ignoring read-only field %s in type %s", self.name, self.owner)
            return False
        if not isinstance(bites, bytes):
            raise TypeError("expected a single slice of bytes for field %s in type %s" % (self.name, self.owner))
        data[self._slice(instance)] = bites
        return True


Byte = partial(SimpleField, size=1, codec=codecs.byte)
Short = partial(SimpleField, size=2, codec=codecs.short)
Int24 = partial(SimpleField, size=3, codec=codecs.int24)
Integer = partial(SimpleField, size=4, codec=codecs.integer)


class ByteArray(SimpleField[bytearray]):

    def __init__(self, offset: int, length: int):
        super().__init__(offset=offset, size=length, codec=codecs.ByteArray())

    def __len__(self):
        return self.size


def _numeric_array(offset: int, size: int, length: int) -> SimpleField[list[int]]:
    return SimpleField(offset=offset, size=size * length, codec=codecs.NumericArrayCodec(width=size))


# Rework: nicer shortcuts

ShortArray = partial(_numeric_array, size=2)
Int24Array = partial(_numeric_array, size=3)
IntArray = partial(_numeric_array, size=4)


class HalfByteArray(SimpleField[List[int]]):

    def __init__(self, offset: int, length: int):
        if length % 2:
            raise ValueError('length of HalfByteArray must be even')
        super().__init__(offset, size=length // 2, codec=codecs.HalfByteCodec())


class SquareEnixInt(SimpleField):
    """
    A weird numeric type used in some Square/Enix RPGs.

    It stores a 2-byte short value and an additional offset with multiples of 50,000. The second byte
    of the 4-byte sequence is not used
    """

    def __init__(self, offset: int):
        super().__init__(offset=offset, size=4, codec=None)

    def decode_from_bytes(self, source: bytes) -> int:
        return 50000 * source[0] + (source[3] << 8) + source[2]

    def encode_to_bytes(self, value: int) -> bytes:
        weird, remainder = divmod(value, 50000)
        return bytes([weird, 0, (remainder & 0xff), (remainder >> 8)])


class Array(SimpleField[List[T]]):
    __slots__ = ("_chunk_size", "_length")

    def __init__(self, offset: int, length: int, chunk_size: int, codec: codecs.Codec[T]):
        super().__init__(offset=offset, size=length * chunk_size, codec=codec)
        self._length = length
        self._chunk_size = chunk_size
        if codec is None:
            raise ValueError("codec is required for Array fields")

    def encode_to_bytes(self, value: List[T]) -> bytes:
        chunks = (chain.from_iterable(self._codec.encode_to_bytes(v) for v in value))
        return bytes(list(chunks))

    def decode_from_bytes(self, source: bytes) -> List[T]:
        return [self._codec.decode_from_bytes(chunk)
                for chunk in codecs.chunk_bytes(source, self._chunk_size)]


class ArrayElement(Seekable):

    def __init__(self, index: int, step: int, parent: Any):
        self._index = index
        self._step = step
        self._parent = parent

    def __eq__(self, other):
        if not isinstance(other, ArrayElement):
            return False
        return all((self._index == other._index,
                    self._parent == other._parent,
                    self._step == other._step,
                    get_field_values(self) == get_field_values(other)))

    def __repr__(self):
        return f"{self.__class__.__name__}(index={self.index})"

    def __hash__(self):
        return hash((self._parent, self._index, self._step, get_field_values(self).values()))

    def __seek__(self, offset: int, attribute_name: str = None) -> int:
        return offset + self._index * self._step

    @property
    def index(self) -> int:
        return self._index

    @property
    def parent(self):
        return self._parent

    @classmethod
    def array(cls, length: int, step: int):
        return ComplexArray(length, step, cls)


AE = TypeVar('AE', bound=ArrayElement, covariant=True)
E = TypeVar('E')


class DenseArrayElement(ArrayElement):
    """An Array Element where data is not stored as a struct, but as a set of parallel arrays"""
    
    def __init_subclass__(cls, **kwargs):
        fields = get_fields(cls)
        for f in fields.values():
            if not hasattr(f, 'size'):                
                raise TypeError('DenseArrayElement only supports fields with a defined size attribute')
    
    def __seek__(self, offset: int, attribute_name: str = None) -> int:
        field = getattr(self.__class__, attribute_name)
        size = getattr(field, 'size')
        return offset + self.index * size
                

class ComplexArray(Field[List[AE]]):
    __slots__ = ("_offset", "_length", "_step", "_element_type")

    def __init__(self, length: int, step: int, element_type: Type[AE] = None):
        super().__init__()
        self._length = length
        self._step = step
        self._element_type = element_type

    @property
    def element_type(self):
        if self._element_type is None:
            if get_origin(self.annotation) != list:
                raise TypeError("field %s in type %s must be annotated as list" % (self.name, self.annotation))
            type_args = get_args(self.annotation)
            if not type_args or not issubclass(type_args[0], ArrayElement):
                raise TypeError("field %s in type %s must be annotated as generic list "
                                "of a subtype of ArrayElement" % (self.name, self.annotation))
            self._element_type = type_args[0]
        return self._element_type

    def _load_element(self, data: bytearray, index: int, parent: object) -> AE:
        return load_instance(data, self.element_type(index=index, step=self._step, parent=parent))

    def load(self, data: bytearray, instance: object) -> List[AE]:
        return [self._load_element(data, i, instance) for i in range(self._length)]

    def save(self, data: bytearray, instance: object):
        value = getattr(instance, self.name)  # type: list[AE]
        for e in value:
            save_instance(data, e)


class BitSet(SimpleField[Set[E]]):
    __slots__ = "_elements",

    def __init__(self, offset: int, elements: List[E], size: int = None):
        if size is None:
            size, remainder = divmod(len(elements), 8)
            if remainder:
                size += 1
        super().__init__(offset=offset, size=size, codec=None)
        self._elements = elements

    def encode_to_bytes(self, value: Set[E]) -> bytes:
        result = [0] * self.size
        for i, name in enumerate(self._elements):
            if name in value:
                byte_index, flag_index = divmod(i, 8)
                flag_value = 2 ** flag_index
                result[byte_index] |= flag_value
        return bytes(result)

    def decode_from_bytes(self, source: bytes) -> Set[E]:

        def iter_bits():
            for i, name in enumerate(self._elements):
                byte_index, flag_index = divmod(i, 8)
                flag_value = 2 ** flag_index
                if source[byte_index] & flag_value:
                    yield name

        return set(iter_bits())


class ParentField:
    __slots__ = "_parent_name",

    def __init__(self, parent_name: str = None):
        self._parent_name = parent_name

    def __set_name__(self, owner, name):
        if not issubclass(owner, ArrayElement):
            raise TypeError('ParentField can only be used in subclasses of ArrayElement')  # pragma: no cover
        if self._parent_name is None:
            self._parent_name = name + "s"

    def __get__(self, instance: ArrayElement, owner):
        if instance is None:
            return self
        return getattr(instance.parent, self._parent_name)[instance.index]

    def __set__(self, instance: ArrayElement, value):
        getattr(instance.parent, self._parent_name)[instance.index] = value


class SimpleList(SimpleField[List[E]]):
    __slots__ = ('_elements', '_nothing', '_empty_slot')

    def __init__(self, offset: int, elements: List[E], nothing: E = None, length: int = None,
                 empty_slot: int = None):
        if length is None:
            length = len(elements)
        super().__init__(offset, size=length, codec=None)
        self._elements = elements
        self._nothing = elements[0] if nothing is None else nothing
        if self._nothing not in elements:
            raise ValueError(f'nothing-value: {nothing} not found in elements')

    def encode_to_bytes(self, value: Collection[E]) -> bytes:
        return bytes([self._elements.index(v) for v in value])

    def decode_from_bytes(self, source: bytes) -> Collection[E]:
        id0 = self._elements.index(self._nothing)
        return [self._elements[b] for b in source if b != id0]


class SimpleTable(SimpleField[Sequence[E]]):
    __slots__ = ('_elements', '_nothing', '_inverted', '_bytes_per_slot', '_empty_slot', '_factory')

    codec: codecs.NumericArrayCodec

    def __init__(self, offset: int, elements: Mapping[E, int], nothing: E = None, bytes_per_slot=1, length: int = None,
                 empty_slot: Optional[int] = 0, factory: Callable = list):
        self._inverted = invert(elements)
        codec = codecs.get_array_codec(bytes_per_slot)
        if length is None:
            length = len(elements)
        super().__init__(offset=offset, size=int(length * bytes_per_slot), codec=codec)
        if nothing is None and empty_slot is None:
            raise ValueError("either nothing or empty slot must be given")
        elif nothing is None:
            # if empty_slot not in self._inverted:
            #    raise ValueError(f"found no mapping for empty slot ({empty_slot})")
            nothing = self._inverted.get(empty_slot)
        elif empty_slot is None:
            if nothing not in elements:
                raise ValueError(f"'nothing' element {nothing} not found in mapping")
        self._elements = elements
        self._nothing = nothing
        self._empty_slot = empty_slot
        self._factory = factory

    @property
    def bytes_per_slot(self) -> int:
        return self.codec.width

    def encode_to_bytes(self, value: Iterable[E]) -> bytes:
        slots = self.codec.encode_to_bytes([self._elements[v] for v in value])
        return codecs.pad(slots, size=self.size, padding_value=self._empty_slot)

    def decode_from_bytes(self, source: bytes) -> Iterable[E]:
        indices = self.codec.decode_from_bytes(source)
        return self._factory((self._inverted[b] for b in indices if b != self._empty_slot))


class Mapped(SimpleField[E]):
    __slots__ = ('_elements', '_nothing', '_inverted')

    def __init__(self, offset: int, elements: Union[Sequence[E], Mapping[E, int]], nothing: int = 0xff,
                 bytes_per_slot=1):
        super().__init__(offset, size=bytes_per_slot, codec=None)
        if isinstance(elements, Mapping):
            self._elements = elements
        elif isinstance(elements, Sequence):
            self._elements = {e: i for i, e in enumerate(elements)}
        else:
            raise TypeError('elements must be a sequence or mapping')

        self._nothing = nothing
        self._inverted = invert(self._elements, bijective=True)
        if len(self._inverted) != len(elements):
            raise ValueError("elements must be a bijective mapping or a sequence without duplicates")

    def encode_to_bytes(self, value: E) -> bytes:
        index = self._elements.get(value, self._nothing)
        return codecs.encode_numeric(index, self.size)

    def decode_from_bytes(self, source: bytes) -> E:
        index = codecs.decode_numeric(source, self.size)
        return self._inverted.get(index, None)


class PositionalInventory(SimpleField[Dict[str, int]]):
    """
    A very basic mapping type. each position (index) in a byte range corresponds to a key.

    The value of each byte is the corresponding value in the mapping. indices a labeled by a list of strings.

    the strings must be unique, and the number of bytes read matches the length of keys.

    If slots are wider than a single byte, the bytes_per_slot argument supports these values as well.
    If slots only use a half byte (or nibble), use bytes_per_slot = 0.5
    """
    __slots__ = ('_keys', '_nothing')

    codec: codecs.NumericArrayCodec

    def __init__(self, offset: int, keys: List[str], nothing: int = 0, bytes_per_slot=1):
        if len(keys) != len(set(keys)):
            raise ValueError('keys must be unique')
        codec = codecs.get_array_codec(bytes_per_slot)
        super().__init__(offset=offset, size=int(len(keys) * bytes_per_slot), codec=codec)
        self._keys = keys
        self._nothing = nothing

    @property
    def bytes_per_slot(self) -> int:
        return self.codec.width

    def encode_to_bytes(self, value: Dict[str, int]) -> bytes:
        indices = [value.get(key, self._nothing) for key in self._keys]
        return self.codec.encode_to_bytes(indices)

    def decode_from_bytes(self, source: bytes) -> Dict[str, int]:
        values = self.codec.decode_from_bytes(source)
        return {self._keys[i]: b for i, b in enumerate(values) if b != self._nothing}


class SparseCounters(Field[Dict[str, int]]):
    __slots__ = '_offset', '_labels', '_bytes_per_slot', '_length', '_codec'

    def __init__(self, offset: int, *, labels: Mapping[str, int], bytes_per_slot: int = 1):
        super().__init__()
        if len(labels) != len({v: k for k, v in labels.items()}):
            raise ValueError('labels must be bijective')
        self._offset = offset
        self._labels = labels
        self._bytes_per_slot = bytes_per_slot
        self._length = max(labels.values()) + 1
        self._codec = codecs.get_array_codec(bytes_per_slot)

    def read_bytes(self, data: bytearray, instance: object) -> bytearray:
        return data[get_slice(self._offset, self._length * self._bytes_per_slot, instance, self._name)]

    def load(self, data: bytearray, instance: object) -> Dict[str, int]:
        source = self.read_bytes(data, instance)
        counts = self._codec.decode_from_bytes(source)
        return {label: counts[pos] for label, pos in self._labels.items()}

    def save(self, data: bytearray, instance: object):
        value = getattr(instance, self._name)
        for label, count in value.items():
            if label not in self._labels:
                raise ValueError('unknown label: ' + label)
            pos = self._labels[label]
            index = self._offset + pos * self._bytes_per_slot
            encoded = codecs.encode_numeric(count, self._bytes_per_slot)
            data[get_slice(index, len(encoded), instance, self._name)] = encoded

    @property
    def labels(self) -> Dict[str, int]:
        return dict(self._labels)

    @property
    def length(self) -> int:
        return self._length


class ByteSet(SimpleField[Collection[E]]):
    """Larger version of a BitSet:

    Operates on an array of bytes and a positional mapping of elements.
    if a byte at index i is 1, the element at index i is included
    """
    __slots__ = '_elements', '_included', '_excluded', '_wrapper'

    def __init__(self, offset: int, *, elements: Sequence[E], included: int = 1, excluded: int = 0,
                 wrapper: Type[Union[List, Set]] = set):
        """
        Create a new ByteSet.

        A ByteSet maps an array of bytes to membership in a set. The test for membership can be refined
        by overriding include(b)

        :param offset:  data is read from this position in
        :param elements: the mapping of each position in the data. the first byte is mapped to the first element etc.
        :param included: the byte value which means a value is part of the set
        :param excluded: the byte value which means a value is __not__ part of the set
        """
        ensure_byte(included, 'included')
        ensure_byte(excluded, 'excluded')
        super().__init__(offset=offset, size=len(elements), codec=None)
        self._elements = elements
        self._included = included
        self._excluded = excluded
        self._wrapper = wrapper

    def include(self, b: int) -> bool:
        return b == self._included

    def decode_from_bytes(self, source: bytes) -> Collection[E]:
        return self._wrapper([self._elements[i] for i, b in enumerate(source) if self.include(b)])

    def encode_to_bytes(self, value: Collection[E]) -> bytes:
        return bytes([self._included if e in value else self._excluded for e in self._elements])


class KeyValueInventory(Field[Dict[E, int]]):
    """
    Item mapping for simple inventories.

    More complex mappings, e.g. when a bit of the second byte is used to modify the item id can be implemented
    by overriding decode_entry and encode_entry.
    """
    __slots__ = ("_elements", "_capacity", "_empty_value", "_empty_element", "_inverted")

    def __init__(self, capacity: int, elements: Sequence[E] | Mapping[int, E], empty_value: int = 0,
                 empty_element: int = 0):
        super().__init__()
        self._capacity = capacity
        self._empty_value = empty_value
        self._empty_element = empty_element
        self._elements = dict(enumerate(elements)) if isinstance(elements, Sequence) else elements
        self._inverted = invert(self._elements)

    @abstractmethod
    def read_bytes(self, data: bytearray, instance: object) -> Iterable[Tuple[int, int]]:
        """read key- and value bytes from the source and iterate them pairwise"""
        ...

    def load(self, data: bytearray, instance: object) -> Dict[E, int]:
        source = self.read_bytes(data, instance)
        entries = [self.decode_entry(e, v) for e, v in source if e != self._empty_value]
        return dict(entries)

    def decode_entry(self, element_id: int, value: int) -> Tuple[E, int]:
        """
        decodes two bytes to a mapping entry
        :param element_id:  a byte from the "elements" array
        :param value:       a byte from the corresponding "values" array
        :return: a new entry for the inventory by decoding the to bytes to an (item, count) tuple
        """
        return self._elements[element_id], value

    def encode_entry(self, element: T, value: int) -> Tuple[int, int]:
        """
        encodes an inventory entry to two bytes
        :param element: the item
        :param value:   the amount of the item in the inventory
        :return: a byte for each array
        """
        return self._inverted.get(element, self._empty_value), value

    def save(self, data: bytearray, instance: object):
        value = getattr(instance, self.name)
        if value == self.load(data, instance):
            log.debug("field %s in type %s is unchanged, not saving", self.name, self.owner)
            return
        bites = [self.encode_entry(e, v) for e, v in value.items()]
        self.save_bytes(data, instance, bites)

    @abstractmethod
    def save_bytes(self, data: bytearray, instance: object, bites: Iterable[Tuple[int, int]]):
        """given a byte value for each entries key and value, saves those bytes to data"""
        ...


class ParallelArraysInventory(KeyValueInventory[Dict[E, int]]):
    """An inventory backed by two parallel byte ranges: one for item id, one for count."""

    __slots__ = ("_keys_offset", "_values_offset")

    def __init__(self, keys_offset: int, values_offset: int, capacity: int,
                 elements: Union[Sequence[E], Mapping[int, E]],
                 empty_value: int = 0):
        super().__init__(capacity=capacity, elements=elements, empty_value=empty_value)
        self._keys_offset = keys_offset
        self._values_offset = values_offset

    def read_bytes(self, data: bytearray, instance: object) -> Iterable[Tuple[int, int]]:
        element_ids = bytes(data[get_slice(self._keys_offset, self._capacity, instance, self._name)])
        values = bytes(data[get_slice(self._values_offset, self._capacity, instance, self._name)])
        return zip(element_ids, values)

    def save_bytes(self, data: bytearray, instance: object, bites: Iterable[Tuple[int, int]]):
        element_ids, values = zip(*bites)
        id_slice = get_slice(self._keys_offset, self._capacity, instance, self._name)
        element_ids = codecs.pad(element_ids, self._capacity, padding_value=self._empty_element)
        data[id_slice] = bytearray(element_ids)
        value_slice = get_slice(self._values_offset, self._capacity, instance, self._name)
        values = codecs.pad(values, self._capacity, padding_value=self._empty_value)
        data[value_slice] = bytearray(values)


class PairwiseInventory(KeyValueInventory[Dict[E, int]]):
    """
    A Key-Value inventory that alternates key- and value bytes

    If capacity is N, the binary representation is:
    [key0 val0 key1 val1 ... key_n val_n]
    """
    __slots__ = '_offset',

    def __init__(self, offset: int, elements: Union[Sequence[E], Mapping[int, E]],
                 capacity: int, empty_value: int = 0, empty_element: int = 0):
        self._offset = offset
        super().__init__(capacity=capacity, elements=elements, empty_value=empty_value, empty_element=empty_element)

    def slice(self, instance: object):
        return get_slice(self._offset, 2 * self._capacity, instance, self._name)

    def read_bytes(self, data: bytearray, instance: object) -> Iterable[Tuple[int, int]]:
        source = data[self.slice(instance)]
        return zip(*[iter(source)] * 2)

    def save_bytes(self, data: bytearray, instance: object, bites: Iterable[Tuple[int, int]]):
        byte_values = list(chain(*bites))
        diff = self._capacity * 2 - len(byte_values)
        if diff < 0:
            raise ValueError(f'capacity ({self._capacity}) exceeded')
        elif diff > 0:
            padding = [self._empty_element, self._empty_value] * (diff // 2)
            byte_values.extend(padding)
        pos = self.slice(instance)
        log.debug("field %s: writing %d bytes to position %d:%d", self.name, len(byte_values), pos.start, pos.stop)
        data[pos] = bytes(byte_values)


class String(SimpleField[str]):
    __slots__ = "_terminator",

    codec: codecs.StringCodec

    def __init__(self, offset: int, size: int, encoding: str = 'ascii', terminator: int = 0):
        super(String, self).__init__(offset=offset, size=size,
                                     codec=codecs.StringCodec(encoding=encoding))
        self._terminator = terminator

    @property
    def encoding(self) -> str:
        return self.codec.encoding

    @property
    def terminator(self) -> int:
        return self._terminator

    def encode_to_bytes(self, value: str) -> Union[bytes, Sequence[Tuple[int, bytes]]]:
        encoded = self.codec.encode_to_bytes(value)
        return codecs.pad(encoded, self.size, self.terminator)

    def decode_from_bytes(self, source: bytes) -> str:
        stripped = bytes(list(takewhile(lambda b: b != self.terminator, source)))
        return self.codec.decode_from_bytes(stripped)


class SquareEnixInventory(ParallelArraysInventory[str]):

    def decode_entry(self, element_id: int, value: int) -> Tuple[str, int]:
        if value & 0x80:
            element_id = element_id + 0x100
            value = value - 0x80
        try:
            item: T = self._elements[element_id]
            return item, value
        except (KeyError, IndexError) as e:
            raise ValueError from e

    def encode_entry(self, element: str, value: int) -> Tuple[int, int]:
        if element not in self._inverted:
            raise ValueError('unknown item: ' + element)
        element_id = self._inverted.get(element)
        if element_id > 0xff:
            return element_id - 0x100, value + 0x80
        return element_id, value


class BinaryCodedDecimal(SimpleField[int]):
    """
    Integer field using binary coded decimal format.
    In BCD, decimal values are encoded per-digit, with each digit taking four bits.
    Examples:

    0x99 -> 99
    0x03 -> 3
    0x99 0x09 -> 999
    0x20 0x33 0x01 -> 13320
    """

    def __init__(self, offset: int, size: int):
        """
        Define a new Field
        :param offset:  the offset to read data from
        :param size:    the number of bytes used to encode this fields value
        """
        super().__init__(offset=offset, size=size, codec=codecs.SBCDCodec(length=size))


class SimpleArray(SimpleField[list[E]]):
    __slots__ = '_element_codec', '_element_size'

    def __init__(self, offset: int, length: int, element_size: int, element_codec: codecs.Codec[E]):
        super().__init__(offset, length * element_size, codec=None)
        self._element_codec = element_codec
        self._element_size = element_size

    def decode_from_bytes(self, source: bytes) -> list[E]:
        return [self._element_codec.decode_from_bytes(chunk)
                for chunk in codecs.chunk_bytes(source, self._element_size)]

    def encode_to_bytes(self, value: list[E]) -> bytes:
        chunks = [self._element_codec.encode_to_bytes(v) for v in value]
        return bytes(list(itertools.chain.from_iterable(chunks)))


class BCDArray(SimpleArray):
    """
    An array of binary coded decimals. yields list[int] as value type.
    """

    def __init__(self, offset: int, length: int, element_size: int = 1):
        """
        Define an array of binary coded decimals.

        Yields list[int] as value type.
        The array will consume a total number of `length * element_size` bytes

        :param offset:  position of the array in the data
        :param length:  number of elements in the array
        :param element_size:  bytes per element in the array
        """
        super().__init__(offset, length, element_size, element_codec=codecs.SBCDCodec(element_size))
