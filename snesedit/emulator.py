"""
Contains emulator-specific IO functions for savestate files
"""

import gzip
from abc import abstractmethod
from pathlib import Path
from typing import Tuple

from snesedit import config

SNES_RAM_SIZE = 0x20000


class Emulator:
    name: str
    num_slots: int

    @staticmethod
    def for_rom(rom_name: str) -> 'Emulator':
        emulator_name = config.get_emulator(rom_name)
        return _EMULATORS[emulator_name]

    @staticmethod
    def by_name(emulator_name: str) -> 'Emulator':
        return _EMULATORS[emulator_name]

    @abstractmethod
    def get_savestate_filename(self, basename: str, slot: int) -> str:
        """Returns the filename for a ROM savestate of the given basename and slot number"""

    @abstractmethod
    def load_ram(self, savestate: Path) -> bytearray:
        """Load the RAM portion of a savestate from the given file"""

    @abstractmethod
    def save_ram(self, savestate: Path, ram: bytearray):
        """Save the RAM portion to a given savestate path"""

    @property
    def savestate_directory(self) -> Path:
        return config.get_savestate_directory(self.name)

    @property
    def sram_directory(self) -> Path:
        return config.get_sram_directory(self.name)

    def get_savestate_file(self, rom_name: str, slot: int) -> Path:
        base_name = config.get_savestate_basename(rom_name)
        return self.savestate_directory / self.get_savestate_filename(base_name, slot)

    def get_sram_file(self, rom_name: str) -> Path:
        base_name = config.get_savestate_basename(rom_name)
        return self.sram_directory / (base_name + '.srm')

    def load_slot(self, rom_name: str, slot: int) -> bytearray:
        savestate = self.get_savestate_file(rom_name, slot)
        return self.load_ram(savestate)

    def save_slot(self, rom_name: str, slot: int, ram: bytearray):
        return self.save_ram(self.get_savestate_file(rom_name, slot), ram)


def get_snes9x_block_offset(snes9x_data: bytearray, block_name: str) -> Tuple[int, int]:
    next_block = 14  # after header
    while next_block < len(snes9x_data):
        block_header = str(snes9x_data[next_block:next_block + 11], encoding='ascii')
        #  e.g. NAM:000019:Chrono Trigger.zip
        assert block_header.endswith(':')
        cur_block_name, block_length, _ = block_header.split(':')
        block_length = int(block_length, 10)
        block_start = next_block + 11
        if cur_block_name == block_name:
            return block_start, block_length
        # keep searching
        next_block = block_start + block_length

    raise ValueError(f"block {block_name} not found in data")


def _unzip(path: Path):
    with gzip.open(path) as f:
        return bytearray(f.read())


def load_snes9x_ram(savestate) -> bytearray:
    data = _unzip(savestate)
    ram_pos, ram_size = get_snes9x_block_offset(data, 'RAM')
    if ram_size != SNES_RAM_SIZE:
        raise ValueError(f"unexpected size for RAM block, expected 0x20000 but got {ram_size:05x}")  # pragma: no cover
    return data[ram_pos:ram_pos + ram_size]


def save_snes9x_ram(savestate: Path, ram: bytearray):
    data = _unzip(savestate)
    ram_pos, _ = get_snes9x_block_offset(data, block_name='RAM')
    data[ram_pos:ram_pos + len(ram)] = ram
    with gzip.open(savestate, mode='wb') as f:
        f.write(data)


def get_snes9x_savestate(directory: Path, basename: str, slot: int):
    ext = "%03d" % slot
    return directory / (basename + "." + ext)


def get_zsnes_ext(slot: int) -> str:
    if 0 > slot or 99 < slot:
        raise ValueError("slots are 0-99")
    if 0 == slot:
        return "zst"
    if slot < 10:
        return f"zs{slot}"
    return f"z{slot}"


def load_zsnes_ram(savestate: Path) -> bytearray:
    with open(savestate, mode='rb') as f:
        data = bytearray(f.read())
    return data[0xc13:0xc13 + SNES_RAM_SIZE]


def save_zsnes_ram(savestate: Path, ram: bytearray):
    with open(savestate, mode='rb') as f:
        data = bytearray(f.read())
    data[0xc13:0xc13 + SNES_RAM_SIZE] = ram
    with(open(savestate, mode='wb')) as f:
        f.write(data)
        f.flush()


class _ZSNES(Emulator):
    num_slots = 99
    name = 'zsnes'

    def get_savestate_filename(self, basename: str, slot: int) -> str:
        return basename + "." + get_zsnes_ext(slot)

    def load_ram(self, savestate: Path) -> bytearray:
        return load_zsnes_ram(savestate)

    def save_ram(self, savestate: Path, ram: bytearray):
        save_zsnes_ram(savestate, ram)


class _Snes9x(Emulator):
    num_slots = 999
    name = 'snes9x'

    def get_savestate_filename(self, basename: str, slot: int) -> str:
        return "%s.%03d" % (basename, slot)

    def load_ram(self, savestate: Path) -> bytearray:
        return load_snes9x_ram(savestate)

    def save_ram(self, savestate: Path, ram: bytearray):
        save_snes9x_ram(savestate, ram)


_EMULATORS = {
    'zsnes': _ZSNES(),
    'snes9x': _Snes9x()
}
