from os import environ
from pathlib import Path
from configparser import ConfigParser
from importlib import resources
from logging import getLogger

APP_NAME = "snesedit"
INI_NAME = "snesedit.ini"

_config: ConfigParser = None

log = getLogger(__name__)


def get_config_file() -> Path:
    if environ.get('APPDATA'):
        return Path(environ.get('APPDATA')) / APP_NAME / INI_NAME
    return Path(f'~/.config/{APP_NAME}/{INI_NAME}').expanduser()


def read():
    global _config
    if _config is None:
        _config = ConfigParser()
        with resources.open_text(__package__, 'default.ini') as d:
            _config.read_file(d, 'defaults')

        with get_config_file().open(mode='r') as f:
            _config.read_file(f, 'user')
    return _config


def get_default_slot() -> int:
    config = read()
    return config.getint('general', 'default-slot')


def get_default_emulator() -> str:
    config = read()
    return config.get('general', 'default-emulator')


def get_emulator(rom_name: str) -> str:
    config = read()
    default = config.get(section='general', option='default-emulator')
    rom_section = "rom:" + rom_name
    if rom_section not in config:
        return default
    return config.get(section=rom_section, option='emulator', fallback=default)


def get_savestate_basename(rom_name: str):
    config = read()
    rom_section = "rom:" + rom_name
    if rom_section in config:
        if 'filename' in config[rom_section]:
            return config.get(rom_section, 'filename')
    return config.get('roms', rom_name, fallback=rom_name)


def get_savestate_directory(emulator: str) -> Path:
    config = read()
    dir_name = config.get(section='emulator:' + emulator, option='save-path')
    return Path(dir_name).expanduser()


def get_sram_directory(emulator: str) -> Path:
    config = read()
    dir_name = config.get(section='emulator:' + emulator, option='sram-path', fallback=None)
    if dir_name is None:
        return get_savestate_directory(emulator)
    return Path(dir_name).expanduser()

