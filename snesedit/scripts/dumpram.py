import sys
import os
from snesedit import config
from snesedit.emulator import Emulator
from argparse import ArgumentParser


def main():
    parser = ArgumentParser(prog='dumpram')
    parser.add_argument('rom_name', type=str, help='name of the ROM for which to dump savestate RAM')
    parser.add_argument('slot', type=int, default=0, help="slot Nr to dump", nargs='?')
    parser.add_argument("-H", "--hex", action='store_true', help="write content as hexadecimal string")
    parser.add_argument('-e', '--emulator', help='use EMULATOR instead of ROM\'s default emulator')
    args = parser.parse_args()

    if not args.emulator:
        emulator_name = config.get_emulator(rom_name=args.rom_name)
    else:
        emulator_name = args.emulator
    emulator = Emulator.by_name(emulator_name)
    ram = emulator.load_slot(rom_name=args.rom_name, slot=args.slot)
    if args.hex:
        sys.stdout.write(ram.hex(sep=' ', bytes_per_sep=1))
    else:
        with os.fdopen(sys.stdout.fileno(), "wb", closefd=False) as stdout:
            stdout.write(ram)
            stdout.flush()

    sys.stdout.flush()


if __name__ == '__main__':
    main()
