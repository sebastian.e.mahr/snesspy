import shutil
import sys
from argparse import ArgumentParser, SUPPRESS
from typing import Tuple, Optional

from snesedit.emulator import Emulator


def import_sram(rom_name: str, source_emulator: Emulator, target_emulator: Emulator, *,
                force=False, dry_run=False) -> Tuple[int, Optional[str]]:
    print("importing", rom_name, "from", source_emulator.name, "to", target_emulator.name)
    src_file = source_emulator.get_sram_file(rom_name)
    dst_file = target_emulator.get_sram_file(rom_name)
    if not src_file.is_file():
        return 2, f"SRAM for {rom_name} not found in {source_emulator.name}"
    if dst_file.is_file() and not force:
        return 2, f"SRAM for {rom_name} already exists in {target_emulator.name}"
    if dry_run:
        return 0, "dry run mode, nothing was changed"
    shutil.copy(src_file, dst_file)
    return 0, None


def main():
    parser = ArgumentParser(description='imports the SRAM data of ROM from one emulator to another')
    imp_grp = parser.add_mutually_exclusive_group()
    imp_grp.add_argument('--to-snes9x', action='store_true', default=True,
                         help='import from zsnes to snes9x [default]')
    imp_grp.add_argument('--to-zsnes', action='store_true', help='import from snes9x to zsnes')
    parser.add_argument('rom', nargs=1, help='ROM name to import')
    parser.add_argument('-f', '--force', action='store_true', help='overwrite existing SRAM files')
    parser.add_argument('--dry-run', action='store_true', help=SUPPRESS)
    zsnes = Emulator.by_name('zsnes')
    snes9x = Emulator.by_name('snes9x')
    args = parser.parse_args()
    direction = (snes9x, zsnes) if args.to_zsnes else (zsnes, snes9x)
    rom_name: str = args.rom[0]
    code, message = import_sram(rom_name, *direction, force=args.force, dry_run=args.dry_run)
    if message:
        print(message, file=sys.stderr)
    sys.exit(code)


if __name__ == '__main__':
    main()
