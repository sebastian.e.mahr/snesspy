import importlib
from argparse import ArgumentParser
from pathlib import Path

import snesedit.data
import snesedit.editors


def main():
    parser = ArgumentParser()
    parser.add_argument('game', help='name of the editor to convert')
    parser.add_argument('key', nargs='+', help='top level keys to dump')
    parser.add_argument('-w', '--write-resource', action='store_true',
                        help='generate yaml resource in editors.data')
    parser.add_argument('-m', '--module-name', default='data',
                        help='name of the data module [default: %(default)s]')
    args = parser.parse_args()
    module_name = f"snesedit.editors.{args.game}.{args.module_name}"
    module = importlib.import_module(module_name)
    if args.write_resource:
        stream = Path(snesedit.editors.__file__).parent / 'data' / f"{args.game}.yml"
        with stream.open(mode='w', encoding='utf8') as f:
            snesedit.data.dump_module(module, *args.key, stream=f)
    else:
        snesedit.data.dump_module(module, *args.key)


if __name__ == '__main__':
    main()
