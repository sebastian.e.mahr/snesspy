from abc import abstractmethod
from dataclasses import dataclass
from itertools import chain
from typing import TypeVar, List, Sequence, Tuple, Union, Iterable, Callable, Protocol, runtime_checkable

T = TypeVar('T')
E = TypeVar('E')

Encoder = Callable[[T], Union[bytes, Sequence[Tuple[int, bytes]]]]
Decoder = Callable[[bytes], T]

"""
This whole model is too complex - no need to use classes where functions would do
"""


@runtime_checkable
class Codec(Protocol[T]):

    @abstractmethod
    def encode_to_bytes(self, value: T) -> bytes:
        """
        Encode a value to a sequence of bytes.
        """

    @abstractmethod
    def decode_from_bytes(self, source: bytes) -> T:
        """
        Decode a sequence of bytes to a value
        """


# noinspection PyProtocol
class ByteArray(Codec[bytearray]):
    decode_from_bytes = bytearray
    encode_to_bytes = bytes


def encode_numeric(value: int, length: int) -> bytes:
    return bytes([(value & (0xff << (i * 8))) >> (i * 8) for i in range(length)])


def decode_numeric(source: bytes, length: int = None) -> int:
    if length is None:
        length = len(source)
    return sum((source[i] << (i * 8)) for i in range(0, length))


def pad(content: Union[bytes, Sequence[int]], size: int, padding_value: int = 0) -> bytes:
    padding = [padding_value] * (size - len(content))
    return bytes(list(chain(content, padding)))


class ScalarCodec(Codec):

    @abstractmethod
    def validate(self, value: T):
        """
        Checks whether a value can be encoded with this codec.

        :raise ValueError if the value is out of range or otherwise invalid
        :raise TypeError if the value is of an inconvertible type.
        """


@dataclass(frozen=True, eq=False)
class NumericCodec(Codec[int]):
    length: int

    @property
    def min_value(self) -> int:
        return 0

    @property
    def max_value(self) -> int:
        return sum(((0xff << (i * 8)) for i in range(self.length)))

    def validate(self, value: T):
        if value < self.min_value:
            raise ValueError(f'cannot encode values < {self.min_value}')
        if value > self.max_value:
            raise ValueError(f'cannot encode values > {self.max_value} with {self.length} bytes')

    def encode_to_bytes(self, value: int) -> bytes:
        self.validate(value)
        return encode_numeric(value, self.length)

    def decode_from_bytes(self, source: bytes) -> int:
        return decode_numeric(source, self.length)


byte = NumericCodec(length=1)
short = NumericCodec(length=2)
int24 = NumericCodec(length=3)
integer = NumericCodec(length=4)


@dataclass(frozen=True, eq=False)
class SBCDCodec(NumericCodec):
    """
    Simple binary coded decimal codec

    in BCD, decimal values are encoded per-digit, with each digit taking four bits.
    Examples:

    0x99 -> 99
    0x03 -> 3
    0x99 0x09 -> 999
    0x20 0x33 0x01 -> 13320
    """

    @property
    def max_value(self):
        return int('9' * self.length * 2, 10)

    def encode_to_bytes(self, value: int) -> bytes:
        if value == 0:
            return bytes(self.length)
        self.validate(value)
        dec_str = str(value)
        if len(dec_str) % 2 != 0:
            dec_str = '0' + str(value)
        return bytes(reversed(bytes.fromhex(dec_str)))

    def decode_from_bytes(self, source: bytes) -> int:
        return int(bytes(reversed(source)).hex(), 10)


def chunk_bytes(bites: bytes, width: int) -> Iterable[bytes]:
    if len(bites) % width != 0:
        raise ValueError("number of bytes must be a multiple of %d" % width)
    return map(bytes, zip(*[iter(bites)] * width))


@dataclass(frozen=True)
class NumericArrayCodec(Codec[List[int]]):
    width: int

    def encode_to_bytes(self, value: List[int]) -> bytes:
        if self.width == 1:
            return bytes(value)
        chunks = (chain.from_iterable(encode_numeric(v, self.width) for v in value))
        return bytes(list(chunks))

    def decode_from_bytes(self, source: bytes) -> List[int]:
        if self.width == 1:
            return [int(b) for b in source]
        return [decode_numeric(c) for c in chunk_bytes(source, self.width)]


short_array = NumericArrayCodec(width=2)
int24_array = NumericArrayCodec(width=3)
int_array = NumericArrayCodec(width=4)


class HalfByteCodec(Codec[List[int]]):
    width = 0.5

    def decode_from_bytes(self, source: bytes) -> List[int]:
        def _iter():
            for b in source:
                yield (b & 0xF0) >> 4
                yield b & 0x0F

        return list(_iter())

    def encode_to_bytes(self, value: List[int]) -> bytes:
        if len(value) % 2:
            raise ValueError("can only encode lists of even length")

        def _iter():
            for lower, upper in zip(*[iter(value)] * 2):
                if lower > 0x0f or upper > 0x0f:
                    raise ValueError('maximum value in HalfByteArray is 15')
                yield (lower << 4) + upper

        return bytes(list(_iter()))


def get_array_codec(bytes_per_slot: int | float) -> Codec[List[int]]:
    if bytes_per_slot == 0.5:
        return HalfByteCodec()
    if type(bytes_per_slot) != int or bytes_per_slot < 1:
        raise ValueError('bytes per slot: must be a positive integer or 0.5')
    return NumericArrayCodec(width=bytes_per_slot)


@dataclass(frozen=True)
class StringCodec(Codec[str]):
    encoding: str = 'ascii'

    def decode_from_bytes(self, source: bytes) -> str:
        return str(source, self.encoding)

    def encode_to_bytes(self, value: str) -> bytes:
        return value.encode(self.encoding)


@dataclass(frozen=True)
class FixedLengthString(Codec[str]):
    length: int
    encoding: str = 'ascii'
    pad_value: int = 0x00

    def decode_from_bytes(self, source: bytes) -> str:
        end = source.find(self.pad_value)
        if end == 0:
            return ''
        if end > 0:
            source = source[:end]
        return source.decode(self.encoding)

    def encode_to_bytes(self, value: str) -> bytes:
        result = value.encode(self.encoding)
        if len(result) > self.length:
            raise ValueError('maximum string length: %d', self.length)
        return pad(result, self.length, self.pad_value)

