from importlib import resources

import yaml

from snesedit.util import invert


class OffsetMapping(yaml.YAMLObject):
    """
    Custom tag for a mapping of integers to names.

    Expects a mapping node with the following attributes:

    offset: an integer with the id value for the first name in `names`
    names:  a list of strings that will form the values of the returned dictionary
    dummy:  (optional, default='<dummy>') if the `names` list contains one or more of these values,
            the corresponding entry will be skipped
    bijective:  (optional, default=false) if true ensures that the mapping is bijective, not counting dummy items

    For each name, an entry will be constructed such as key,value == (offset + index(name), name). dummy
    values will be skipped.
    """
    yaml_loader = yaml.SafeLoader
    yaml_tag = '!OffsetMapping'

    @classmethod
    def from_yaml(cls, loader, node) -> dict[int, str]:
        mapping = loader.construct_mapping(node, deep=True)
        dummy = mapping.get('dummy', '<dummy>')
        return unpack_offset_list(mapping, dummy=dummy)


class Inverted(yaml.YAMLObject, dict):
    yaml_tag = '!Inverted'
    yaml_loader = yaml.SafeLoader
    yaml_dumper = yaml.SafeDumper

    @classmethod
    def to_yaml(cls, dumper: yaml.SafeDumper, data):
        return dumper.represent_mapping(cls.yaml_tag, invert(data), flow_style=False)

    @classmethod
    def from_yaml(cls, loader: yaml.SafeLoader, node):
        mapping = loader.construct_mapping(node, deep=True)
        return cls(invert(mapping))


def load_editor_data(key: str):
    with resources.path('snesedit.editors.data', f"{key}.yml") as path:
        with path.open('r', encoding='utf-8') as stream:
            return yaml.safe_load(stream)


def unpack_offset_list(mapping: dict, dummy='<dummy>'):
    offset = mapping['offset']
    names = mapping['names']
    if not isinstance(names, list):
        raise TypeError('expected a list of names in mapping')
    if not isinstance(offset, int):
        raise TypeError('expected an offset in mapping')
    return {offset + index: name for index, name in enumerate(names) if name != dummy}


def dump_module(module, *keys: str, stream=None):
    obj = {key: getattr(module, key.replace('-', '_').upper()) for key in keys}
    result = yaml.safe_dump(obj, stream=stream, sort_keys=False, default_flow_style=False)
    if stream is None:
        print(result)
        return result

