"""
Python decorators are awesome.

But sometimes, all you want is just to attach metadata to a function or class
for later use - such as annotations in java work.

This module provides a decorator factory to store arguments to a decorator under a key in the decorated functions
__annotations__ attribute. Decorated functions are returned as-is, not wrapped.

"""
from typing import Any, Callable, Set, Union, Type, Mapping, List, Iterable, Tuple, Optional


class AnnotationError(ValueError):
    pass


class Annotator:
    """
    A decorator for storing metadata in a function or class definition

    >>> shutdown = Annotator('on_shutdown', {'order'})
    >>> @shutdown(order=4)
    >>> def close_stuff():
    >>>     ...
    >>> @shutdown(order=20, category='database')
    >>> def close_database():
    >>>     ...
    >>> shutdown.annotated == [close_stuff, close_database]
    ... True
    >>> shutdown.get(close_database)
    ... {'order': 20, 'category': 'database'}
    """

    def __init__(self, annotation_key: str, required: Set[str] = None, **defaults):
        """
        create a new annotation decorator for the given key
        :param annotation_key: store metadata under this key in an annotated objects __annotations__
        :param required:       specifies which metadata keys are required and must be passed to the decorator
        :param defaults:       a dictionary with default entries for metadata.
        """
        self._key = annotation_key
        self._required = set() if required is None else required
        self._defaults = defaults
        self._annotated = set()

    def __call__(self, f: Union[Type, Callable] = None, /, **kwargs):
        def wrap(o):
            entries = dict(**self._defaults) | dict(**kwargs)
            if self._required:
                missing = [arg for arg in self._required if arg not in kwargs]
                if missing:
                    raise ValueError(f"annotation for key {self._key} is missing argument(s) {missing}")
            self.validate(o, **entries)
            result = annotate_object(o, self._key, **entries)
            self._annotated.add(result)
            return result

        # See if we're being called as @annotator or @annotator().
        if f is None:
            # We're called with parens.
            return wrap

        # we're called as @annotator with no parens and no further entries
        return wrap(f)

    def get(self, obj: Callable) -> Mapping[str, Any]:
        return get_annotations(obj, self._key)

    @property
    def annotated(self) -> List[Callable]:
        return list(self._annotated)

    @property
    def required(self) -> Set[str]:
        return set(self._required)

    @property
    def key(self) -> str:
        return self._key

    def iter_annotations(self) -> Iterable[Tuple[Callable, Mapping[str, Any]]]:
        """
        :return: an iterator over (func, metadata) tuples for every annotated function
        """
        return ((obj, self.get(obj)) for obj in self._annotated)

    def methods_of(self, cls: Type) -> List[Tuple[Callable, Mapping[str, Any]]]:
        """
        :param cls: a class for which to select methods annotated by this annotator
        :return:    a list of methods in cls with annotations of this annotator. Does not include inherited methods.
        """

        return [(method, annotation) for method, annotation in self.iter_annotations()
                if method.__qualname__.startswith(cls.__qualname__)]

    def validate(self, method: Callable, **annotations: Any):
        """
        Called before storing annotation metadata. Allows subclasses to perform sanity checks.
        :param method:      the method to annotate
        :param annotations: the metadata to annotate the method with
        :raise AnnotationError if a sanity check fails
        """
        pass


def get_annotations(obj: Callable, key: str) -> Optional[Mapping[str, Any]]:
    return getattr(obj, '__annotations__', {}).get(key)


def annotate_object(obj: Callable, key: str, **kwargs):
    entries = obj.__annotations__.get(key, None)
    if entries is None:
        entries = {}
        obj.__annotations__[key] = entries
    entries.update(kwargs)
    return obj
