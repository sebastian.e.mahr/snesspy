import math
from typing import Mapping, Dict, TypeVar

K = TypeVar('K')
V = TypeVar('V')


def invert(mapping: Mapping[K, V], bijective: bool = True) -> Dict[V, K]:
    result = {v: k for k, v in mapping.items()}
    if bijective and len(result) != len(mapping):
        bag = {}
        for k, v in mapping.items():
            bag[v] = bag.get(v, 0) + 1
        dupes = {k: v for k, v in bag.items() if v > 1}
        raise ValueError(f"not a bijective mapping, duplicate values: {dupes}")
    return result


def ensure_byte(value: int, name: str = 'value'):
    """if :value is not an unsigned byte [0-255], raises ValueError"""
    if not 0 <= value <= 255:
        raise ValueError(f"{name} must be a single byte [0-255], actual: {value}")


def highest_bit(value: int) -> int:
    if not value:
        return 0
    return int(math.log2(value))


def terminate(string: bytes, *, terminator: int = 0) -> bytes:
    if not string:
        return bytes([terminator])
    if string[-1] != terminator:
        result = bytearray(len(string) + 1)
        result[0:len(string)] = string
        result[-1] = terminator
        return bytes(result)
    return string


def count_bits_set(value: int) -> int:
    count = 0
    n = value
    # Subtracting 1 from a decimal number flips all the bits
    # after the rightmost set bit (which is 1) including the rightmost set bit.
    # So if we subtract a number by 1 and do it bitwise & with itself (n & (n-1)),
    # we unset the rightmost set bit.
    # If we do n & (n-1) in a loop and count the number of times the loop executes, we get the set bit count.
    while n:
        n &= (n - 1)
        count += 1

    return count
