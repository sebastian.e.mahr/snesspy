import inspect
import logging
import sys
import typing
from argparse import ArgumentParser, Action, Namespace
from dataclasses import dataclass
from functools import partial
from typing import Any, Sequence, Text, Callable, Tuple, Optional, Type, Mapping, Generator

from snesedit.annotator import Annotator, AnnotationError

logger = logging.getLogger(__name__)


class CommandLineError(ValueError):

    def __init__(self, message, exit_code: int = 2):
        super().__init__(message)
        self.exit_code = exit_code
        self.message = message

    def terminate(self):
        print(self.message, file=sys.stderr)
        sys.exit(self.exit_code)


class GroupedParser(ArgumentParser):
    """
    An argument parser that allows read-access to its argument groups.

    Sometimes parsers are populated automatically or over multiple phases; the default API offers
    no way to add an argument to a group that was defined else where.

    This class stores argument groups by their title and allows getting a group using get_argument_group(self, title)

    It also offers a shortcut to declare ListOptions, which print a set of lines and terminate the program.

    GroupedParsers that are created as subparsers have access to their parent parser that created them.
    """

    def __init__(self, *args, **kwargs):
        self._all_groups = {}
        self._list_options = []
        self._global_args = set()
        self._parent_parser = kwargs.pop('parent_parser', None)
        super().__init__(*args, **kwargs)

    @property
    def actions(self) -> Mapping[str, Action]:
        """
        Returns: a map of this parsers actions, keyed by their destination argument name
        """
        return {a.dest: a for a in self._actions}

    @property
    def parent(self) -> Optional['GroupedParser']:
        return self._parent_parser

    @property
    def global_args(self) -> frozenset[str]:
        """returns: all argument names that are flagged with 'inherit', including those from parent parsers"""
        result = set(self._global_args)
        if self.parent:
            result.update(self.parent.global_args)
        return frozenset(result)

    def add_argument_group(self, *args: Any, **kwargs: Any):
        group = super().add_argument_group(*args, **kwargs)
        self._all_groups[group.title] = group
        return group

    def get_argument_group(self, title: str):
        return self._all_groups.get(title)

    # noinspection PyShadowingBuiltins
    def add_list_option(self, name: str, items: Sequence[str], prefix: str = "--list-", group: str = None,
                        help: str = None):
        if help is None:
            help = f"list all {name} and exit"
        self.add_argument(prefix + name, help=help, action=ListAction, const=items, group=group)

    def add_argument(self, *name_or_flags: Text, **kwargs: Any) -> Action:

        group_title = kwargs.pop('group', None)
        inherits = kwargs.pop('inherit', False)
        if group_title is None:
            arg_action = super().add_argument(*name_or_flags, **kwargs)
        else:
            group = self.get_argument_group(group_title)
            if group is None:
                raise ValueError('parser has no argument group ' + group_title)
            arg_action = group.add_argument(*name_or_flags, **kwargs)
        if inherits:
            self._global_args.add(arg_action.dest)
        return arg_action

    def create_subparser(self, *args, **kwargs) -> 'GroupedParser':
        return GroupedParser(*args, parent_parser=self, **kwargs)

    def add_subparsers(self, **kwargs):
        if 'parser_class' not in kwargs:
            kwargs['parser_class'] = self.create_subparser
        return super().add_subparsers(**kwargs)


class ListAction(Action):
    """
    Super eager action for list options.

    Given a 'const' value of a list of strings, iterates over that list and prints one element per line,
    and terminates the program afterwards

    This happens immediately during the parsing of the command line.
    """

    def __init__(self, option_strings, dest: str, const: Sequence[str], **kwargs):
        if const is None:
            raise ValueError("const is required for list actions")
        super().__init__(option_strings, dest, nargs=0, const=const, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        for value in self.const:
            print(value)
        sys.exit(0)


action = Annotator('savestate.action', modifies=True, default=False)

option = partial(action, modifies=False)


class SubCommandAnnotator(Annotator):

    def validate(self, method: Callable, **annotations: Any):
        signature = inspect.signature(method)
        for param in signature.parameters.values():
            if param.name == 'self':
                continue
            if param.annotation == signature.empty or not isinstance(param.annotation, type):
                raise AnnotationError('parameters of subcommands require a type annotation')
            if param.kind == param.KEYWORD_ONLY and param.default == param.empty:
                raise AnnotationError('options (keyword arguments) for subcommands require a default value')
        if annotations.get('actions'):
            if signature.return_annotation == signature.empty or not isinstance(signature.return_annotation, type):
                raise AnnotationError('subcommands with sub actions must define a class as return annotation')


_subcommand_instance = SubCommandAnnotator('savestate.subcommand')


def subcommand(m: Callable = None, *, name: str = None, modifies=False, actions=False,
               help: Mapping[str, str] = None, short_opts: Mapping[str, str] = None):
    """
    Subcommand annotations mark methods as callbacks for an argument parser.

    the subcommand will have the same name as the method, and all its positional
    arguments will be considered command line arguments.
    Positional method arguments with a default value are considered optional (nargs=?) in terms of
    command line parsing.
    Keyword-Only arguments are turned into options of the long form, e.g.--argument-name.
    The subcommands help string is taken from the methods docstring. Help for each argument can be specified
    with the "help" argument to the decorator

    Supported keyword-arguments for annotations:
    :param m        the method to annotate as a subcommand
    :param name     the subcommand name, defaults to the kebap-cased function name
    :param modifies specifies if the method possibly modifies its instance upon which it is called. Default False
    :param help     a dictionary with the help text for each argument name
    :param short_opts   usually actions are only added as long options. This mapping may contain short opts
                        (1 character strings) for parameter names added as options
    :param actions  if true, additional @action callbacks can be added from another class.
                    The methods return annotation must be a type from which @action annotations are found
                    and added to the command parser.
                    An invocation of the method *must* return an instance of that type.
                    The actions picked up from the command line are then invoked on that instance.

                    Nesting subcommands is not supported
    """
    # this function mainly exists so we can have a proper docstring
    kwargs = dict(actions=actions, modifies=modifies)
    if name is not None:
        kwargs['name'] = name
    if help is not None:
        kwargs['help'] = help
    if short_opts is not None:
        kwargs['short_opts'] = short_opts
    return _subcommand_instance(m, **kwargs)


@dataclass(frozen=True)
class Callback:
    name: str
    method: Callable
    modifies: bool
    has_arg: bool
    options: Sequence[str]
    is_default: bool = False

    @classmethod
    def for_method(cls, method: Callable, metadata: Mapping[str, Any], parser: GroupedParser) -> 'Callback':
        modifies = metadata['modifies']
        name = metadata.get('name', method.__name__)  # type: str
        long_opt = "--" + name.replace("_", "-")
        short_opt = metadata.get('short_name', '')
        if short_opt is None:
            opt_args = (long_opt,)
        else:
            if not short_opt:
                short_opt = name[0]
                if metadata.get('modifies'):
                    short_opt = short_opt.upper()
            opt_args = ("-" + short_opt, long_opt)
        arg_help = metadata.get('help', method.__doc__)
        is_default = metadata.get('default', False)
        main_arg, additional_args = _get_action_method_args(method)
        add_kwargs = {}
        has_arg = False
        if main_arg is None:
            arg_action = 'store_true'
        else:
            has_arg = True
            arg_action = 'store'
            arg_type = main_arg.annotation
            if typing.get_origin(arg_type) == list:
                arg_type_args = typing.get_args(arg_type)
                if arg_type_args:
                    add_kwargs['type'] = arg_type_args[0]
                add_kwargs['nargs'] = '+'
            else:
                add_kwargs['type'] = arg_type
            add_kwargs['metavar'] = metadata.get('metavar', main_arg.name.upper())
        group = metadata.get('group', None)
        container = parser if group is None else parser.get_argument_group(group)
        if container is None:
            raise ValueError(f"annotation on method {method.__qualname__} specifies unknown argument group {group}")
        container.add_argument(*opt_args, dest=name, help=arg_help, action=arg_action, **add_kwargs)
        return cls(name, method, modifies, has_arg, additional_args, is_default)

    def execute(self, instance: Any, args: Namespace):
        callback_arg = getattr(args, self.name)
        kwargs = {opt: getattr(args, opt) for opt in self.options if hasattr(args, opt)}
        pos_args = [callback_arg] if self.has_arg else []
        try:
            return self.method(instance, *pos_args, **kwargs)
        except CommandLineError as cle:
            cle.terminate()


def _get_action_method_args(method: Callable) -> Tuple[Optional[inspect.Parameter], Sequence[str]]:
    # ignore 'self'
    parameters = list(inspect.signature(method).parameters.values())
    assert parameters[0].name in {'self', 'cls'}
    args = parameters[1:]
    pos_kind = {inspect.Parameter.POSITIONAL_OR_KEYWORD, inspect.Parameter.POSITIONAL_ONLY}
    pos_args = [p for p in args if p.kind in pos_kind]
    kw_args = [p.name for p in args if p.kind == p.KEYWORD_ONLY]
    if len(pos_args) > 1:
        raise ValueError('action method %s can have at most one positional argument' % method.__qualname__)
    main_arg = pos_args[0] if len(pos_args) > 0 else None
    return main_arg, kw_args


def get_action_callbacks(cls: Type, parser: GroupedParser) -> Sequence[Callback]:
    callbacks = [Callback.for_method(method, metadata, parser)
                 for method, metadata in action.methods_of(cls)]
    defaults = [c.name for c in callbacks if c.is_default]
    if len(defaults) > 1:
        raise AnnotationError(f"found multiple default callbacks in cls {cls.__qualname__}: {defaults}")
    return sorted(callbacks, key=lambda c: str(int(bool(c.modifies))) + ":" + c.name)


@dataclass(frozen=True)
class SubCommand:
    name: str
    method: Callable
    parameter_names: Sequence[str]
    modifies: bool
    callbacks: Sequence[Callback]
    nested: Mapping[str, "SubCommand"]
    parent: str = None

    def run(self, instance: Any, **kwargs):
        try:
            return self.method(instance, **kwargs)
        except CommandLineError as cle:
            cle.terminate()


def find_subcommands(cls: Type, parser: GroupedParser,
                     _parent: str = None) -> Generator[Tuple[str, SubCommand], None, None]:
    cmd_methods = _subcommand_instance.methods_of(cls)
    if cmd_methods:
        prefix = '' if _parent is None else _parent + "_"
        # noinspection PyTypeChecker
        sub_parsers = parser.add_subparsers(title='subcommands', dest=prefix + "sub_cmd")
        for method, metadata in cmd_methods:
            name = metadata.get('name', method.__name__.replace('_', '-'))
            modifies = metadata.get('modifies', False)
            short_opts = metadata.get('short_opts', {})
            signature = inspect.signature(method)
            cmd_parser = sub_parsers.add_parser(name=name, help=method.__doc__)

            assert isinstance(cmd_parser, GroupedParser)  # this is mostly for the type checker
            arg_help = metadata.get('help', {})
            param_names = [p.name.replace('_', '-') for p in signature.parameters.values() if p.name != 'self']
            callbacks = []
            nested = {}
            for param in signature.parameters.values():
                if param.name == 'self':
                    continue
                arg_args = {'help': arg_help.get(param.name)}
                arg_names = [param.name.replace('_', '-')]
                if param.kind == param.POSITIONAL_OR_KEYWORD:
                    arg_args['type'] = param.annotation
                    if param.default != param.empty:
                        arg_args['default'] = param.default
                        arg_args['nargs'] = '?'
                elif param.kind == param.KEYWORD_ONLY:
                    if param.name in parser.global_args:
                        continue
                    arg_args['dest'] = param.name
                    long_arg_name = "--" + param.name.replace('_', '-')
                    if param.name in short_opts:
                        arg_names = ['-' + short_opts[param.name], long_arg_name]
                    else:
                        arg_names = [long_arg_name]
                    if param.annotation == bool:
                        arg_args['action'] = param.default and 'store_false' or 'store_true'
                    else:
                        arg_args['type'] = param.annotation
                        if param.default != param.empty:
                            arg_args['default'] = param.default
                else:
                    raise AnnotationError('subcommand arguments cannot be variadic')
                cmd_parser.add_argument(*arg_names, **arg_args)
            if metadata.get('actions'):
                callbacks = get_action_callbacks(signature.return_annotation, cmd_parser)
                this_name = name if _parent is None else _parent + "." + name
                nested = dict(find_subcommands(signature.return_annotation, cmd_parser, _parent=this_name))
            yield name, SubCommand(name, method, param_names, modifies, callbacks, nested, parent=_parent)


def execute_callbacks(callbacks: Sequence[Callback], instance: Any, args: Namespace, *,
                      execute_default=False, subcommands: Mapping[str, SubCommand] = None,
                      _sub_command_prefix: str = "") -> Tuple[bool, int]:
    """
    Executes callbacks and subcommand on an instance.
    Subcommands take precedence over callbacks.

    :param callbacks:   Callbacks created from @action methods
    :param instance:    the instance on which methods are invoked
    :param args:        a namespace of parsed command line arguments
    :param execute_default: if true and no subcommands/actions where specified by `args`, look for a callback
                            with the `default` flag and execute it
    :param subcommands: a map with subcommands collected from @subcommand methods
    :param _sub_command_prefix: internal prefix that is added when recursing into nested subcommands
    :return: a tuple (modified, executed): whether at least on callback or command with the "modifies"
             flag was executed, and the number of total methods invoked
    """
    modified = False
    executed = 0
    sub_cmd_name = getattr(args, _sub_command_prefix + "sub_cmd", None)
    if subcommands and sub_cmd_name:
        sc = subcommands.get(sub_cmd_name)
        sc_kw_args = {name.replace('-', '_'): getattr(args, name) for name in sc.parameter_names}
        logger.debug("running subcommand %s(**%s)", sc.name, sc_kw_args)
        cmd_instance = sc.run(instance, **sc_kw_args)
        if (sc.callbacks or sc.nested) and cmd_instance:
            modified, num_executed = execute_callbacks(sc.callbacks, cmd_instance,
                                                       args, execute_default=True, subcommands=sc.nested,
                                                       _sub_command_prefix=sc.name + "_") or sc.modifies
            executed += num_executed
        else:
            modified = sc.modifies
            executed = 1
    else:
        for c in callbacks:
            arg = getattr(args, c.name)
            if (c.has_arg and arg is not None) or (not c.has_arg and arg):
                c.execute(instance, args)
                modified = modified or c.modifies
                executed += 1
    if 0 == executed and execute_default:
        default_callbacks = [c for c in callbacks if c.is_default]
        if default_callbacks:
            default_callbacks[0].execute(instance, args)
            modified = modified or default_callbacks[0].modifies
            executed += 1

    return modified, executed
