import logging
import shutil
from argparse import SUPPRESS
from typing import ClassVar, Optional, Sequence, Union, Tuple, Mapping

from snesedit.cli import GroupedParser, ListAction, get_action_callbacks, find_subcommands, \
    execute_callbacks
from snesedit.config import get_default_slot
from snesedit.emulator import Emulator
from snesedit.fields import load_instance, save_instance, get_field_values, ArrayElement

log = logging.getLogger(__name__)


def format_inventory(inventory: Mapping[str, int]):
    return ", ".join(('%s x %d' % entry for entry in inventory.items()))


def format_equipment(equipment: Mapping[str, str], separator: str = ', '):
    return separator.join(('%s: %s' % entry for entry in equipment.items()))


def create_savestate_parser(rom_name, groups):
    default_slot = get_default_slot()
    parser = GroupedParser(description=f'display and modify {rom_name} save states')
    parser.add_argument('--slot', type=int, default=default_slot, metavar='N',
                        help=f"load slot N instead of default slot [{default_slot}]")
    parser.add_argument('-v', '--verbose', action='store_true', help='generate more output', inherit=True)
    parser.add_argument('-d', '--dump', action='store_true',
                        help='show state content. this is the default action if no others are given')
    parser.add_argument('--emulator', type=str, default=None, help=SUPPRESS)
    parser.add_argument('--debug', action='store_true', default=False, help=SUPPRESS)
    for title, description in groups.items():
        parser.add_argument_group(title=title, description=description)
    return parser


class Character(ArrayElement):
    cur_hp: int
    max_hp: int
    cur_mp: int
    max_mp: int
    name: str

    def heal(self):
        self.cur_hp = self.max_hp
        self.cur_mp = self.max_mp

    def __repr__(self):
        return f"{self.__class__.__name__} [{self.index}]"


class SaveState:
    rom_name: ClassVar[str] = None
    _current_slot: Optional[int] = 0
    _ram: Optional[bytearray] = None
    _modified: bool = False

    list_options: ClassVar = {}
    init_options: Sequence[str] = []
    argument_groups: ClassVar = {}

    def __init__(self, **init_options):
        self._current_slot = None
        self._ram = None
        self._modified = False
        for attr, value in init_options.items():
            setattr(self, attr, value)

    def __repr__(self):
        return f"{self.rom_name} state {self._current_slot}"

    def load(self, slot: int, emulator_name: str = None):
        if self.rom_name is None:
            raise NotImplementedError(f"class {self.__class__.__name__} must define a rom_name field")
        if emulator_name is None:
            emulator = Emulator.for_rom(self.rom_name)
        else:
            emulator = Emulator.by_name(emulator_name)
        self._ram = emulator.load_slot(self.rom_name, slot)
        self._current_slot = slot
        load_instance(self._ram, self)

    def save(self, slot: int = None, emulator_name: str = None):
        if self._ram is None:
            raise ValueError("no save data has been loaded")
        if save_instance(self._ram, self):
            if emulator_name is None:
                emulator = Emulator.for_rom(self.rom_name)
            else:
                emulator = Emulator.by_name(emulator_name)
            target_slot = self._current_slot if slot is None else slot
            save_file = emulator.get_savestate_file(self.rom_name, target_slot)
            if target_slot != self._current_slot and not save_file.exists():
                # TODO: keep envelop data in memory
                # we need to read the envelop data during saving
                base_file = emulator.get_savestate_file(self.rom_name, self._current_slot)
                shutil.copy(base_file, save_file)

            log.debug('state modified, saving to %s', save_file)
            emulator.save_slot(self.rom_name, target_slot, self._ram)
        else:
            log.debug('state was not modified, save file untouched')

    def display(self, verbose=False):
        print(f"{self.rom_name} state:")
        for name, value in get_field_values(self).items():
            print(f"{name}: {value}")

    @classmethod
    def get_list_options(cls) -> Mapping[Union[str, Tuple[str, str]], Sequence[str]]:
        """
        Returns a dictionary of named lists of strings.

        For each entry, adds an option '--list-{key}' to the states argument parser.
        If the option is encountered, prints each item of the sequence (one per line)
        and terminates the program with sys.exit(0)

        If the key is a string, the option is registered directly with the parser.

        If the key is a tuple, the first item is the option name, the second is the title
        of the argument group in the parser. The group must be already configured
        through one of the @action methods or configure_parser().

        By default, returns the class attribute 'list_options'. This method exists mainly
        as a place for this docstring, although some fancier subclasses might find a use
        in overriding it.
        """
        return getattr(cls, "list_options", {})

    @classmethod
    def get_argument_groups(cls) -> Mapping[str, str]:
        """
        Returns a dictionary of named argument groups.

        @action methods in a savestate class may be grouped as described in argparse,
        but the groups must be defined before any options are discovered.

        This method returns a mapping of 'group title' -> 'description' used to define
        the state parsers argument groups.

        @action methods may use the title in their 'group' parameter. Unknown groups
        will raise a ValueError

        By default, returns the class attribute 'argument_groups'. This method exists mainly
        as a place for this docstring, although some fancier subclasses might find a use
        in overriding it.
        """
        return getattr(cls, 'argument_groups', {})

    @classmethod
    def _configure_list_options(cls, parser):
        for key, values in cls.get_list_options().items():
            container = parser
            if isinstance(key, tuple):
                key, group = key
                container = parser.get_argument_group(group)
                if container is None:
                    raise ValueError(f'list option {key} refers to unknown argument group {group}')
            opt = key.replace(" ", "-").replace("_", "-")
            container.add_argument(f"--list-{opt}", action=ListAction, help=f"list all {key} and exit",
                                   const=values)

    @classmethod
    def configure_parser(cls, parser: GroupedParser):
        """
        Set additional arguments for the command line parser.

        This method is a stub and exists to be overridden.
        """
        pass  # pragma: no cover

    @classmethod
    def main(cls, arguments: Sequence[str] = None):
        parser = create_savestate_parser(cls.rom_name, cls.get_argument_groups())
        callbacks = list(get_action_callbacks(cls, parser))
        cls._configure_list_options(parser)
        sub_cmds = dict(find_subcommands(cls, parser))
        cls.configure_parser(parser)
        args = parser.parse_args(arguments)
        logging.basicConfig(level=args.debug and logging.DEBUG or logging.WARN)
        init_kwargs = {opt: getattr(args, opt, None) for opt in cls.init_options}
        state = cls(**init_kwargs)
        state.load(args.slot, args.emulator)
        state_modified, num_exec = execute_callbacks(callbacks, state, args, subcommands=sub_cmds)
        if num_exec == 0 or args.dump:
            state.display(verbose=args.verbose)
        if state_modified:
            state.save(emulator_name=args.emulator)
