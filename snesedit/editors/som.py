from dataclasses import dataclass
from itertools import starmap
from typing import Sequence, Mapping, Tuple, Callable, Optional, Set, Union

from snesedit.cli import GroupedParser, action, CommandLineError
from snesedit.codecs import pad
from snesedit.fields import Short, Byte, Int24, ByteArray, PositionalInventory, HalfByteArray, SimpleField
from snesedit.savestate import SaveState, Character, format_inventory

HELMETS = [
    'Bare Head', 'Bandanna', 'Hair Ribbon', 'Rabite Cap', 'Head Gear', 'Quill Cap',
    'Steel Cap', 'Golden Tiara', 'Raccoon Cap', 'Quilted Hood', 'Tiger Cap', 'Circlet',
    'Ruby Armlet', 'Unicorn Helm', 'Dragon Helm', 'Duck Helm', 'Needle Helm',
    'Cockatrice Cap', 'Amulet Helm', 'Griffin Helm', 'Faerie Crown'
]
ARMORS = [
    'Bare Body', 'Overalls', 'Kung Fu Suit', 'Midge Robe', 'Chain Vest', 'Spiky Suit', 'Kung Fu Dress',
    'Fancy Overalls', 'Chest Guard', 'Golden Vest', 'Ruby Vest', 'Tiger Suit', 'Tiger Bikini', 'Magical Armor',
    'Tortoise Mail', 'Flower Suit', 'Battle Suit', 'Vestguard', 'Vampire Cape', 'Power Suit', 'Faerie Cloak'
]

RINGS = [
    'Bare Hand', 'Faerie Ring', 'Elbow Pad', 'Power Wrist', 'Cobra Bracelet', 'Wolf\'s Band', 'Silver Band',
    'Golem Ring', 'Frosty Ring', 'Ivy Amulet', 'Gold Bracelet', ' Shield Ring', 'Lazuri Ring', 'Guardian Ring',
    'Gauntlet', 'Ninja Gloves', 'Dragon Ring', ' Watcher Ring', 'Imp\'s Ring', 'Amulet Ring', 'Wristband'
]

EQUIPMENT = HELMETS + ARMORS + RINGS

EQUIPMENT_TYPES = {"helmet": HELMETS, "armor": ARMORS, "ring": RINGS}

# the index in this list is the itemId
ITEMS = [
    'Candy', 'Chocolate', 'Honey', 'Walnut', 'Herb', 'Cup',  # Consumable
    'Magic Rope', 'Flammie Drum', 'Moogle Belt', 'Midge Mallet',  # Unique
    'Barrel'  # also Consumable
]

UNIQUE_ITEMS = frozenset(ITEMS[6:10])
CONSUMABLES = frozenset(set(ITEMS) - UNIQUE_ITEMS)

WEAPONS = ["Glove", "Sword", "Axe", "Spear", "Whip", "Bow", "Boomerang", "Pole Dart"]

SPIRITS = [
    "Gnome", "Udine", "Salamando", "Sylphid", "Luna", "Dryad", "Shade", "Lumina"
]

EXP_REQUIRED = [
    -1, 0, 16, 47, 105, 204, 363, 602, 945, 1418, 2049, 2870, 3914, 5218, 6819, 8759, 11080, 13827, 17049, 20794, 25115,
    30065, 35701, 42081, 49265, 57316, 66298, 76278, 87324, 99507, 112899, 127575, 143611, 161086, 180080, 200675,
    222956, 247009, 272921, 300783, 330686, 362724, 396992, 433588, 472611, 514162, 558343, 605260, 655018, 707726,
    763494, 822433, 884658, 950283, 1019425, 1092204, 1168739, 1249153, 1333570, 1422116, 1514918, 1612106, 1713810,
    1820163, 1931299, 2047354, 2168465, 2294772, 2426416, 2563540, 2706287, 2854804, 3009239, 3169740, 3336459, 3509548,
    3689162, 3875456, 4068587, 4268715, 4476000, 4690605, 4912693, 5142430, 5379983, 5625521, 5879214, 6141234, 6411754,
    6690950, 6978998, 7276076, 7582364, 7898043, 8223296, 8558308, 8903264, 9258352, 9623761, 9999682
]

Levels = Mapping[str, Tuple[int, int]]

EQUIPPED_BY = {0x40: 0, 0x80: 1, 0xC0: 2}


def _get_equip_type(item: str):
    for kind, items in EQUIPMENT_TYPES.items():
        if item in items:
            return kind
    raise CommandLineError('unknown equipment item: ' + item)


@dataclass(frozen=True)
class Weapon:
    level: int = None
    equipped_by: int = None


class ManaWeapons(SimpleField[dict[str, Weapon]]):

    def __init__(self):
        super().__init__(offset=0xcc54, size=8)

    def decode_from_bytes(self, source: bytes) -> dict[str, Weapon]:
        def decode_byte(index, value):
            level = value & 0x0f
            equip_flag = value & 0xf0
            return WEAPONS[index], Weapon(level, EQUIPPED_BY.get(equip_flag))

        return dict([decode_byte(i, v) for i, v in enumerate(source) if v != 0xff])

    def encode_to_bytes(self, value: dict[str, Weapon]) -> bytes:
        def encode_byte(index):
            name = WEAPONS[index]
            weapon = value.get(name)
            if weapon is None:
                return 0xff
            return weapon.level + EQUIPPED_BY.get(weapon.equipped_by, 0)

        return bytes([encode_byte(i) for i in range(self.size)])


def _has_progress(_: str, level: Tuple[int, int]) -> bool:
    return bool(level[0] or level[1])


def format_levels(levels: Levels, selector: Callable = _has_progress):
    return ", ".join((f"{name}: {lp[0]}:{lp[1]:02}"
                      for name, lp in levels.items() if selector(name, lp)))


class ManaInventory(SimpleField[dict[str, int]]):
    """
    Items in SoM are stored really weird:

    The inventory has ten slots à one byte. The item in that slot is identified by the second nibble
    in the byte (masked 0x0f). values outside of range (0x0a) may glitch or crash.

    If the item is unique (see UNIQUE_ITEMS), the first nibble is zero.
    For consumable items, the number of items is stored in the first nibble, but doubled, so 0x41 means two chocolates
    """

    @staticmethod
    def format(items: dict[str, int]):
        def format_item(item, count):
            if item in UNIQUE_ITEMS:
                return item
            return f"{item} x {count}"

        return ", ".join(starmap(format_item, items.items()))

    def __init__(self):
        super().__init__(offset=0xcc48, size=10, codec=None)

    def decode_from_bytes(self, source: bytes) -> dict[str, int]:
        def iter_entries():
            for b in source:
                item_id = b & 0x0f
                if item_id > len(ITEMS):
                    continue
                item = ITEMS[item_id]
                if item in UNIQUE_ITEMS:
                    yield item, 1
                else:
                    count = (b & 0xf0) // 0x20
                    yield item, count

        return dict(iter_entries())

    def encode_to_bytes(self, value: dict[str, int]) -> bytes:
        def encode_item(item: str, count: int):
            if item not in ITEMS:
                raise ValueError(f"unknown item: {item}")
            item_id = ITEMS.index(item)
            if item in UNIQUE_ITEMS:
                return item_id
            if count > 4:
                raise ValueError('max item count: 4')
            return count * 0x20 + item_id

        bites = [encode_item(i, v) for i, v in value.items()]
        return pad(bites, size=10, padding_value=0xff)


class ManaCharacter(Character):
    BASE_OFFSET = 0xe182
    NAMES = ["Hero", "Girl", "Sprite"]

    parent: "ManaState"

    cur_hp: int = Short(BASE_OFFSET)
    max_hp: int = Short(0xE184)
    cur_mp: int = Byte(0xE186)
    max_mp: int = Byte(0xE187)
    strength: int = Byte(0xE188)
    agility: int = Byte(0xE189)
    constitution: int = Byte(0xE18A)
    intelligence: int = Byte(0xE18B)
    wisdom: int = Byte(0xE18C)

    experience: int = Int24(0xE18D)
    _level: int = Byte(BASE_OFFSET - 1)  # stored 0-based
    _weapon_levels: list[int] = HalfByteArray(0xE1C0, 8)
    _weapon_progress: list[int] = ByteArray(0xE1D0, 8)

    _magic_levels: list[int] = HalfByteArray(0xE1C4, 8)
    _magic_progress: list[int] = ByteArray(0xE1D8, 8)
    _magic_flags: bytearray = ByteArray(0xE1C8, 8)

    @property
    def name(self):
        return self.NAMES[self.index]

    @property
    def weapon_levels(self) -> Levels:
        return {name: (self._weapon_levels[i], self._weapon_progress[i])
                for i, name in enumerate(WEAPONS)}

    @property
    def equipped_weapon(self) -> Optional[str]:
        for name, weapon in self.parent.weapons.items():
            if weapon.equipped_by == self.index:
                return name

    @property
    def magic_levels(self) -> Levels:
        return {name: (self._magic_levels[i], self._magic_progress[i])
                for i, name in enumerate(SPIRITS)}

    @property
    def level(self) -> int:
        return self._level + 1

    @property
    def exp_for_next_level(self) -> int:
        if self.level >= 99:
            return 0
        total_required = EXP_REQUIRED[self.level + 1]
        return total_required - self.experience

    def has_spirit(self, spirit: Union[str, int]) -> bool:
        if type(spirit) == str:
            spirit = SPIRITS.index(spirit)
        return self._magic_flags[spirit] > 0

    def __repr__(self):
        return f"{self.name} : {self.cur_hp}/{self.max_hp} HP, {self.cur_mp}/{self.max_mp} MP, {self.experience} XP"

    def display(self, verbose: bool = False):
        print(f"{self.name}: Level {self.level} {self.cur_hp}/{self.max_hp} HP, {self.cur_mp}/{self.max_mp} MP,"
              f"{self.experience} XP, {self.exp_for_next_level} for Level {self.level + 1}")
        if verbose:
            def has_magic(name, _):
                return self.has_spirit(name)

            print(" - Equipped:", self.equipped_weapon)
            print(" - Weapon Levels:", format_levels(self.weapon_levels))
            if self.index:
                print(" - Magic Levels:", format_levels(self.magic_levels, has_magic))
                # print('Magic Flags:', self._magic_flags.hex(sep=' '))

    def magic_level_up(self, max_level: int = None, verbose=False):
        max_level = len(self.parent.spirits) if max_level is None else max_level
        for index, spirit in enumerate(SPIRITS):
            if not self.has_spirit(index):
                continue
            level, progress = self.magic_levels[spirit]
            if level < max_level:
                if verbose:
                    print(f"{self.name}: increasing {spirit} from {level}:{progress:02} to {max_level}:00")
                self._magic_progress[index] = 0
                self._magic_levels[index] = max_level

    def weapon_level_up(self, *, verbose: bool = False):
        weapon_index = WEAPONS.index(self.equipped_weapon)
        level = self._weapon_levels[weapon_index]
        max_level = min(self.parent.orbs[self.equipped_weapon][0] + 1, 8)
        if level >= max_level:
            msg = f"{self.name}: {self.equipped_weapon} already at max level ({level})"
        else:
            self._weapon_progress[weapon_index] = 99
            msg = f"{self.name}: {self.equipped_weapon} now at {level}:99"
        if verbose:
            print(msg)

    def max_weapon_levels(self, *, verbose: bool = True):
        """maxes out all weapon skill levels for this characters"""
        for index, (weapon, (level, progress)) in enumerate(self.weapon_levels.items()):
            max_level = min(self.parent.orbs[weapon][0] + 1, 8)
            if level < max_level:
                self._weapon_levels[index] = max_level
                self._weapon_progress[index] = 0
                if verbose:
                    print(f"{self.name}: {WEAPONS[index]}: {level}:{progress:02} -> {max_level}:00")

    def level_up(self, max_level=None, verbose=False):
        if max_level is None:
            max_level = self.level + 1
        if max_level > 99:
            raise CommandLineError('max character level is 99')
        if self.level >= max_level:
            print(f"{self.name}: already at level {self.level}")
            return
        required = EXP_REQUIRED[max_level]
        if verbose:
            print(f"{self.name}: adding {required - self.experience - 1} XP to gain level {max_level}")
        # if we set it to 0 required, the character will still gain a level,
        # but no notification is shown in-game
        self.experience = required - 1


ITEM_OPTIONS = "Inventory actions"
EQUIP_OPTIONS = "Equipment actions"
PARTY_OPTIONS = "Party actions"


class ManaState(SaveState):
    rom_name = "Secret of Mana"

    list_options = {
        ("weapons", EQUIP_OPTIONS): WEAPONS,
        ("consumables", ITEM_OPTIONS): CONSUMABLES,
        ("armor", EQUIP_OPTIONS): ARMORS,
        ("helmets", EQUIP_OPTIONS): HELMETS,
        ("rings", EQUIP_OPTIONS): RINGS
    }

    argument_groups = {
        ITEM_OPTIONS: "manage consumable items",
        EQUIP_OPTIONS: "manage weapon orbs and equipment",
        PARTY_OPTIONS: "manage party attributes and levels"
    }

    _characters: Sequence[ManaCharacter] = ManaCharacter.array(length=3, step=0x200)
    gold: int = Int24(0xCC6A)

    orbs_forged: dict[str, int] = PositionalInventory(0xCFB0, WEAPONS)
    orbs_found: dict[str, int] = PositionalInventory(0xCFB8, WEAPONS)

    # level in 0x00 - 0x0f, equipped by in 0x10 - 0xf0
    _weapon_levels: bytearray = ByteArray(0xCC54, length=8)
    weapons: dict[str, Weapon] = ManaWeapons()

    inventory: dict[str, int] = ManaInventory()

    _helmets: bytearray = ByteArray(0xCC24, length=10)
    _armors: bytearray = ByteArray(0xCC30, length=10)
    _rings: bytearray = ByteArray(0xCC3C, length=10)

    @property
    def helmets(self) -> Sequence[str]:
        return [EQUIPMENT[b & 0x3f] for b in self._helmets if b != 0xff]

    @property
    def armors(self) -> Sequence[str]:
        return [EQUIPMENT[b & 0x3f] for b in self._armors if b != 0xff]

    @property
    def rings(self) -> Sequence[str]:
        return [EQUIPMENT[b & 0x3f] for b in self._rings if b != 0xff]

    @property
    def characters(self):
        return [c for c in self._characters if c.max_hp]

    @property
    def orbs(self) -> Levels:
        return {w: (self.orbs_forged.get(w, 0), self.orbs_found.get(w, 0)) for w in WEAPONS}

    @property
    def unique_items(self) -> Set[str]:
        return {item for item in UNIQUE_ITEMS if item in self.inventory}

    @property
    def consumables(self) -> Mapping[str, int]:
        return {item: count for item, count in self.inventory.items() if item not in UNIQUE_ITEMS}

    @property
    def boy(self) -> ManaCharacter:
        return self._characters[0]

    @property
    def girl(self) -> ManaCharacter:
        return self._characters[1]

    @property
    def sprite(self) -> ManaCharacter:
        return self._characters[2]

    @property
    def spirits(self) -> Sequence[str]:
        return [s for s in SPIRITS if self.girl.has_spirit(s) or self.sprite.has_spirit(s)]

    @classmethod
    def configure_parser(cls, parser: GroupedParser):
        parser.add_argument('--level', type=int, default=None, metavar='LVL', group=PARTY_OPTIONS,
                            help='when leveling up characters or magic skills, '
                                 'override the default cap and allow a maximum of LVL')
        parser.add_argument('--count', type=int, default=1, metavar='N', group=EQUIP_OPTIONS,
                            help='when using --add-equipment, add N items instead of one')

    def display(self, verbose=False):
        print(f"Secret of Mana State: {self.gold} Gold")
        if verbose:
            print("Unique Items:", ", ".join(self.unique_items))
            print("Consumable Items:", format_inventory(self.consumables))
            print("Weapons: ", ", ".join([f"{name}: {w.level}"
                                          for name, w in self.weapons.items()]))
            print("Orbs:", ", ".join((f"{name}: {ff[0]}/{ff[1]}"
                                      for name, ff in self.orbs.items() if ff[1])))
            print("Spirits:", ", ".join(self.spirits))
            for et in ('helmets', 'armors', 'rings'):
                print(f"{et.capitalize()}:", ", ".join(getattr(self, et)))

        for c in self.characters:
            c.display(verbose)

    def get_equipment(self, character_index: int) -> Sequence[str]:
        return [self.get_equipped_item(slot, character_index) for slot in ('helmet', 'armor', 'ring')]

    def get_equipped_item(self, slot: str, index: int) -> str:
        for byte in getattr(self, f"_{slot}s"):
            item_id = byte & 0x3f
            equipped = byte & 0xc0
            if byte != 0xff and equipped and EQUIPPED_BY.get(equipped, -1) == index:
                return EQUIPMENT[item_id]
        return "No " + slot

    @action(name='gold', short_name='G', group=PARTY_OPTIONS)
    def add_gold(self, gold: int):
        """add GOLD to party gold"""
        self.gold += gold

    @action(group=PARTY_OPTIONS)
    def heal(self):
        """heal all characters"""
        for c in self._characters:
            c.heal()

    @action(group=ITEM_OPTIONS)
    def refill(self, *, verbose: bool = False):
        """refills all consumable items except barrels"""
        for item in CONSUMABLES:
            if item != 'Barrel' and self.inventory.get(item, 0) < 4:
                if verbose:
                    print("added", 4 - self.inventory.get(item, 0), "x", item)
                self.inventory[item] = 4

    @action(metavar='N', short_name='E', name='experience', group=PARTY_OPTIONS)
    def add_experience(self, amount: int, *, verbose: bool = False):
        """add N XP to each character"""
        if verbose:
            print(f"adding {amount} experience")
        for c in self.characters:
            c.experience += amount
            if verbose:
                print(f"{c.name}: {c.experience} XP")

    @action(short_name='O', group=EQUIP_OPTIONS)
    def add_orb(self, weapon: str, *, verbose=False):
        """add a weapon orb for WEAPON"""
        if weapon not in WEAPONS:
            raise CommandLineError('unknown weapon %s, see --list-weapons for allowed values' % weapon)
        self.orbs_found[weapon] = self.orbs_found.get(weapon, 0) + 1
        if verbose:
            print(f"added {weapon} orb, new weapon level: {self.orbs_forged[weapon]}/{self.orbs_found[weapon]}")

    @action(short_name=None, group=PARTY_OPTIONS)
    def level_up(self, *, level: int = None, verbose: bool = False):
        """adds EXP to all character to gain a level, or up to --level if given"""
        for c in self.characters:
            c.level_up(level, verbose)

    @action(short_name=None, group=PARTY_OPTIONS)
    def magic_level_up(self, *, level: int = None, verbose=False):
        """
        Level up all acquired spirits to at least level (number of spirits).
        This upper cap can be overridden by --level
        """
        if level is None:
            level = len(self.spirits)
        if level < 0 or level > 9:
            raise ValueError("Level must be in [1..8]")
        if verbose:
            print(f"increasing magic up to level {level}")
        self.girl.magic_level_up(level, verbose)
        self.sprite.magic_level_up(level, verbose)

    @action(short_name=None, group=ITEM_OPTIONS)
    def add_consumable(self, item: str, *, count: int = 1, verbose: bool = False):
        """adds 1 (or --count, up to 4) of consumable ITEM to inventory"""
        if item not in CONSUMABLES:
            raise CommandLineError("not a consumable item: " + item)
        current = self.inventory.get(item, 0)
        after = min(4, current + count)
        self.inventory[item] = after
        if verbose:
            print("added", after - current, "x", item)

    @action(short_name=None, group=EQUIP_OPTIONS)
    def add_equipment(self, item: str, *, count: int, verbose: bool):
        """
        adds 1 (or --count if given) of ITEM to party equipment
        """
        kind = _get_equip_type(item)
        item_id = EQUIPMENT.index(item)
        storage: bytearray = getattr(self, f"_{kind}s")
        slots = [i for i, v in enumerate(storage) if v == 0xff]
        if len(slots) < count:
            raise ValueError(f'not enough room to add {count} x {item}')
        if verbose:
            print(f"adding {count} x {item}")
        for i in slots[:count]:
            storage[i] = item_id

    @action(short_name=None, group=PARTY_OPTIONS)
    def weapon_level_up(self, *, verbose: bool = True):
        """Set progress of all equipped weapons to :99"""
        for c in self.characters:
            c.weapon_level_up(verbose=verbose)

    @action(short_name=None, group=PARTY_OPTIONS)
    def max_weapon_levels(self, *, verbose: bool = True):
        """maxes out all weapon skill levels for all characters"""
        for c in self.characters:
            c.max_weapon_levels(verbose=verbose)


if __name__ == "__main__":
    ManaState.main()
