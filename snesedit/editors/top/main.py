import math
import re
from typing import List, Dict, Set, Mapping, Optional, Sequence

from snesedit.cli import action, subcommand, CommandLineError
from snesedit.editors.top.data import BASE_NAMES, SKILL_TYPES
from snesedit.editors.top.types import TopString, TopInventory, Equipment
from snesedit.fields import Int24, Short, Byte, ShortArray, BitSet, SparseCounters, \
    PositionalInventory
from snesedit.savestate import SaveState, Character, format_equipment
from . import data


def to_equip_attr(equip_slot: str) -> str:
    return equip_slot.replace(' ', '').lower()


ITEM_GROUP = 'Inventory Management'
EQUIP_SLOTS = "Weapon", "Armor", "Shield", "Helmet", "Gloves", "Accessory 1", "Accessory 2"
EQUIP_ATTRS = [to_equip_attr(label) for label in EQUIP_SLOTS]


class TopCharacter(Character):
    name: str = TopString(0x6b1d)

    cur_hp: int = Short(0x6ad0)
    max_hp: int = Short(0x6ace)
    cur_mp: int = Short(0x6ad6)
    max_mp: int = Short(0x6ad4)

    weapon: str = Equipment(0)
    armor: str = Equipment(1)
    shield: str = Equipment(2)
    helmet: str = Equipment(3)
    gloves: str = Equipment(4)
    accessory1: str = Equipment(5)
    accessory2: str = Equipment(6)
    _equipment = ShortArray(0x6b0c, length=7)

    level: int = Byte(0x6ad8)
    _status: Set[str] = BitSet(0x6ada, elements=['Poisoned', 'Stoned', 'Paralyzed', 'Dead'])
    experience: int = Int24(0x6b1a)
    _archetype: int = Byte(0x6a92)  # see BASE_NAMES

    parent: "TopState"

    @property
    def playable(self) -> bool:
        return self._archetype in (1, 2, 3, 4, 6)

    @property
    def archetype(self) -> str:
        return BASE_NAMES[self._archetype]

    @property
    def skills(self) -> List[str]:
        skill_attr = SKILL_TYPES[self._archetype]
        if skill_attr:
            return getattr(self.parent, skill_attr)
        return []

    @property
    def skill_type(self) -> Optional[str]:
        return SKILL_TYPES[self._archetype]

    @property
    def status(self) -> str:
        if not self._status:
            return "Normal"
        return ", ".join(self._status)

    @property
    def class_name(self) -> str:
        if self._archetype == data.Archetype.CLESS:
            return data.CLESS_CLASSES[getattr(self.parent, '_class_flag')]
        return data.CLASSES[self._archetype]

    @property
    def equipment(self) -> Mapping[str, str]:
        return {label: getattr(self, attr) for label, attr in zip(EQUIP_SLOTS, EQUIP_ATTRS)}

    def __repr__(self):
        return f"{self.name}: L{self.level} {self.class_name} - {self.cur_hp}/{self.max_hp} HP " \
               f"{self.cur_mp}/{self.max_mp} TP, {self.experience} XP"

    def heal(self):
        super().heal()
        self._status.clear()

    @action(default=True, modifies=False)
    def display(self, *, verbose=False):
        """Show character details"""
        print(f"{self.name}: L{self.level} {self.class_name} - {self.cur_hp}/{self.max_hp} HP",
              f"{self.cur_mp}/{self.max_mp} TP, {self.experience} XP")
        if verbose:
            print("Status:", self.status)
            print("Equipment:\n -", format_equipment(self.equipment, separator='\n - '))
            if self.skill_type:
                print(f"{self.skill_type.capitalize()}:\n -", "\n - ".join(self.skills))

    @action(name='experience')
    def add_xp(self, xp: str, *, verbose: bool = False):
        """add XP experience points to selected characters. Either absolute or in percent of current XP"""
        if not re.match(r'\d+\%?', xp):
            raise CommandLineError('XP must be an integer or percent value')
        if xp.endswith('%'):
            if verbose:
                print(f"Adding {xp} of XP (current: {self.experience}) to {self.name}")
            to_add = int(xp[:-1])
            xp = int(self.experience * (to_add / 100.0))
        else:
            xp = int(xp)
        self.experience += xp

    @action(shortName=None, modifies=False)
    def list_skill_names(self):
        """List all possible skills for this character"""
        items = getattr(data, self.skill_type.upper())
        for name in items:
            print(name)

    @action(short_name=None)
    def add_skill(self, skill: str):
        """Add SKILL to character. Must be a usable skill for this character as returned by --list-skill-names"""
        self.parent.add_skill(self.skill_type, skill)

    @action(short_name=None)
    def remove_skill(self, skill: str):
        """remove SKILL from character."""
        self.parent.remove_skill(self.skill_type, skill)

    @action(name='activate', short_name=None, modifies=True)
    def set_active_spells(self, spells: list[str], *, verbose: bool = False):
        """Mark only SPELLS as used in battle"""
        if self._archetype == data.Archetype.CLESS:
            raise CommandLineError(f"{self.name}'s techniques cannot be activated")
        if self._archetype == data.Archetype.CHESTER:
            raise CommandLineError(f"{self.name} does not have techniques")
        if not self.playable:
            raise CommandLineError(f"unplayable character: {self.name}")
        active_skills = set(spells)
        raw_skillset = self.parent.raw_skillset(self.skill_type)
        for skill in self.skills:
            flag = 0x01 if skill in active_skills else 0x81
            if raw_skillset[skill] != flag:
                raw_skillset[skill] = flag
                verb = 'activated' if flag == 0x01 else 'deactivated'
                if verbose:
                    print(skill, verb)


class TopState(SaveState):
    rom_name = "Tales of Phantasia"

    gold: int = Int24(0x14b3)
    battles: int = Short(0x143d)
    _step_counter: int = Byte(0x143c)
    _class_flag: int = Byte(0x14ab)

    characters: List[TopCharacter] = TopCharacter.array(length=5, step=0x100)
    inventory: Dict[str, int] = TopInventory()

    food1: int = Byte(0x1484)  # max 200
    food2: int = Short(0x1486)  # max 2000
    food3: int = Short(0x1488)  # max 20000

    rare_items: Set[str] = BitSet(0x1593, elements=data.RARE_ITEMS)

    _techniques: Mapping[str, int] = PositionalInventory(offset=0x7091, keys=data.TECHNIQUES)
    _tech_levels: dict[str, int] = SparseCounters(0x7156, labels=data.TECH_LEVELS)

    _mana: Mapping[str, int] = PositionalInventory(0x70b6, keys=data.DATA['mana'])
    _summons: Mapping[str, int] = PositionalInventory(0x70d6, keys=data.SUMMONS)
    _spells: Mapping[str, int] = PositionalInventory(0x70f6, keys=data.SPELLS)

    argument_groups = {
        ITEM_GROUP: 'display and modify the party inventory'
    }

    list_options = {
        ('item names', ITEM_GROUP): data.ITEMS
    }

    def _tech_progress(self, tech: str):
        if self._tech_levels.get(tech):
            return str(self._tech_levels[tech]) + '%'
        return '-' if tech in self._techniques else '?'

    @property
    def techniques(self) -> Sequence[str]:
        def _iter_tech_names():
            for tech in data.TECHNIQUES:
                if tech.startswith('_') or tech not in self._techniques:
                    continue
                if tech in data.TECH_COMBOS:
                    if self._techniques[tech] == 0x81:
                        yield tech + '*'
                    else:
                        progress = '/'.join([self._tech_progress(t) for t in data.TECH_COMBOS[tech]])
                        yield f"{tech} [{progress}]"
                elif tech in data.TECH_LEVELS:  # skill can be mastered, e.g. Tiger Teeth
                    progress = self._tech_levels.get(tech, 100)
                    if 0 <= progress < 100:
                        yield f"{tech} [{progress}%]"
                    else:
                        yield f"{tech}"
                else:
                    yield tech  # base skill, e.g. Focus

        return tuple(_iter_tech_names())

    @property
    def mana(self) -> Sequence[str]:
        return tuple(self._mana.keys())

    @property
    def spells(self) -> Sequence[str]:
        return tuple(self._spells.keys())

    @property
    def summons(self) -> Sequence[str]:
        return tuple(self._summons.keys())

    @action(modifies=False)
    def all_characters(self):
        """display/modify all playable characters, including the backup character"""
        setattr(self, "_all_characters", True)

    @property
    def active_characters(self):
        max_index = 4 if getattr(self, '_all_characters', False) else 3
        return [c for c in self.characters if c.playable and c.level and c.index <= max_index]

    def display(self, verbose=False):
        print(f"ToP State {self._current_slot}: {self.gold} Gold, {self.battles} Battles")
        for c in self.active_characters:
            c.display(verbose=verbose)

    @action(name='gold')
    def add_gold(self, gold: int):
        """add GOLD to party account"""
        self.gold += gold

    @action(name='experience')
    def add_xp(self, xp: str, *, verbose: bool = False):
        """add XP experience points or percent of current XP to selected characters"""
        if not re.match(r'\d+%?', xp):
            raise CommandLineError('XP must be an integer optionally follows by a % character')
        for c in self.active_characters:
            c.add_xp(xp, verbose=verbose)

    @action
    def heal(self):
        """heal selected characters"""
        for c in self.active_characters:
            c.heal()

    @subcommand(modifies=True, short_opts={'verbose': 'v', 'fill': 'f'}, help={
        'item': 'name of the item to add',
        'count': 'add count items instead of one',
        'fill': 'ignore count and add max (up to 15) items',
        'verbose': 'show what is being done'
    })
    def add(self, item: str, count: int = 1, *, fill: bool = False, verbose: bool = False):
        """
        add one (or COUNT) ITEM to the inventory. To add maximum items, use --fill
        """
        if fill:
            count = 15
        elif count > 15:
            raise CommandLineError("cannot add more than 15 items")
        if item not in data.ITEMS:
            raise CommandLineError(f"unknown item: '{item}'")
        current = self.inventory.get(item, 0)
        new_total = min(15, current + count)
        self.inventory[item] = new_total
        if verbose:
            diff = new_total - current
            print("added", diff, 'x', item)

    @subcommand(name='remove', modifies=True)
    def remove_item(self, item: str):
        """Removes ITEM from inventory"""
        if item in self.inventory:
            del self.inventory[item]

    # noinspection PyUnusedLocal
    @subcommand(actions=True, modifies=False, short_opts={'verbose': 'v'},
                help={'name': 'name of the character to select',
                      'verbose': 'generate more output'})
    def character(self, name: str, *, verbose: bool = False) -> TopCharacter:
        """Display and modify character details"""
        for c in self.characters:
            if c.playable and c.name.lower() == name.lower():
                return c
        raise CommandLineError("no such character: " + name)

    @action(name='bottles', short_name=None, group=ITEM_GROUP)
    def replenish_bottles(self):
        """replenish all bottles (except charm)"""
        for kind in ('Medicine', 'Remedy', 'Holy', 'Dark', 'Flare', 'Life', 'Rune'):
            self.add(f'{kind} Bottle', fill=True)

    @action(name='gummies', short_name=None, group=ITEM_GROUP)
    def replenish_gummies(self):
        """replenish all gummies"""
        for kind in ('Apple', 'Orange', 'Mixed', 'Lemon', 'Pine', 'Miracle'):
            self.add(f'{kind} Gummy', fill=True)

    @action(name='inventory', modifies=False, group=ITEM_GROUP)
    def show_inventory(self):
        """list the party inventory"""
        for item, count in self.inventory.items():
            print(f"{count:02} x {item}")

    @action(short_name=None, modifies=True, group=ITEM_GROUP)
    def transform_items(self, *, verbose: bool = False):
        """
        upgrade all transformable items as if using a rune bottle. If the resulting item slot is full,
        leaves the source items untouched.
        Does not affect items with bidirectional transformations (like the Nymph's Ring)
        """

        def prt(msg):
            if verbose:
                print(msg)

        for source, result in data.TRANSFORMATIONS.items():
            if source in self.inventory:
                source_count = self.inventory[source]
                result_count = self.inventory.get(result, 0)
                if source_count + result_count <= 15:
                    prt(f"transforming {source_count} x {source} to {result}")
                    del self.inventory[source]
                    self.inventory[result] = source_count + result_count
                else:
                    upgrade_count = 15 - result_count
                    leftover = source_count - upgrade_count
                    if upgrade_count:
                        prt(f"transforming {upgrade_count} x {source} to {result}, {leftover} left")
                        self.inventory[source] = leftover
                        self.inventory[result] = 15
                    else:
                        prt(f"found {source_count} x {source} to transform, but already 15 {result} in inventory")

    @action(short_name='T')
    def master_techs(self, *, verbose=False):
        """Boost all masterable skills that have been learned to 99%%"""  # needs double % for argparse
        for tech in data.TECH_LEVELS:
            if tech in self._techniques and self._tech_levels[tech] < 100:
                if verbose:
                    print(tech, 'ready to master')
                self._tech_levels[tech] = 99

    @action(short_name=None)
    def catch_up(self, *, verbose=False):
        """
        Let weaker characters catch up giving everyone at least 85%% the XP that the highest-leveled character has.
        """
        max_xp = max((c.experience for c in self.characters))
        threshold = int(max_xp * 0.85)
        for c in self.active_characters:
            if c.experience < threshold:
                if verbose:
                    added = threshold - c.experience
                    print(f"boosting {c.name}: adding {added} XP")
                c.experience = threshold

    @action(name="rare-items", modifies=False, group=ITEM_GROUP)
    def list_rare_items(self):
        """Show the rare (story) items"""
        print("Rare Items:")
        for item in self.rare_items:
            print(" -", item)

    @action(name='food')
    def fill_food_sacks(self):
        """Fill up the parties food sack(s)"""
        if "Small Sack" in self.rare_items:
            self.food1 = 200
        if "Medium Sack" in self.rare_items:
            self.food2 = 2000
        if "Large Sack" in self.rare_items:
            self.food3 = 20000

    @action(modifies=True, short_name=None)
    def set_battles(self, battles: int):
        """Set the number of battles fought"""
        if battles < 0:
            raise CommandLineError('battles must be a positive integer')
        self.battles = battles

    @subcommand(modifies=True, help={
        'character': 'name of the the character to equip',
        'equip_slot': 'equipment slot, one of ' + ', '.join(f"'{e}'" for e in EQUIP_SLOTS),
        'item': 'the item to equip in the given slot. '
                'No sanity checks are done, i.e. equipping a food item as weapon will crash the game'
    })
    def equip(self, character: str, equip_slot: str, item: str):
        """Equip character with item in specified slot."""
        print(f"equipping {character} with {equip_slot} {item}")
        char = self.character(name=character)
        attr = to_equip_attr(equip_slot)
        if attr not in EQUIP_ATTRS:
            raise CommandLineError("unknown equipment slot: " + equip_slot)
        if item not in data.ITEMS:
            raise CommandLineError("unknown item: " + item)
        setattr(char, attr, item)

    def raw_skillset(self, skill_type: str, check_skill: str = None) -> Dict[str, int]:
        if skill_type not in SKILL_TYPES:
            raise ValueError('unknown skill type' + skill_type)
        skill_names = getattr(data, skill_type.upper())
        if check_skill and check_skill not in skill_names:
            raise CommandLineError(f"unknown {skill_type[:-1]} '{check_skill}'")
        return getattr(self, '_' + skill_type)

    def add_skill(self, skill_type: str, skill: str):
        skillset = self.raw_skillset(skill_type, skill)
        if skill not in skillset:
            skillset[skill] = 0x81 if skill_type == 'techniques' else 1

    def remove_skill(self, skill_type: str, skill: str):
        skillset = self.raw_skillset(skill_type)
        if skill in skillset:
            del skillset[skill]
        if skill_type == 'techniques' and self._tech_levels.get(skill, 0):
            self._tech_levels[skill] = 0


if __name__ == '__main__':
    TopState.main()
