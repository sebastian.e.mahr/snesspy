from enum import IntEnum
from typing import Mapping, Tuple
from snesedit.data import load_editor_data

DATA = load_editor_data('top')
ITEMS = DATA['items']
NOTHING = 0
RARE_ITEMS: list[str] = DATA['rare-items']
TECHNIQUES: list[str] = DATA['techniques']

# 0x01 -> usable
MANA: list[str] = DATA['mana']
SUMMONS: list[str] = DATA['summons']

SPELLS: list[str] = DATA['spells']
EQUIPMENT_SLOTS = ("Weapon", "Armor", "Shield", "Helmet", "Gloves", "Accessory1", "Accessory2")

TECH_LEVELS: dict[str, int] = DATA['tech-levels']
TECH_COMBOS: Mapping[str, Tuple[str, str]] = DATA['tech-combos']

BASE_NAMES = [None, 'Cless', 'Mint', 'Arche', 'Chester', 'Rambard', 'Klarth', 'Lia']
TRANSFORMATIONS: dict[str, str] = DATA['transformations']

class Archetype(IntEnum):
    CLESS = 1
    MINT = 2
    ARCHE = 3
    CHESTER = 4
    RAMBARD_ = 5
    KLARTH = 6
    LIA_ = 7

SKILL_TYPES = [None, 'techniques', 'mana', 'spells', None, None, 'summons', None]
CLASSES = DATA['classes']
CLESS_CLASSES =   DATA['cless.classes']

for src, dst in TRANSFORMATIONS.items():
    assert src in ITEMS, "bad transformation source: " + src
    assert dst in ITEMS, "bad transformation target: " + dst