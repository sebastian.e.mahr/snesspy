from typing import Tuple, Union

from snesedit.editors.top.data import ITEMS, EQUIPMENT_SLOTS
from snesedit.fields import SimpleField, ParallelArraysInventory, T


class TopString(SimpleField[str]):

    def __init__(self, offset: int):
        super().__init__(offset, size=8, codec=None)

    def encode_to_bytes(self, value: str) -> bytes:
        if len(value) > 7:
            raise ValueError('max string length in ToP: 7')
        encoded = [ord(c) - 0x20 for c in value]
        while len(encoded) < 8:
            encoded.append(0xff)
        return bytes(encoded)

    def decode_from_bytes(self, source: bytes) -> str:
        translated = [b + 0x20 for b in source if b not in (0x00, 0xff)]
        characters = [chr(t) for t in translated]
        return "".join(characters)
        # return "".join(map(lambda b: chr(b + 0x20), filter(lambda b: b and b != 0xFF, source)))


class TopInventory(ParallelArraysInventory[str]):

    def __init__(self):
        super().__init__(keys_offset=0x8969, values_offset=0x8aa9, capacity=318,
                         elements=ITEMS)

    def decode_entry(self, element_id: int, value: int) -> Tuple[str, int]:
        if value & 0x80:
            element_id = element_id + 0x100
            value = value - 0x80
        try:
            item: T = self._elements[element_id]
            return item, value
        except IndexError as e:
            raise ValueError from e

    def encode_entry(self, element: str, value: int) -> Tuple[int, int]:
        element_id = self._inverted.get(element)
        if element_id > 0xff:
            return element_id - 0x100, value + 0x80
        return element_id, value


class Equipment:

    def __init__(self, slot: Union[int, str]) -> None:
        super().__init__()
        if type(slot) is str:
            slot = EQUIPMENT_SLOTS.index(slot)
        self.slot = slot

    def __get__(self, instance, owner):
        if instance is None:
            return self
        equipment = getattr(instance, '_equipment')
        item_id = equipment[self.slot]
        return ITEMS[item_id]

    def __set__(self, instance, value):
        item_id = ITEMS.index(value)
        getattr(instance, '_equipment')[self.slot] = item_id
