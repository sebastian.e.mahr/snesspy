from snesedit.fields import BinaryCodedDecimal, Short, ShortArray, Byte, DenseArrayElement
from snesedit.savestate import SaveState

TOWNS = 'Fillmore,Bloodpool,Kasandora,Aitos,Marahna,Northwall'.split(',')


class Town(DenseArrayElement):
    population: int = Short(0x218)
    growth_rate: int = Byte(0x0228)
    tech_level: int = Short(0x022E)
    offerings: int = Short(0x023A)
    soul_count: int = BinaryCodedDecimal(0x19EFA, 2)

    @property
    def name(self) -> str:
        return TOWNS[self.index]

    def __repr__(self):
        return f"{self.name}: {self.population} POP, level {self.tech_level}, growth {self.growth_rate}, " + \
               f"{self.offerings} offerings, {self.soul_count} souls"


class ActState(SaveState):
    rom_name = 'ActRaiser'

    soul_points: int = Short(0x0282)
    max_sp: int = Short(0x0284)
    angel_cur_hp: int = Byte(0x0286)
    angel_max_hp: int = Byte(0x0287)
    total_population: int = Short(0x0218)
    towns = Town.array(length=len(TOWNS), step=2)
    _current_town: int = Byte(0x0341)
    _mode: int = Byte(0x18)
    _map: int = Byte(0x19)
    cur_hp: int = Byte(0x1D)
    max_hp: int = Byte(0x1E)
    _level_score: int = BinaryCodedDecimal(0x1F, 2)
    magic_points: int = Byte(0x20)

    _town_populations: list[int] = ShortArray(0x021C, length=len(TOWNS))

    @property
    def level_score(self) -> int:
        return self._level_score * 10


if __name__ == "__main__":
    ActState.main()
