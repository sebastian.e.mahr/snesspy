from snesedit.fields import Byte
from snesedit.savestate import SaveState


class MegaManX(SaveState):
    rom_name = "Mega Man X"

    lives: int = Byte(0x1FB4)


if __name__ == '__main__':
    MegaManX.main()
