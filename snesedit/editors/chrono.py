import logging
from typing import Sequence

from snesedit.cli import action, GroupedParser, subcommand, CommandLineError
from snesedit.fields import ByteArray, Short, Byte, Int24, Mapped, ParallelArraysInventory, SimpleField, BCDArray, \
    ByteSet
    from snesedit.savestate import SaveState, Character
from snesedit.util import count_bits_set, invert

NAMES = ["Crono", "Marle", "Lucca", "Robo", "Frog", "Ayla", "Magus"]
PARTY_SIZE = len(NAMES)

# for which tech level is magic required? e.g. Chrono: 3rd tech = lightning,
# Magus: 1st tech, frog: 3, Robo & Ayla: none
MAGIC_REQUIRED = [3, 3, 3, 0xff, 3, 0xff, 1]

TRIPLE_TECHS = (
    'Delta Force', 'Lifeline', 'Arc Impulse', 'Final Kick', 'Fire Zone',
    'Delta Storm', 'GatlingKick', 'Triple Raid', 'Twister', '3D Attack',
)

log = logging.getLogger(__name__)

# empty strings between commas mean unusable dummy items
ITEM_NAMES = str(
    "Nothing,Wood Sword,Iron Blade,SteelSaber,Lode Sword,Red Katana,Flint Edge,Dark Saber,Aeon Blade,Demon Edge,"
    "AlloyBlade,Star Sword,VedicBlade,Kali Blade,Shiva Edge,Bolt Sword,Slasher,Bronze Bow,Iron Bow,Lode Bow,Robin Bow,"
    "Sage Bow,Dream Bow,Comet Arrow,SonicArrow,Valkyrie,Siren,,,,,Air Gun,Dart Gun,Auto Gun,PicoMagnum,Plasma Gun,"
    "Ruby Gun,Dream Gun,Megablast,Shock Wave,Wonder Shot,Graedus,,,,,Tin Arm,Hammer Arm,Mirage Hand,Stone Arm,"
    "DoomFinger,"
    "Magma Hand,MegatonArm,Big Hand,Kaiser Arm,Giga Arm,Terra Arm,Crisis Arm,,BronzeEdge,Iron Sword,Masamune I,"
    "FlashBlade,Pearl Edge,Rune Blade,BraveSword,Masamune II,Demon Hit,Fist I,Fist II,Fist III,Iron Fist,"
    "Bronze Fist,,,DarkScythe,Hurricane,StarScythe,DoomSickle,Mop,Bent Sword,Bent Hilt,Masamune (0),"
    "Swallow,Slasher II,"
    "Rainbow,,,,,,Hide Tunic,Karate Gi,BronzeMail,MaidenSuit,Iron Suit,Titan Vest,Gold Suit,Ruby Vest,Dark Mail,"
    "Mist Robe,Meso Mail,Lumin Robe,Flash Mail,Lode Vest,Aeon Suit,ZodiacCape,Nova Armor,PrismDress,Moon Armor,"
    "Ruby Armor,RavenArmor,Gloom Cape,White Mail,Black Mail,Blue Mail,Red Mail,White Vest,Black Vest,Blue Vest,"
    "Red Vest,Taban Vest,Taban Suit,,Hide Cap,BronzeHelm,Iron Helm,Beret,Gold Helm,Rock Helm,CeraTopper,Glow Helm,"
    "Lode Helm,Aeon Helm,Prism Helm,Doom Helm,Dark Helm,Gloom Helm,Safe Helm,Taban Helm,Sight Cap,Memory Cap,Time Hat,"
    "Vigil Hat,OzziePants,Haste Helm,R'bow Helm,MermaidCap,,Bandana,Ribbon,PowerGlove,Defender,MagicScarf,Amulet,"
    "Dash Ring,Hit Ring,Power Ring,Magic Ring,Wall Ring,SilverErng,Gold Erng,SilverStud,Gold Stud,SightScope,Charm Top,"
    "Rage Band,FrenzyBand,Third Eye,Wallet,GreenDream,Berserker,PowerScarf,Speed Belt,Black Rock,Blue Rock,SilverRock,"
    "White Rock,Gold Rock,Hero Medal,MuscleRing,Flea Vest,Magic Seal,Power Seal,Relic,SeraphSong,Sun Shades,"
    "PrismSpecs,,Tonic,Mid Tonic,Full Tonic,Ether,Mid Ether,Full Ether,Elixir,HyperEther,MegaElixir,Heal,Revive,"
    "Shelter,Power Meal,Lapis,Barrier,Shield,Power Tab,Magic Tab,Speed Tab,Petal,Fang,Horn,Feather,Seed,Bike Key,"
    "Pendant,Gate Key,PrismShard,C. Trigger,Tools,Jerky,DreamStone,Race Log,Moon Stone,Sun Stone,Ruby Knife,Yakra Key,"
    "Clone,Toma's Pop,2 Petals,2 Fangs,2 Horns,2 Feathers"
)

ITEMS = {name: i for i, name in enumerate(ITEM_NAMES.split(',')) if name}
ITEM_IDS = invert(ITEMS)


class String(SimpleField[str]):

    def __init__(self, offset: int):
        super().__init__(offset=offset, size=6, codec=None)

    def decode_from_bytes(self, source: bytes) -> str:
        def to_char(byte):
            if byte == 0 or byte == 0xff:
                return ""
            if byte < 0xA0:
                return "?"
            if byte < 0xBA:
                return chr(byte - 95)
            if byte <= 0xD3:
                return chr(byte - 89)
            if byte <= 0xDD:
                return chr(byte - 164)
            return "?"

        return "".join(map(to_char, source))


class ChronoCharacter(Character):
    parent: "ChronoState"

    cur_hp: int = Short(0x2603)
    max_hp: int = Short(0x2605)
    cur_mp: int = Short(0x2607)
    max_mp: int = Short(0x2609)

    # all stats are max 99
    power: int = Byte(0x260B)
    stamina: int = Byte(0x260C)
    speed: int = Byte(0x260D)
    magic: int = Byte(0x260E)
    hit: int = Byte(0x260F)
    evade: int = Byte(0x2610)
    defense: int = Byte(0x2611)

    helmet: str = Mapped(0x2627, ITEMS)
    armor: str = Mapped(0x2628, ITEMS)
    weapon: str = Mapped(0x2629, ITEMS)
    relic: str = Mapped(0x262A, ITEMS)

    tech_points_needed = Short(0x262d)
    level = Byte(0x2612)
    _experience = Int24(0x2613)
    _exp_needed = Short(0x262b)

    # uses a special decoding table
    name: str = String(0x2c23)

    @property
    def default_name(self) -> str:
        return NAMES[self.index]

    @property
    def tech_level(self) -> int:
        return count_bits_set(getattr(self.parent, '_tech_levels')[self.index])

    @property
    def has_magic(self) -> bool:
        return getattr(self.parent, '_magic_flags') & (1 << self.index)

    def __seek__(self, offset: int, attribute_name: str = None) -> int:
        if attribute_name == 'name':
            return offset + self.index * 6
        return super().__seek__(offset, attribute_name)

    def learn_tech(self):
        if self.tech_level == 8:
            log.warning("character %s already learned all techs", self.name)
            return False
        next_tech = self.tech_level + 1
        if not self.has_magic and next_tech >= MAGIC_REQUIRED[self.index]:
            log.warning("character %s needs to learn magic first", self.name)
            return False
        self.tech_points_needed = 1
        return True

    def display(self, *, verbose: bool = False):
        xp_left = f", for next level: {self._exp_needed}" if verbose else ""
        name = f"{self.name} [{self.default_name}]" if verbose and self.default_name != self.name else self.name
        print(f"{name}: level {self.level}, {self.cur_hp}/{self.max_hp} HP, "
              f"{self.cur_mp}/{self.max_mp} MP, {self._experience} XP{xp_left}")
        if verbose:
            magic = '*' if self.has_magic else ''
            if self.tech_level < 8:
                print(f" - techs learned: {self.tech_level}{magic}, {self.tech_points_needed} TP for next")
            else:
                print(f" - learned all techs{magic}")
            print(" - equipment:", ", ".join([self.weapon, self.helmet, self.armor, self.relic]))

    def level_up(self, *, verbose: bool = False):
        gain = self._exp_needed - 1
        self._experience += gain
        self._exp_needed = 1
        if verbose:
            print(f"{self.name}: gained {gain} XP to reach level {self.level + 1}")


class ChronoState(SaveState):
    rom_name = "Chrono Trigger"

    list_options = {
        'item-names': ITEMS
    }

    init_options = 'all_characters',
    all_characters: bool

    gold: int = Int24(0x2c53)
    silver_points: int = Byte(0x10052)
    triple_techs: set[str] = ByteSet(0x284d, elements=TRIPLE_TECHS)

    _double_techs: bytearray = ByteArray(0x283e, 15)
    _active_party: bytearray = ByteArray(0x2980, 3)
    _characters: Sequence[ChronoCharacter] = ChronoCharacter.array(PARTY_SIZE, step=0x50)
    _tech_levels: bytearray = ByteArray(0x2837, PARTY_SIZE)
    _inventory: dict[str, int] = ParallelArraysInventory(0x2400, 0x2500, elements=ITEM_IDS, capacity=0x100)
    _magic_flags: int = Byte(0x0101e0)
    _time: list[int] = BCDArray(0x402, length=4)

    @property
    def party(self) -> Sequence[str]:
        return [NAMES[i] for i in self._active_party if i <= PARTY_SIZE]

    @property
    def play_time(self):
        time = "".join(map(str, reversed(self._time)))
        return time[:2] + ":" + time[2:]

    @property
    def characters(self) -> Sequence[ChronoCharacter]:
        if self.all_characters:
            return list(self._characters)
        return [self._characters[i] for i in self._active_party if i <= PARTY_SIZE]

    @classmethod
    def configure_parser(cls, parser: GroupedParser):
        parser.add_argument('--all-characters', action='store_true', default=False,
                            help='display and modify all characters instead of active party')

    def display(self, verbose=False):
        print(f"Chrono Trigger: {self.gold} gold, {self.silver_points} silver points, played {self.play_time}h")
        if verbose and self.triple_techs:
            print("Triple Techs unlocked:", ",".join(self.triple_techs))
        for c in self.characters:
            c.display(verbose=verbose)

    @action(name='gold')
    def add_gold(self, gold: int):
        """adds GOLD to party money"""
        self.gold += gold

    @action(short_name='S', name='silver-points')
    def add_silver_points(self, points: int, *, verbose: bool = False):
        """adds POINTS silver points for the millennial fare"""
        self.silver_points += points
        if verbose:
            print(f"added {points} silver points, total = {self.silver_points}")

    @action
    def heal(self):
        """heals selected characters: those in party unless using --all-characters"""
        for c in self.characters:
            c.heal()

    @action(short_name=None, modifies=True)
    def level_up(self, *, verbose: bool = False):
        """adds XP to all characters so only 1 more is needed to reach the next level"""
        for c in self.characters:
            c.level_up(verbose=verbose)

    @action(modifies=False)
    def inventory(self):
        """show the party inventory"""
        for item, amount in self._inventory.items():
            print(f"{amount:02} x {item}")

    def get_character(self, name: str) -> ChronoCharacter:
        try:
            search = name.lower()
            return next((c for c in self.characters
                         if search in (c.name.lower(), c.default_name.lower())))
        except StopIteration:
            raise CommandLineError('unknown character or not in party: ' + name)

    @action(short_name=None)
    def next_tech(self, name: str):
        """lets the character NAME learn a new tech by setting required TP to 1"""
        self.get_character(name).learn_tech()

    @subcommand(name='add', modifies=True, help={
        'item': 'the item to add',
        'count': 'number of items to add [default:1]'})
    def add_item(self, item: str, count: int = 1):
        """add one or more of an item to the inventory"""
        if item not in ITEMS:
            raise CommandLineError('unknown item: ' + item)
        self._inventory[item] = self._inventory.get(item, 0) + count

    @subcommand(name='remove', modifies=True, help={'item': 'the item to remove'})
    def remove_item(self, item: str, *, verbose: bool = False):
        """removes all instances of ITEM from inventory"""
        if item not in ITEMS:
            raise CommandLineError('unknown item: ' + item)
        if item not in self._inventory:
            if verbose:
                print(item, "not in inventory")
        else:
            current = self._inventory[item]
            del self._inventory[item]
            if verbose:
                print("removed", current, "x", item)


if __name__ == "__main__":
    ChronoState.main()
