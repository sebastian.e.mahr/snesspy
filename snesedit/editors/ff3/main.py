from typing import List, Set

from snesedit.cli import action
from snesedit.editors.ff3.data import NAMES, ITEM_IDS, ITEMS, FF3String, RARE_ITEMS
from snesedit.fields import Int24, Byte, ByteArray, Short, BitSet, Mapped, ParallelArraysInventory
from snesedit.savestate import SaveState, Character

# lots of offsets:
# https://datacrystal.romhacking.net/wiki/Final_Fantasy_VI:RAM_map

STATUS = ["Darkness", "Zombie", "Poison", "[MagiTek]", "Invisible", "Imp", "Stone", "Dead"]


class FF3Character(Character):
    _id1: int = Byte(0x1600)
    _name: str = FF3String(0x1602)

    level: int = Byte(0x1608)

    cur_hp: int = Short(0x1609)
    max_hp: int = Short(0x160b)
    cur_mp: int = Short(0x160d)
    max_mp: int = Short(0x160f)

    vigor: int = Byte(0x161A)
    speed: int = Byte(0x161B)
    stamina: int = Byte(0x161C)
    magic_power: int = Byte(0x161D)

    experience: int = Int24(0x1611)

    _esper = Byte(0x161e)
    right_hand: str = Mapped(0x161F, ITEM_IDS)
    left_hand: str = Mapped(0x1620, ITEM_IDS)
    head: str = Mapped(0x1621, ITEM_IDS)
    body: str = Mapped(0x1622, ITEM_IDS)
    relic1: str = Mapped(0x1623, ITEM_IDS)
    relic2: str = Mapped(0x1624, ITEM_IDS)

    _status1: Set[str] = BitSet(0x1614, elements=STATUS)
    _status2: int = Byte(0x1615)

    @property
    def id(self) -> int:
        return self._id1 & 0x0f

    @property
    def active(self) -> bool:
        return (self._id1 & 0xf0) == 0

    @property
    def status(self):
        return "Normal" if not self._status1 else ",".join([s for s in STATUS if s in self._status1])

    def heal(self):
        super().heal()
        self._status1.clear()

    @property
    def name(self) -> str:
        return NAMES[self.index]

    def display(self, *, verbose: bool = False):
        print(f"{self._name}: Level {self.level}, {self.cur_hp}/{self.max_hp} HP, {self.cur_mp}/{self.max_mp} MP "
              f"({self._id1:02x}) , Status: {self.status}, Esper: {self._esper}")
        if verbose:
            print(f" - Equipment: RH={self.right_hand}")


class FF3State(SaveState):
    rom_name = "Final Fantasy III"

    gold: int = Int24(offset=0x1860)
    steps: int = Int24(offset=0x1866)
    _party: bytearray = ByteArray(0x69, length=4)
    _active_party_id: int = Byte(0x1a5d)
    gained_characters: set[str] = BitSet(0x1ede, NAMES)
    _characters: List[FF3Character] = FF3Character.array(length=len(NAMES), step=0x25)
    inventory: dict[str, int] = ParallelArraysInventory(keys_offset=0x1869, values_offset=0x1969, capacity=0x100,
                                                        empty_value=0xff, elements=ITEMS)
    rare_items: set[str] = BitSet(0x1EBA, RARE_ITEMS)

    @property
    def characters(self) -> List[FF3Character]:
        return [c for c in self._characters if c.active]

    def display(self, verbose=False):
        print(f"FF3 - {self.gold} GP, {self.steps} steps")
        print("Party:", self._party.hex(' ', 1), "- Active", self._active_party_id,
              "- Gained:", ','.join(self.gained_characters))
        for c in self.characters:
            c.display(verbose=verbose)

    @action
    def heal(self):
        """heals all characters"""
        for c in self.characters:
            c.heal()


if __name__ == "__main__":
    FF3State.main()
