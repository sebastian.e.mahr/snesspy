import sys
from argparse import SUPPRESS
from typing import List, Dict, Mapping, Tuple

from snesedit.cli import action, subcommand, CommandLineError, GroupedParser
from snesedit.editors.sd3.types import Sd3String, Spells, Equipment
from snesedit.fields import ByteArray, Integer, Short, Byte, SquareEnixInt, Array, ParentField, \
    PositionalInventory, ParallelArraysInventory
from snesedit.savestate import SaveState, Character
from . import data

GROUP_INVENTORY = 'Inventory Management'

SHORT_ATTRIBUTES = "STR, AGL, VIT, INT, PIE, LUC".split(", ")
ATTRIBUTES = "strength", "agility", "vitality", "intelligence", "spirit", "luck"

MAX_SPELLS = 12


def equip_field_args(items: Mapping[int, str]):
    inverted = {v: k for k, v in items.items()}
    return dict(elements=inverted, length=7, nothing='Nothing')


class Sd3Character(Character):
    parent: "Sd3State"
    name: str = ParentField('character_names')
    cur_hp: int = Short(0x1f0f2)
    cur_mp: int = Short(0x1f0f4)
    max_hp: int = Short(0x1f102)
    max_mp: int = Short(0x1f104)
    _level: int = Byte(0x1f111)
    experience: int = SquareEnixInt(0x1f0fd)

    _extra_weapons: list[str] = Equipment(0x1f0f2 + 0x6f, elements=data.WEAPONS)
    _extra_helmets: list[str] = Equipment(0x1f0f2 + 0x7f, elements=data.HELMETS)
    _extra_armors: list[str] = Equipment(0x1f0f2 + 0x87, elements=data.ARMORS)
    _extra_gauntlets: list[str] = Equipment(0x1f0f2 + 0x8f, elements=data.GAUNTLETS)
    _extra_shields: list[str] = Equipment(0x1f0f2 + 0x97, elements=data.SHIELDS)

    strength: int = Byte(0x1edf6 + 0x300)
    agility: int = Byte(0x1edf7 + 0x300)
    vitality: int = Byte(0x1edf8 + 0x300)
    intelligence: int = Byte(0x1edf9 + 0x300)
    spirit: int = Byte(0x1edfa + 0x300)
    luck: int = Byte(0x1edfb + 0x300)

    # first byte: character index
    # second byte: number of class changes
    # third byte: alignment (light / dark / mixed)
    _class: Tuple[int, int, int] = ByteArray(offset=0x1f113, length=3)

    _spell_ids: list[int] = ByteArray(offset=0x1f1a5, length=12)
    _spell_targets: list[int] = ByteArray(offset=0x1f1b1, length=12)

    spells: Dict[str, data.SpellTarget] = Spells()

    BASE_NAMES = ["Duran", "Kevin", "Hawk", "Angela", "Carlie", "Lise"]

    @property
    def level(self) -> int:
        return self._level + 1

    @property
    def archetype(self) -> int:
        return self._class[0]

    @property
    def base_name(self) -> str:
        return self.BASE_NAMES[self.archetype]

    @property
    def class_name(self) -> str:
        return data.get_class_name(*self._class)

    def __seek__(self, offset: int, attribute_name: str = None) -> int:
        # somehow our offsets are off by one iteration (sometimes??)
        if not self.parent.shift:
            return offset + self._step * (self.index - 1)
        return super().__seek__(offset, attribute_name)

    @action(default=True, modifies=False)
    def display(self, *, verbose: bool = False):
        """Show character details"""
        print(f"{(self.name + ':'):7} Level {self.level:2} {self.class_name}, "
              f"{self.cur_hp:3}/{self.max_hp:3} HP, {self.cur_mp:2}/{self.max_mp:2} MP {self.experience} XP")
        if verbose:
            attrs = " ".join([f"{label} {getattr(self, name)}" for label, name in zip(SHORT_ATTRIBUTES, ATTRIBUTES)])
            print("       ", attrs)
            print("       ", ", ".join(("%s (%s) " % entry for entry in self.spells.items())))
            for slot in data.BEST_RESOLVERS:
                items = getattr(self, f"_extra_{slot}")
                if items:
                    print(f"        Extra {slot}:", ", ".join(items))

    @property
    def attributes(self) -> Mapping[str, int]:
        return {name: getattr(self, name) for name in ATTRIBUTES}

    @action
    def heal(self, *, verbose: bool = False):
        """heals this character"""
        if verbose:
            print("healed", self.name)
        super().heal()

    @action(name='experience', modifies=True)
    def add_experience(self, xp: int):
        """adds XP experience points to this character"""
        self.experience += xp

    @action(modifies=True)
    def best(self, *, verbose: bool = False):
        """adds the best equipment for the current class."""
        if verbose:
            print("adding best equipment for class", self.class_name)
        for slot in data.BEST_RESOLVERS:
            item_type = slot[:-1]
            item_id, item = data.get_best_item(slot, *self._class)
            if item_id:
                target = getattr(self, '_extra_' + slot)
                if len(target) >= 7:
                    print(f"cannot add {item_type}, no more storage capacity", file=sys.stderr)
                else:
                    if verbose:
                        print(f"adding {item_type}: {item}")
                    target.append(item)

    @action(modifies=True, short_name=None)
    def revert_class_change(self, *, verbose: bool = False):
        """reverts the latest class change, clearing all spells but preserving attribute values"""
        archetype, class_level, alignment = self._class
        if not class_level:
            raise CommandLineError("already at base class: " + self.class_name)
        new_class_level, new_alignment = data.revert_class_change(class_level, alignment)
        old_class = self.class_name
        self.spells.clear()
        self._class = archetype, new_class_level, new_alignment
        if verbose:
            print(f"changed {self.name}'s class from {old_class} to {self.class_name}")

    @action(short_name=None, modifies=True)
    def spell_profile(self, profile: str, *, verbose: bool = False):
        """Load a spell profile, replacing all spells with the one from the profile"""
        if profile not in data.SPELL_PROFILES:
            known = ", ".join(data.SPELL_PROFILES.keys())
            raise CommandLineError("unknown spell profile: " + profile + ", must be one of " + known)
        self.spells.clear()
        for spell, target in data.SPELL_PROFILES[profile].items():
            if verbose:
                print(f"adding spell {spell} ({target})")
            self.spells[spell] = data.SpellTarget.value_of(target)

    @subcommand(name='addSpell', modifies=True, help={
        'spell': 'the spell to add. see --list-spell-names for a list of possible values',
        'target': 'how the spell targets the enemies/party. default: S (single), some special spells their own default'
    })
    def add_spell(self, spell: str, target: str = None, *, verbose: bool = False):
        """Adds a spell to character or changes spell targeting"""
        if spell not in data.SPELL_IDS:
            raise CommandLineError('unknown spell:' + spell)
        if target is None:
            target = data.DEFAULT_TARGETS[spell]
        try:
            flag = data.SpellTarget.value_of(target)
        except ValueError:
            raise CommandLineError('unknown spell target ' + target + '. must be one of S, M, C, A')
        if flag == data.SpellTarget.NONE:
            raise CommandLineError('illegal spell target: use --remove-spell to')
        if spell in self.spells:
            if self.spells[spell] != flag:
                if verbose:
                    print(f"updating target of {spell} to {target}")
                self.spells[spell] = flag
        else:
            if len(self.spells) >= MAX_SPELLS:
                raise CommandLineError('no empty spell slots')
            if verbose:
                print(f"adding spell {spell} ({str(flag)})")
            self.spells[spell] = flag

    @subcommand(name='removeSpell', modifies=True, help={
        'spell': 'the spell to remove from the characters magic menu'
    })
    def remove_spell(self, spell: str):
        """removes spell from character"""
        self.spells.pop(spell, None)

    @subcommand(name='set', modifies=True)
    def set_attribute(self, attribute: str, value: int, *, verbose: bool = False):
        """set an attribute value for this character"""
        if attribute not in ATTRIBUTES:
            raise CommandLineError("unknown attribute '" + attribute + "'. Must be one of " + ", ".join(ATTRIBUTES))
        if not 1 <= value <= 99:
            raise CommandLineError("attribute value must be in [1, 99]")
        current = getattr(self, attribute)
        if verbose:
            print("setting attribute", attribute, current, "->", value, "for", self.name)
        setattr(self, attribute, value)

    @subcommand(name='addEquipment', modifies=True, help={'item': 'The equipment to add'})
    def add_equipment(self, item: str, *, verbose: bool = False):
        """Adds an equipment item to the appropriate slot"""
        equip_type = data.get_equip_type(item)
        if not equip_type:
            raise CommandLineError('not a valid equipment item: ' + str)
        if verbose:
            print("adding", equip_type, item)
        attr_name = f"_extra_{equip_type}s"
        getattr(self, attr_name).append(item)


class Sd3State(SaveState):
    rom_name = "Seiken Densetsu III (DeJAP)"

    list_options = {
        "character-names": Sd3Character.BASE_NAMES,
        ("items", GROUP_INVENTORY): data.ITEMS,
        "spell-names": data.SPELL_NAMES.values()
    }

    init_options = ["shift"]

    argument_groups = {
        "Inventory Management": "Modify the parties inventory and storage"
    }

    gold: int = Integer(offset=0x2c24)
    _characters: List[Sd3Character] = Sd3Character.array(step=0x300, length=3)
    character_names: List[str] = Array(offset=0x2c00, length=3, chunk_size=12, codec=Sd3String())
    storage: Dict[str, int] = PositionalInventory(offset=0x2e39, keys=data.ITEMS[1:])
    inventory: Dict[str, int] = ParallelArraysInventory(keys_offset=0x2ee9, values_offset=0x2ef3,
                                                        elements=data.ITEMS, capacity=10)

    # this field is passed as an init option to __init__ and set as an instance attribute,
    # it corresponds to the '--shift' added in #configure_parser()
    shift: bool

    @property
    def characters(self) -> List[Sd3Character]:
        return [c for c in self._characters if c.experience or c.index == 0]

    def display(self, verbose=False):
        print(f"{self.rom_name} state, {self.gold} gold")
        for c in self.characters:
            c.display(verbose=verbose)

    @classmethod
    def configure_parser(cls, parser: GroupedParser):
        parser.add_argument('--shift', action="store_true", inherit=True,
                            help="read character data as shifted (0x300), sometimes "
                                 "the game just does that for no obvious reason")

    @action(name='inventory', modifies=False, group=GROUP_INVENTORY)
    def show_inventory(self):
        """Show the items in ring menu inventory"""
        print(" === Inventory: ===")
        for item, count in self.inventory.items():
            if count:
                print('%2d x %s' % (count, item))

    @action(name='storage', modifies=False, group=GROUP_INVENTORY)
    def show_storage(self):
        """Show items in party storage"""
        print(" === Storage: ===")
        for item, count in self.storage.items():
            if count:
                print('%2d x %s' % (count, item))

    @action(name='gold')
    def add_gold(self, gold: int):
        """add GOLD amount to party"""
        self.gold += gold

    @action(modifies=False, help=SUPPRESS, short_name=None)
    def best_equipment_for(self, class_name: str):
        class_ids = data.get_class_id(class_name)
        print(f"{class_name}:", " - ".join(map(str, class_ids)))
        for slot in data.BEST_RESOLVERS:
            item = data.get_best_item(slot, *class_ids)
            print(slot[:-1], "->", item)

    @subcommand(name='add', modifies=True, short_opts={'verbose': 'v', 'inventory': 'i'}, help={
        'item': 'the name of an item to add',
        'count': 'number of items to add, default: 1',
        'verbose': 'generate more output',
        'fill': 'ignore count and max out items (99)',
        'inventory': 'add item to inventory instead of storage'
    })
    def add_item(self, item: str, count: int = 1, *, fill: bool = False,
                 inventory: bool = False, verbose: bool = False):
        """add an item to storage"""
        if item not in data.ITEMS:
            raise ValueError('unknown item: %s' % item)
        if inventory and len(self.inventory) >= 10:
            inventory = False
            print("inventory is full, adding to storage")
        max_count, target = (9, "inventory") if inventory else (99, "storage")
        if fill:
            count = max_count
        item_count = max(1, min(count, max_count))
        if verbose:
            print(f"setting {item} -> {item_count} in {target}")
        getattr(self, target)[item] = item_count

    @subcommand(actions=True)
    def character(self, name: str) -> Sd3Character:
        """Display and modify a single character"""
        name = name.lower()
        for c in self.characters:
            if name == c.name.lower() or name == c.base_name.lower() or name == str(c.index):
                return c
        raise CommandLineError("unknown character " + name)

    @action(name='claws', short_name=None, group=GROUP_INVENTORY)
    def add_claws(self, *, verbose: bool = False):
        """add up to 30 of all claw items to storage"""
        claws = [item for item in data.ITEMS if item.endswith('Claw')]
        for critter in claws:
            if verbose:
                to_add = 30 - self.storage.get(critter)
                if critter > 0:
                    print(f"adding {to_add} x {critter}")
            self.storage[critter] = min(self.storage.get(critter), 30)

    @action(group=GROUP_INVENTORY)
    def refill(self, *, verbose: bool = False):
        """Refill all consumable (non-unique) items in inventory"""
        for item in self.inventory:
            if item not in data.UNIQUE_ITEMS and item not in data.DUMMY_ITEMS:
                if verbose and self.inventory[item] < 9:
                    print("refilling:", item)
                self.inventory[item] = 9

    @action
    def heal(self, *, verbose=True):
        """heal all characters"""
        for c in self.characters:
            c.cur_mp = c.max_mp
            c.cur_hp = c.max_hp
            if verbose:
                print(f'healed {c.name}: {c.cur_hp}/{c.max_hp} HP {c.cur_mp}/{c.max_mp} MP')

    @action(name='experience')
    def add_experience(self, n: int):
        """add N experience points to all characters"""
        for c in self.characters:
            c.experience += n


if __name__ == '__main__':
    Sd3State.main()
