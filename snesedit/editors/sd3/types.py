import logging
from typing import Tuple, Mapping

from snesedit.codecs import Codec
from snesedit.fields import ParallelArraysInventory, SimpleTable, E
from .data import SPELL_NAMES, SpellTarget

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


class Sd3String(Codec[str]):

    def __init__(self, max_length=6):
        super(Sd3String, self).__init__()
        self.max_length = max_length

    def encode_to_bytes(self, value: str) -> bytes:
        if len(value) > self.max_length:
            raise ValueError("max length: %d" % self.max_length)
        bytes_ = value.encode('ascii', errors='replace')
        result = bytes(2 * len(value))
        for i, b in enumerate(bytes_):
            result[2 * i] = b

    def decode_from_bytes(self, source: bytes) -> str:
        stripped = [source[i] for i in range(0, len(source), 2) if source[i] != 0]
        return str(bytes(stripped), encoding='ascii')


class Spells(ParallelArraysInventory[SpellTarget]):

    def __init__(self):
        super(Spells, self).__init__(keys_offset=0x1f1a5, values_offset=0x1f1b1, capacity=12, elements=SPELL_NAMES)

    def decode_entry(self, element_id: int, value: int) -> Tuple[str, SpellTarget]:
        if value > SpellTarget.CASTER:
            value = 0
        return SPELL_NAMES.get(element_id, '???'), SpellTarget(value)


class Equipment(SimpleTable[str]):

    def __init__(self, offset: int, elements: Mapping[int, str]):
        super(Equipment, self).__init__(offset=offset, length=7, elements={v: k for k, v in elements.items()},
                                        nothing='Nothing', empty_slot=0, bytes_per_slot=1)
