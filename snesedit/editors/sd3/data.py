from collections import defaultdict
from enum import IntEnum
from typing import Tuple

ITEMS = [
    "Nothing",
    "Paladin's Proof",
    "Lord's Proof",
    "Master's Proof",
    "Duelist's Proof",
    "Gold Wolf Soul",
    "Silver Wolf Soul",
    "Death Wolf Soul",
    "Demon Wolf Soul",
    "Good Luck Die",
    "Bad Luck Die",
    "Bullseye Die",
    "Nighteye Die",
    "Arcane Book",
    "Book of Secrets",
    "Book of Rune",
    "Forbidden Book",
    "Holy Water Vial",
    "Bottle of Salt",
    "Bottle of Ashes",
    "Bottle of Blood",
    "Briesingamen",
    "MorningStarChain",
    "KnightDrgn Chain",
    "Gleipnir",
    "Item Seed",
    "Mysterious Seed",
    "Flying Item Seed",
    "Magic Seed",
    "??? Seed",
    "Weapon/Armor Seed",
    "Earth Coin",
    "Gnome Statue",
    "Basilisk's Fang",
    "Bulette's Scale",
    "Needlion's Eye",
    "Molebear's Claw",
    "Storm Coin",
    "Jinn Statue",
    "Harpy's Fang",
    "Bird's Scale",
    "Bee's Eye",
    "Siren's Claw",
    "Ice Coin",
    "Undine Statue",
    "WhiteDragon Fang",
    "Sahagin's Scale",
    "Slime's Eye",
    "Poseidon's Claw",
    "Flame Coin",
    "Salamando Statue",
    "Fire Lizard Fang",
    "Drake's Scale",
    "Battum's Eye",
    "Kerberos' Claw",
    "Darkness Coin",
    "Shade Statue",
    "Ghost's Eye",
    "Specter's Eye",
    "Shadowzero's Eye",
    "Demon's Claw",
    "Light Coin",
    "Wisp Statue",
    "Poto Oil",
    "Pakkun Oil",
    "Mama Poto Oil",
    "Papa Poto's Claw",
    "Moon Coin",
    "Luna Statue",
    "ChibiDevil's Eye",
    "Porobin Oil",
    "Wolf Devil Oil",
    "Carmilla's Claw",
    "Myconid's Eye",
    "Assassin Bug Eye",
    "Dryad Statue",
    "Grell Oil",
    "Matango Oil",
    "Crawler's Claw",
    "Hand Axe",
    "Shuriken",
    "Dart",
    "Pumpkin Bomb",
    "Round Drop",
    "Pakkun Chocolate",
    "Magic Walnut",
    "Honey Drink",
    "Puipui Grass",
    "Stardust Herb",
    "Angel's Grail",
    "Magical Rope",
    "Gunpowder",  # U -> unique
    "Chibikko Hammer",  # U
    "Moogle Badge",  # U
    "Pihyara Flute",  # u
    "Wind Drum",  # U
    "Dreamsee Herb",
    "Half Key A (dummy)",  # U
    "Half Key B (dummy)",  # U
    "Mystery Key (dummy)",  # U
    "Dragon's Eye (dummy)",
    "Illusion Mirror",  # U
    "Demon Statue (dummy)",
]

UNIQUE_ITEM_IDS = [91, 92, 93, 94, 95, 101]
DUMMY_ITEM_IDS = [97, 98, 99, 100, 102]
UNIQUE_ITEMS = [ITEMS[i] for i in UNIQUE_ITEM_IDS]
DUMMY_ITEMS = [ITEMS[i] for i in DUMMY_ITEM_IDS]

BASE_SPELL_NAMES = [
    "(no spell)", "Diamond Missile", "Earthquake", "Stone Cloud", "Protect Up", "Speed Down", "Diamond Saber",
    "Air Blast", "Thunderstorm", "Stun Wind", "Speed Up", "Protect Down", "Thunder Saber", "Ice Smash", "Mega Splash",
    "Cold Blaze", "Mind Up", "Power Down", "Ice Saber", "Fireball", "Explode", "Blaze Wall", "Power Up", "Mind Down",
    "Flame Saber", "Evil Gate", "Dark Force", "DeathSpell", "AntiMagic", "Black Curse", "Dark Saber", "Holy Ball",
    "Saint Beam", "Heal Light", "Magic Shield", "Tinkle Rain", "Saint Saber", "Lunatic", "Half Vanish", "Body Change",
    "Life Booster", "Energy Ball", "Moon Saber", "Sleep Flower", "Poison Bubble", "Trans Shape", "Aura Wave",
    "Counter Magic", "Leaf Saber", "Detect (dummy)", "Turn Undead", "Demon Breath", "Rainbow Dust", "Ancient",
    "Ancient 2 (dummy)",
    "Ancient 3 (dummy)", "Freya", "Marduke", "Iormungand", "Lamian Naga", "Unicorn Head", "Machine Golem", "Ghoul",
    "Ghost", "Gremlin", "Great Demon", "Earth Jutsu", "Thunder Jutsu", "Water Jutsu", "Fire Jutsu", "Arrow",
    "Spike", "Rock Fall", "Land Mine", "Poison Breath", "Flame Breath", "Blow Needles", "Deadly Weapon", "Double Spell",
    "Pressure Point"]

SPELL_IDS = {name: spell_id for spell_id, name in enumerate(BASE_SPELL_NAMES) if "dummy" not in name}

# most spell ids > 0x50 are monster or special attacks,
# but some of Hawk's spells are usable:
SPELL_IDS["Shuriken"] = 0x7B
SPELL_IDS["Silver Dart"] = 0x7E
SPELL_IDS["Cutter Missile"] = 0x7F
SPELL_IDS["Crescent"] = 0x80
SPELL_IDS["Rocket Launcher"] = 0x81
SPELL_IDS["Axe Bomber"] = 0x82

SPELL_NAMES = {spell_id: name for name, spell_id in SPELL_IDS.items()}

SPELL_TARGET_LABELS = '-', 'S', 'A', 'M', 'C'


class SpellTarget(IntEnum):
    NONE = 0
    SINGLE = 1
    ALL = 2
    MULTIPLE = 3
    CASTER = 4

    @classmethod
    def value_of(cls, label: str) -> 'SpellTarget':
        try:
            return cls(SPELL_TARGET_LABELS.index(label.upper()))
        except IndexError:
            raise ValueError(label)

    def __str__(self):
        return SPELL_TARGET_LABELS[self]

    __repr__ = __str__


SPELL_PROFILES = {
    "ubermage": {
        # 6 multitarget level 2 elemental
        "Saint Beam": "M",
        "Dark Force": "M",
        "Earthquake": "M",
        "Thunderstorm": "M",
        "Explode": "M",
        "Mega Splash": "M",
        # 4 single target level 3 elemental
        "Stone Cloud": "S",
        "Stun Wind": "S",
        "Cold Blaze": "S",
        "Blaze Wall": "S",
        # two uber spells:
        "Rainbow Dust": "A",
        "Ancient": "A",
    },
    "uberknight": {
        # all the sabres
        "Diamond Saber": "M",
        "Thunder Saber": "M",
        "Ice Saber": "M",
        "Flame Saber": "M",
        "Saint Saber": "M",
        "Dark Saber": "M",
        "Moon Saber": "M",
        "Leaf Saber": "M",
        # paladin stuff:
        "Heal Light": "M",
        "Tinkle Rain": "M",
        # why not:
        "Aura Wave": "S",
    }
}

for profile, spells in SPELL_PROFILES.items():
    if len(spells) > 12:
        raise ValueError(f"spell profile {profile}: too many spells")
    for spell, target_code in spells.items():
        if spell not in SPELL_IDS:
            raise ValueError(f"unknown spell {spell} for spell profile {profile}")
        try:
            SpellTarget.value_of(target_code)
        except ValueError:
            raise ValueError(f"for spell {spell} in profile {profile}: bad spell target: {target_code}")

DEFAULT_TARGETS = defaultdict(lambda: 'S', {
    'Ancient': 'A',
    'Pressure Point': 'C',
    'Double Spell': 'A',
    'Rainbow Dust': 'A',
    "Freya": 'A',
    "Marduke": 'A',
    "Iormungand": 'A',
    "Lamian Naga": 'A'
})


def best_weapon(class_level, class_type):
    if 0 == class_level:
        return 22
    if 1 == class_level:
        return 25
    return 26 + class_type


WEAPONS = {
    # SWORDS
    0x01: "Bronze Sword",  # N
    0x02: "Iron Sword",  # N
    0x03: "Gladius",  # N
    0x04: "Broadsword",  # N
    0x05: "Sabre",  # N
    0x06: "Steel Sword",  # N
    0x07: "Bastard Sword",  # N
    0x08: "Silver Blade",  # N
    0x09: "Estoc",  # L
    0x0A: "Falchoin",  # D
    0x0B: "Flamberge",  # N
    0x0C: "Colichemarde",  # L
    0x0D: "Shamshir",  # D
    0x0E: "Palestorm",  # N
    0x0F: "Mythril Sword",  # L
    0x10: "Katzbalger",  # D
    0x11: "Balmunk",  # N
    0x12: "Valar Sword",  # L
    0x13: "Kusanagi Blade",  # D
    0x14: "Tyrving",  # N
    0x15: "Defender",  # L
    0x16: "Muramasa Blade",  # D
    0x17: "Krau-Solas",  # N -> best neutral!
    0x18: "Excalibur",  # L
    0x19: "Levatein",  # D
    0x1A: "Dragonbane",  # WL/WD -> both classes with one class change
    0x1B: "Brave Blade",  # LL -> light + light -> Paladin
    0x1C: "Sigmund",  # LD -> light + dark  -> Lord
    0x1D: "Ragnarok",  # DL -> dark + light  -> Swordmaster
    0x1E: "Deathbringer",  # DD -> dark + dark    > Duelist
    # CLAWS:
    0x2B: "Leather Glove",
    0x2C: "Iron Knuckles",
    0x2D: "Needle Glove",
    0x2E: "Molebear Claw",
    0x2F: "Chain Glove",
    0x30: "Bagh Nakh",
    0x31: "Fiend's Claw",
    0x32: "Silverthorn",
    0x33: "Moogle Claw",
    0x34: "Keen Knuckle",
    0x35: "Power Glove",
    0x36: "Kaiser Knuckle",
    0x37: "Acid Claw",
    0x38: "Cyclone Claw",
    0x39: "Mythril Claw",
    0x3A: "Bone Knuckle",
    0x3B: "Kerberos Claw",
    0x3C: "Diamond Knuckle",
    0x3D: "Ghost Hand",
    0x3E: "Rock Claw",
    0x3F: "Gleam Glove",
    0x40: "Jug Puncher",
    0x41: "Vampire Claw",
    0x42: "Aura Glove",
    0x43: "Rotten Knuckle",
    0x44: "Dragon Claw",
    0x45: "Spiral Claw",
    0x46: "Holy Glove",
    0x47: "Skull Disect",
    0x48: "Gigas Glove",
    # KNIVES:
    0x55: "Flint Knife",
    0x56: "Dagger",
    0x57: "Baselard",
    0x58: "Roundel Dagger",
    0x59: "Sharkteeth",
    0x5A: "Steel Dagger",
    0x5B: "Misericorde",
    0x5C: "Katar",
    0x5D: "Main Gauche",
    0x5E: "Garuda",
    0x5F: "Crystal Dagger",
    0x60: "Elf Dagger",
    0x61: "Ashura",
    0x62: "Bluegale",
    0x63: "Mithril Knife",
    0x64: "Yasha",
    0x65: "Dancing Dagger",
    0x66: "Field Dagger",
    0x67: "Bishamon",
    0x68: "Merkiel Dagger",
    0x69: "Sylvan Knife",
    0x6A: "Taishaku",
    0x6B: "Sheol Dirk",
    0x6C: "Crescent Knife",
    0x6D: "Acala",
    0x6E: "Crimson Glare",
    0x6F: "Orihalcon",
    0x70: "Manslaughter",
    0x71: "Kongo Rakan",
    0x72: "Deathstroke",
    0x74: "??? Weapon",
    # RODS:
    0x7F: "Wooden Cane",
    0x80: "Staff",
    0x81: "Witch Staff",
    0x82: "Oak Pine",
    0x83: "Pewter Rod",
    0x84: "Ruby Cane",
    0x85: "Crystal Rod",
    0x86: "Soul Rod",
    0x87: "Varshu Staff",
    0x88: "Cunning Staff",
    0x89: "Ash Cane",
    0x8A: "Will Staff",
    0x8B: "Tot's Cane",
    0x8C: "Rajin's Cane",
    0x8D: "Mythril Rod",
    0x8E: "Skull Rod",
    0x8F: "Memyl Rod",
    0x90: "Druid Cane",
    0x91: "Revelation Cane",
    0x92: "Nebula Staff",
    0x93: "World Tree Branch",
    0x94: "Ancient Rod",
    0x95: "Mizunara Cane",
    0x96: "Eternal Rod",
    0x97: "Celnunnos Cane",
    0x98: "Ceryeceon",
    0x99: "Ganvantein",
    0x9A: "Spirit Cane",
    0x9B: "Rune Staff",
    0x9C: "Dragon Rod",
    # FLAILS:
    0xA9: "Hollow Rod",
    0xAA: "Wood Flail",
    0xAB: "Ball and Chain",
    0xAC: "Light Flail",
    0xAD: "Warhammer",
    0xAE: "Steel Maul",
    0xAF: "Duck Ironball",
    0xB0: "Silver Flail",
    0xB1: "Heavy Flail",
    0xB2: "Hammer Flail",
    0xB3: "Morning Star",
    0xB4: "Heiro Flail",
    0xB5: "Puppet Flail",
    0xB6: "Blockbuster",
    0xB7: "Mythril Maul",
    0xB8: "Troll Maul",
    0xB9: "Cuneal Maul",
    0xBA: "Holy Flail",
    0xBB: "Thibula Flail",
    0xBC: "Gravity Maul",
    0xBD: "Ultima Maul",
    0xBE: "Bloodsucker",
    0xBF: "Meteo Smash",
    0xC0: "Mjolnir",
    0xC1: "Satan Flail",
    0xC2: "Vertina Maul",
    0xC3: "Judgementes",
    0xC4: "Gigas Flail",
    0xC5: "Maul of the Dead",
    0xC6: "Juggernaut",
    # SPEARS:
    0xD3: "Bronze Lance",
    0xD4: "Long Spear",
    0xD5: "Flamea",
    0xD6: "Corseca",
    0xD7: "Partisan",
    0xD8: "Steel Lance",
    0xD9: "Glaive",
    0xDA: "Silver Lance",
    0xDB: "Wing Spear",
    0xDC: "Mystic Spear",
    0xDD: "Plune Lance",
    0xDE: "Dark Piercer",
    0xDF: "Torrento Spear",
    0xE0: "Golden Spear",
    0xE1: "Mythril Spear",
    0xE2: "Brainwrecker",
    0xE3: "Griffin Lance",
    0xE4: "Staghorn",
    0xE5: "Wolf's Fang",
    0xE6: "Maidenleaf",
    0xE7: "Valkyrie Spear",
    0xE8: "Mideel Spear",
    0xE9: "Brunak",
    0xEA: "Gungir",
    0xEB: "Paintooth",
    0xEC: "Blaze Piercer",
    0xED: "True Spear",
    0xEE: "Stargazer",
    0xEF: "Dragon Lance",
    0xF0: "Giant's Spear"
}


def best_helmet(class_level, class_type):
    if 0 == class_level:
        return 2
    if 1 == class_level:
        return 7
    return 8 + class_type


# See http://shrines.rpgclassics.com/snes/sd3/armor.shtml
HELMETS = {
    # Duran:
    0x01: "Leather Visor",  # N -> Any
    0x02: "Headgear",  # N
    0x03: "Studded Helm",  # N
    0x04: "Visored Helm",  # 1 -> any class with at least one CC
    0x05: "Horned Helm",  # 1
    0x06: "Silver Sallet",  # 1
    0x07: "Beryl Arnet",  # 1
    0x08: "Dragon Helm",  # 1
    0x09: "Hero's Crown",  # LL -> Paladin
    0x0A: "Protection Helm",  # LD -> Lord
    0x0B: "Rising Moon Helm",  # DL -> Swordmaster
    0x0C: "Skull Head",  # DD -> Duelist
    # Kevin:
    0x0D: "Bandana",
    0x0E: "Warrior Band",
    0x0F: "Headband",
    0x10: "Chakra Band",
    0x11: "Werewolf's Mane",
    0x12: "Majuu Mask",
    0x13: "Genjuu Mask",
    0x14: "Dragon's Mane",
    0x15: "Ivory Band",
    0x16: "Darkshine Band",
    0x17: "Sapphire Band",
    0x18: "Ruby Band",
    # Hawk:
    0x19: "Leather Hat",
    0x1A: "Garravilla",
    0x1B: "Feather Hat",
    0x1C: "Fearie Hat",
    0x1D: "Grizzly Garea",
    0x1E: "Black Hood",
    0x1F: "Noctogoggles",
    0x20: "Fool's Crown",
    0x21: "Wind Spirit Hat",
    0x22: "Silver Wolf Garea",
    0x23: "Stealth Hood",
    0x24: "Bloody Mask",
    # Angela:
    0x25: "Circlet",
    0x26: "Witch Hood",
    0x27: "Emerald Tiara",
    0x28: "Panther Hood",
    0x27: "Emerald Tiara",
    0x28: "Panther Hood",
    0x29: "Silver Circlet",
    0x2A: "White Snow Veil",
    0x2B: "Mist Veil",
    0x2C: "Moonstone Tiara",
    0x2D: "Myien Crown",
    0x2E: "Eremos Crown",
    0x2F: "Rune Veil",
    0x30: "Ancient Tiara",
    # Carlie:
    0x31: "Cat-ear Hood",
    0x32: "Rabite Hat",
    0x33: "Mog Cap",
    0x34: "Silk Robbin",
    0x35: "Parobin Hood",
    0x36: "Holy Spirit Hood",
    0x37: "Sunstone Hat",
    0x38: "Spiritus Robbin",
    0x39: "Bishop's Ribbon",
    0x3A: "Sage's Ribbon",
    0x3B: "Undead Ribbon",
    0x3C: "Bitium Ribbon",
    # Lise:
    0x3D: "Winged Ribbon",
    0x3E: "Leather Helmet",
    0x3F: "Barbute",
    0x40: "Viking Helm",
    0x41: "Horncrest",
    0x42: "Unicorn Helm",
    0x43: "Aurora Helm",
    0x44: "Pegasus Helmet",
    0x45: "Vanir Helmet",
    0x46: "Stardust Helmet",
    0x47: "Rising Dragon",
    0x48: "Wolf Helmet"
}


def best_armor(class_level, class_type):
    if 0 == class_level:
        return 6
    if 1 == class_level:
        return 15
    return 16 + class_type


ARMORS = {
    # Duran:
    0x7F: "Quilted Leather",  # N
    0x80: "Hard Leather",  # N
    0x81: "Bezant Mail",  # N
    0x82: "Lamellar Armor",  # N
    0x83: "Hauberk",  # N
    0x84: "Half Plate",  # N
    0x85: "Plate Mail",  # N
    0x86: "Lunula Mail",  # 1
    0x87: "Reflex",  # 1
    0x88: "Pegasus Armor",  # 1
    0x89: "Leanis Plate",  # 1
    0x8A: "Knight Armor",  # L
    0x8B: "Swordsman Armor",  # D
    0x8C: "Gold Armor",  # L
    0x8D: "Platinum Armor",  # D
    0x8E: "Dragon's Mail",  # 1
    0x8F: "Hero's Armor",  # LL
    0x90: "Protect Armor",  # LD
    0x91: "Master's Armor",  # DL
    0x92: "Skeleton Mail",  # DD
    # Keyvin:
    0x93: "Cotton Uniform",
    0x94: "Leather Belt",
    0x95: "Fur Vest",
    0x96: "Warrior Uniform",
    0x97: "Chain Vest",
    0x98: "Wolf Belt",
    0x99: "Protector",
    0x9A: "Beast Uniform",
    0x9B: "Lyshee Vest",
    0x9C: "Battlesuit",
    0x9D: "Amber Uniform",
    0x9E: "Red Uniform",
    0x9F: "Blue Uniform",
    0xA0: "Genjuu Belt",
    0xA1: "Majuu Belt",
    0xA2: "Scale Uniform",
    0xA3: "Byakko Uniform",
    0xA4: "Genbu Uniform",
    0xA5: "Seiryuu Uniform",
    0xA6: "Suzaku Uniform",
    # Hawk:
    0xA7: "Cotton Kilt",
    0xA8: "Black Fatigue",
    0xA9: "Camoflage Cloak",
    0xAA: "Thief's Cape",
    0xAB: "Soft Leather",
    0xAC: "Idaten Cloak",
    0xAD: "Chain Guard",
    0xAE: "Moonbeam Cloak",
    0xAF: "Elf Breastplate",
    0xB0: "Fleetwind Cape",
    0xB1: "Flourite Plate",
    0xB2: "Utsushimi Cape",
    0xB3: "Darksuit",
    0xB4: "Yafuku Cuirass",
    0xB5: "Ninga Garb",
    0xB6: "Shijima Mail",
    0xB7: "Phantom Cuirass",
    0xB8: "Silverwolf Pelt",
    0xB9: "Wind Demon Mail",
    0xBA: "Black Garb",
    # Angela:
    0xBB: "Cotton Robe",
    0xBC: "Silk Robe",
    0xBD: "Witch's Robe",
    0xBE: "Queen Bee Dress",
    0xBF: "Bat Coat",
    0xC0: "Tiger Bikini",
    0xC1: "Rose Leotard",
    0xC2: "Bunnydress",
    0xC3: "Owl Coat",
    0xC4: "Zephyr Robe",
    0xC5: "Mananan Robe",
    0xC6: "Sunrise Dress",
    0xC7: "Dusk Dress",
    0xC8: "Pure White Robe",
    0xC9: "Darkness Robe",
    0xCA: "Dreamdevil Coat",
    0xCB: "Myein Dress",
    0xCC: "Eremos Coat",
    0xCD: "Rune Coat",
    0xCE: "Ancient Robe",
    # Carlie:
    0xCF: "Chibikko Robe",
    0xD0: "Lamb Suit",
    0xD1: "Priest's Robe",
    0xD2: "Popoi's Rags",
    0xD3: "Sailor Dress",
    0xD4: "Poto Suit",
    0xD5: "Velvet Cape",
    0xD6: "Moogle Smock",
    0xD7: "Pakkum Suit",
    0xD8: "Glint Robe",
    0xD9: "Utsufishi Robe",
    0xDA: "Golden Robe",
    0xDB: "Silver Robe",
    0xDC: "Minister's Robe",
    0xDD: "Shrine Girl Robe",
    0xDE: "Kurikara Robe",
    0xDF: "Bishop's Robe",
    0xE0: "Sage's Robe",
    0xE1: "Undead",
    0xE2: "Bitium Dress",
    # Lise:
    0xE3: "Padded Leather",
    0xE4: "Cuir Boulli",
    0xE5: "Feather Vest",
    0xE6: "Spiked Leather",
    0xE7: "Chainmail",
    0xE8: "Banded Mail",
    0xE9: "Scale Mail",
    0xEA: "Mani Plate",
    0xEB: "Steda Plate",
    0xEC: "White Eagle Plate",
    0xED: "Jotzammoran",
    0xEE: "Valkrie Mail",
    0xEF: "Rune Armor",
    0xF0: "Wing Armor",
    0xF1: "Fang Armor",
    0xF2: "Phantasm Armor",
    0xF3: "Goddess Armor",
    0xF4: "Polaris Armor",
    0xF5: "Dragon Knight Armor",
    0xF6: "Wulfhezein"
}


def best_gauntlet(class_level, class_type):
    if 0 == class_level:
        return 2
    if 1 == class_level:
        return 6
    if class_type < 2:
        return 7
    return 8


GAUNTLETS = {
    # Duran:
    0x49: "Leather Gauntlet",  # N
    0x4A: "Steel Gauntlet",  # N
    0x4B: "Gauntlet",  # N
    0x4C: "Strength Armband",  # 1
    0x4D: "Knight's Crest",  # L
    0x4E: "Fencer's Armband",  # D
    0x4F: "Dragon Ring",  # 1
    0x50: "War King's Crest",  # L2 -> Any light second class (Paladin, Lord)
    0x51: "Master's Armband",  # D2 -> Any dark second class (Swordmaster, Duelist)
    # Keyvin:
    0x52: "Leather Neckband",
    0x53: "Beastman Collar",
    0x54: "Power Wrist",
    0x55: "Red Moon Horn",
    0x56: "Cardinal Eye",
    0x57: "Mad Beast's Fang",
    0x58: "Dragon's Bone",
    0x59: "Toshei Armband",
    0x5A: "Demon Neckband",
    # Hawk:
    0x5B: "Utsusemi Earring",
    0x5C: "Thief's Earring",
    0x5D: "Cobra Earrings",
    0x5E: "Ancient Talisman",
    0x5F: "Tree Spirit Ring",
    0x60: "Mistscreen Charm",
    0x61: "Wishbone",
    0x62: "Lucky Card",
    0x63: "Stealth Card",
    # Angela:
    0x64: "Jewel Ring",
    0x65: "Crystal Ring",
    0x66: "Mist Pendant",
    0x67: "Protect Ring",
    0x68: "Snow Crystal",
    0x69: "Fireball",
    0x6A: "Sage Stone",
    0x6B: "Blizzard Hairpin",
    0x6D: "Marble Ring",
    # Carlie:
    0x6E: "Bunny Egg",
    0x6F: "Moon Ring",
    0x70: "Protect Earrings",
    0x71: "Whitelight Ring",
    0x72: "Blackshade Ring",
    0x73: "Magatama",
    0x74: "Moon Flower",
    0x75: "Black Onyx",
    # Lise:
    0x76: "Vambrace",
    0x77: "Steel Bracelet",
    0x78: "Wind God Bracelet",
    0x79: "Earth Bracelet",
    0x7A: "Gyralhorne",
    0x7B: "Rune Earrings",
    0x7C: "Yadorigi Armlet",
    0x7D: "Draupnir",
    0x7E: "Giant's Ring"
}


def best_shield(level, class_type):
    if level == 0:
        return None
    if level == 1:
        if class_type == 1:
            return None
        return 5
    if class_type == 0:
        return 6
    if class_type == 1:
        return 7
    return None


SHIELDS = {
    # All Duran, obviously
    0xF7: "WaterDragon Shld",  # L
    0xF8: "Red-heat Shield",  # L
    0xF9: "Thunder God Shld",  # L
    0xFA: "Earth Shield",  # L
    0xFB: "Gold Shield",  # L
    0xFC: "Dragon Shield",  # L
    0xFD: "Sacred Shield",  # LL
    0xFE: "Oath Shield",  # LD -> Protects Status!
    0xFF: "Adamant Shield"  # LD
}

# Made up by three bytes:
# first byte: character index
# second byte: number of class changes
# third byte:
# if light / dark
CLASSES = (
    # 0-0-0      #0-1-0    #0-1-1       #0-2-0     #0-2-1  #0-2-2         #0-2-3
    ("Fighter", "Knight", "Gladiator", "Paladin", "Lord", "Swordmaster", "Duelist"),
    ("Grappler", "Monk", "Bashkar", "God Hand", "Warrior Monk", "Death Hand", "Dervish"),
    ("Thief", "Ranger", "Ninja", "Wanderer", "Rogue", "Ninja Master", "Nightblade"),
    ("Magician", "Sorceress", "Delvar", "Grand Divina", "Archmage", "Rune Master", "Magus"),
    ("Cleric", "Priestess", "Enchantress", "Bishop", "Sage", "Necromancer", "Evil Shaman"),
    ("Amazoness", "Valkyrie", "Rune Maiden", "Vanadis", "Star Lancer", "Dragon Master", "Fenrir Knight")
)


def get_class_id(class_name: str) -> Tuple[int, int, int]:
    name = class_name.lower()
    for archetype, class_names in enumerate(CLASSES):
        names = [c.lower() for c in class_names]
        for index, search in enumerate(names):
            if name == search:
                if 0 == index:
                    return archetype, 0, 0
                if index < 3:
                    return archetype, 1, index - 1
                return archetype, 2, index - 3
    raise ValueError("unknown class: " + class_name)


def get_class_name(character_index: int, class_changes: int, alignment: int) -> str:
    offset = 1 if class_changes > 1 else 0
    return CLASSES[character_index][class_changes + alignment + offset]


def revert_class_change(class_level: int, alignment: int) -> Tuple[int, int]:
    match class_level:
        case 0:
            return class_level, alignment
        case 1:
            return 0, 0
        case 2:
            return 1, 0 if alignment < 2 else 1
        case _:
            raise ValueError('unsupported class level', class_level)


BEST_RESOLVERS = {
    "helmets": (best_helmet, HELMETS, None),
    "armors": (best_armor, ARMORS, None),
    "gauntlets": (best_gauntlet, GAUNTLETS, 9),
    "weapons": (best_weapon, WEAPONS, 0x2a),
    "shields": (best_shield, SHIELDS, None)
}


def get_equip_type(item_name: str):
    for equip_type in BEST_RESOLVERS:
        collection = set(BEST_RESOLVERS[equip_type][1].values())
        if item_name in collection:
            return equip_type[:-1]
    return None


def get_best_item(item_type, archetype, class_level, class_type) -> Tuple[int, str]:
    if item_type == "shields" and (archetype != 0 or class_level == 0):
        return 0, 'Nothing'
    resolver, collection, length = BEST_RESOLVERS[item_type]
    if length is None:
        length = int(len(collection) / 6)
    type_offset = min(map(int, collection.keys()))
    item_offset = resolver(class_level, class_type)
    if item_offset is None:
        return 0, 'Nothing'
    class_offset = length * archetype
    _item_id = type_offset + class_offset + item_offset
    return _item_id, collection[_item_id]
