from snesedit.cli import action
from snesedit.fields import BinaryCodedDecimal, Byte, Int24
from snesedit.savestate import SaveState

STAT_NAMES = 'bite', 'strength', 'kick', 'strike', 'horn damage', 'horn durability'


class EvoState(SaveState):
    rom_name = 'E.V.O. Search for Eden'

    cur_hp: int = BinaryCodedDecimal(0xC5, size=2)
    max_hp: int = BinaryCodedDecimal(0xC7, size=2)
    evo_points: int = Int24(0xD2)

    bite: int = Byte(0xC9)
    strength: int = Byte(0xCA)
    kick: int = Byte(0xCB)
    strike: int = Byte(0xCC)
    horn_damage: int = Byte(0xCD)
    horn_durability: int = Byte(0xCE)

    argument_groups = {'actions': "modifiable game stats"}

    def display(self, verbose=False):
        print(f"E.V.O: {self.cur_hp}/{self.max_hp} HP, {self.evo_points} evo points")
        if verbose:
            for stat in STAT_NAMES:
                value = getattr(self, stat.replace(' ', '_'))
                print(f" {stat}: {value}")

    @action(group='actions')
    def heal(self):
        """restores HP"""
        self.cur_hp = self.max_hp

    @action(name='evo-points', group='actions')
    def add_evo_points(self, points: int):
        """adds POINTS evolution points. UI refreshes after evolution or eating."""
        self.evo_points += points


if __name__ == '__main__':
    EvoState.main()
