# PAR codes:
# https://gamefaqs.gamespot.com/snes/588784-terranigma/faqs/19381
from snesedit.cli import action
from snesedit.data import load_editor_data
from snesedit.fields import Short, BinaryCodedDecimal, Byte, PairwiseInventory
from snesedit.savestate import SaveState

DATA: dict[str, dict[int, str]] = load_editor_data('terranigma')
ALL_ITEMS = DATA['items'] | DATA['uniques']
UNIQUES = set(DATA['uniques'].values())


class Terranigma(SaveState):
    rom_name = 'Terranigma'

    cur_hp: int = Short(0x65d)
    max_hp: int = Short(0x657)

    strength_bonus: int = Short(0x659)
    defense_bonus: int = Short(0x65b)

    base_strength: int = Short(0x662)
    base_defense: int = Short(0x65f)
    luck: int = Byte(0x661)

    gems = BinaryCodedDecimal(0x694, 3)
    magic_rocks: int = BinaryCodedDecimal(0x7ed, 1)
    xp: int = BinaryCodedDecimal(0x690, 3)
    level: int = Byte(0x656)

    items = PairwiseInventory(offset=0x18000, elements=ALL_ITEMS, capacity=27)
    rings = PairwiseInventory(offset=0x18080, elements=DATA['rings'], capacity=15)

    def display(self, verbose=False):
        print(f"Terranigma: Level {self.level}, {self.cur_hp}/{self.max_hp}, {self.xp} XP")

    @action
    def heal(self):
        """restores HP"""
        self.cur_hp = self.max_hp

    @action
    def experience(self, xp: int):
        """adds XP experience points"""
        self.xp += xp


if __name__ == '__main__':
    Terranigma.main()
