from typing import Set, Mapping, Sequence, Tuple, List, Dict
from snesedit.cli import GroupedParser, action
from snesedit.fields import Short, ArrayElement, Byte, Int24, PositionalInventory, BitSet, ShortArray, ByteArray, String
from snesedit.savestate import SaveState

import logging

log = logging.getLogger(__name__)

DEFAULT_ING_CAP = 50
WEAPONS = [
    None, "Bone Crusher", "Gladiator Sword", "Crusader Sword", "Neutron Blade", "Spider's Claw",
    "Bronze Axe", "Knight Basher", "Atom Smasher", "Horn Spear", "Bronze Spear", "Lance",
    "Laser Lance ", "Bazooka", "*Aura", "*Regenerate"
]

# bazooka cannot be leveled
LEVELED_WEAPONS = WEAPONS[1:-3]

INGREDIENTS = [
    "Wax", "Water", "Vinegar", "Root", "Oil", "Mushroom", "Mud Pepper", "Meteorite",
    "Limestone", "Iron", "Gunpowder", "Grease", "Feather", "Ethanol", "Dry Ice",
    "Crystal", "Clay", "Brimstone", "Bone", "Atlas Amulet", "Ash", "Acorn"
]

# these will not be automatically refilled
RARE = ("Mud Pepper", "Meteorite", "Atlas Amulet")

ALCHEMY = [
    "Acid Rain", "Atlas", "Barrier", "Call Up", "Corrosion", "Crush",
    "Cure", "Defend", "Double Drain", "Drain", "Energize", "Escape",
    "Explosion", "Fireball", "Fire Power", "Flash", "Force Field",
    "Hardball", "Heal", "Lance", "Laser", "Levitate", "Lightning Storm",
    "Miracle Cure", "Nitro", "One Up", "Reflect", "Regrowth", "Revealer",
    "Revive", "Slow Burn", "Speed", "Sting", "Stop", "Super Heal"
]

CURRENCIES = ["talons", "jewels", "coins", "kredits"]

EQUIPMENT = [
    "Grass Vest", "Shell Plate", "Dino Skin", "Bronze Armor",
    "Stone Vest", "Centurion Cape", "Silver Mail", "Gold-Plated Vest",
    "Shining Armor", "Magna Mail", "Titanium Vest", "Virtual Vest",
    "Grass Hat", "Shell Hat", "Dino Helm", "Bronze Helmet",
    "Obsidian Helm", "Centurion Helm", "Titan's Crown", "Dragon Helm",
    "Knight's Helm", "Lightning Helm", "Old Reliable", "Brainstorm",
    "Vine Bracelet", "Mammoth Guard", "Claw Guard", "Serpent Bracer",
    "Bronze Gauntlet", "Gloves of Ra", "Iron Bracer", "Magician's Ring",
    "Dragon's Claw", "Cyberglove", "Protector Ring", "Virtual Glove",
    "Leather Collar", "Spiky Collar", "Defender Collar", "Spot's Collar",
    "Thunder Ball", "Particle Bomb", "Cryo-Blast"
]

TRADING_GOODS = [
    'Annihilation Amulet', 'Bead', 'Ceramic Pot', 'Chicken', 'Golden Jackal', 'Jeweled Scarab',
    'Limestone Tablet', 'Perfume', 'Rice', 'Spice', 'Spoon', 'Tapestry', 'Ticket for Exhibition'
]

ITEMS = ['Petal', 'Nectar', 'Honey', 'Dog Biscuit', 'Wings', 'Essence', 'Pixie Dust', 'Call Bead']


class SoeCharacter(ArrayElement):
    NAMES = ["Boy", "Dog"]

    #  most stats are in the same struct with the same offset between boy and dog, but cur_hp is way off
    CUR_HP_OFFSETS = [0x4eb3, 0x4f61]
    #  names are also elsewhere
    NAME_OFFSETS = [0x2210, 0x2234]

    max_hp: int = Short(0xa35)
    cur_hp: int = Short(0xdead)  # see __seek__

    attack: int = Short(0x0a3f)
    defense: int = Short(0x0a41)
    magic_def: int = Short(0x0a43)
    evade: int = Short(0x0a45)
    hit: int = Short(0x0a47)
    name: str = String(0xdead, size=0x20)

    level = Byte(0x0a50)
    experience: int = Int24(0xa49)

    @property
    def base_name(self) -> str:
        return self.NAMES[self.index]

    def __seek__(self, offset: int, attribute_name: str = None) -> int:
        if attribute_name == 'cur_hp':
            return self.CUR_HP_OFFSETS[self.index]
        if attribute_name == 'name':
            return self.NAME_OFFSETS[self.index]
        return super().__seek__(offset, attribute_name)

    def __str__(self):
        return self.name

    def __repr__(self):
        stats = f"{self.attack}A {self.defense}D {self.magic_def}MD {self.evade}%E {self.hit}H%"
        return f"{self.name} L{self.level} {self.cur_hp}/{self.max_hp} HP {self.experience} EXP [{stats}]"

    def display(self, verbose=False):
        print(f"{self.name}: Level {self.level} {self.cur_hp}/{self.max_hp} HP {self.experience} EXP", )
        if verbose:
            print(f"{self.attack} Atk {self.defense}Def {self.magic_def}MDef {self.evade}%Evd {self.hit}%Hit")


class SoEState(SaveState):
    rom_name = 'Secret of Evermore'

    talons: int = Int24(0x0ac6)
    jewels: int = Int24(0x0ac9)
    coins: int = Int24(0x0acc)
    kredits: int = Int24(0x0acf)  # sic!

    characters: Sequence[SoeCharacter] = SoeCharacter.array(length=2, step=0x4a)

    ingredients: Dict[str, int] = PositionalInventory(0x22ff, INGREDIENTS)
    equipment: Dict[str, int] = PositionalInventory(0x231d, EQUIPMENT)
    items: Dict[str, int] = PositionalInventory(0x2315, ITEMS)
    trade_goods: Dict[str, int] = PositionalInventory(0x2517, TRADING_GOODS, bytes_per_slot=2)
    weapons: Set[str] = BitSet(0x22da, WEAPONS)
    formulas: Set[str] = BitSet(0x2258, ALCHEMY)

    _alchemy_progress: List[int] = ShortArray(0x2f52, length=len(ALCHEMY))
    _alchemy_levels: List[int] = ShortArray(0x2f98, length=len(ALCHEMY))
    _weapon_levels: List[int] = ByteArray(0x0adf, length=len(LEVELED_WEAPONS) * 2)
    _dog_attack: List[int] = ByteArray(0x0b07, length=2)
    _equipped_weapon: int = Byte(0xaba)

    argument_groups = {
        'characters': 'manage character state and progress',
        'alchemy': 'manage formulas and ingredients',
        'inventory': 'manage equipment and items',
        'money': None
    }

    list_options = {
        ("ingredients", "alchemy"): INGREDIENTS,
        ("weapons", "inventory"): WEAPONS,
        ("equipment", "inventory"): EQUIPMENT,
        ("trading-goods", "inventory"): TRADING_GOODS
    }

    @property
    def weapon_levels(self) -> Mapping[str, Tuple[int, int]]:
        def weapon_level(i):
            progress = int(self._weapon_levels[2 * i] / 256.0 * 100)
            level = self._weapon_levels[2 * i + 1]
            return level, progress

        return {weapon: weapon_level(i) for i, weapon in enumerate(LEVELED_WEAPONS) if weapon in self.weapons}

    @property
    def dog_attack(self) -> Tuple[int, int]:
        progress = int(self._dog_attack[0] / 256.0 * 100)
        level = self._dog_attack[1]
        return level, progress

    @property
    def alchemy(self) -> Mapping[str, Tuple[int, int]]:
        return {formula: (self._alchemy_levels[i], self._alchemy_progress[i])
                for i, formula in enumerate(ALCHEMY) if formula in self.formulas}

    @property
    def equipped_weapon(self) -> str:
        return WEAPONS[int(self._equipped_weapon / 2)]

    def display(self, verbose=False):
        def summarize(inventory: Mapping[str, int]):
            return ", ".join(f"{item} x {count}" for item, count in inventory.items())

        print('Money: ', ", ".join(('%d %s' % (getattr(self, c), c) for c in CURRENCIES)))
        for c in self.characters:
            c.display(verbose)
        if verbose:
            print("Equipment:", summarize(self.equipment))
            print("Alchemy progress:", ", ".join(f"{f}: {l[0]}:{l[1]}" for f, l in self.alchemy.items()))
            print("Ingredients:", summarize(self.ingredients))
            print("Weapon Levels:", ", ".join(f"{wpn}: {l[0]}:{l[1]:02}" for wpn, l in self.weapon_levels.items()))
            print("Trade Goods:", summarize(self.trade_goods))
            print("Equipped:", self.equipped_weapon)

    @classmethod
    def configure_parser(cls, parser: GroupedParser):
        parser.add_argument("--ingredient-cap", type=int, default=DEFAULT_ING_CAP, metavar='CAP',
                            group='alchemy', help='when using --refill, top up each ingredient to a maximum '
                                                  'of CAP [default:%d]' % DEFAULT_ING_CAP)
        parser.add_argument("--currency", type=str, metavar="TYPE", group='money',
                            help="when adding --money, add to account TYPE instead of the default currency")
        parser.add_argument('--max-level', group='alchemy', type=int, default=9,
                            help='do not level alchemy formulas beyond this level [default: 9]')
        parser.add_argument('--formula', type=str, group='alchemy',
                            help='when using --alchemy-level-up, only increase FORMULA')
        parser.add_argument('-c', '--count', group='alchemy', type=int, default=10,
                            help='when adding ingredients, add this amount [default: 10]')

    @action(group='characters')
    def heal(self, *, verbose: bool = False):
        """Heal both characters"""
        for c in self.characters:
            c.cur_hp = c.max_hp
        if verbose:
            print("healed both characters")

    @action(group='characters')
    def experience(self, xp: int, *, verbose: bool = False):
        """add XP experience to both characters"""
        for c in self.characters:
            c.experience += xp
        if verbose:
            print(f"added {xp} experience")

    @action(group='inventory', short_name=None)
    def add_equipment(self, item: str, *, verbose: bool = False):
        """adds one ITEM to equipment"""
        if item not in EQUIPMENT:
            raise ValueError(f"unknown item {item}")
        if verbose:
            print("adding 1 x", item)
        self.equipment[item] = 1 + self.equipment.get(item, 0)

    @action(group='inventory', short_name=None)
    def add_weapon(self, weapon: str):
        """adds WEAPON to the boys selection"""
        if weapon not in WEAPONS:
            raise ValueError(f"unknown weapon: {weapon}")
        self.weapons.add(weapon)

    @action(group='inventory', short_name=None)
    def remove_weapon(self, weapon: str):
        self.weapons.remove(weapon)

    @action(group='characters', short_name=None)
    def weapon_level_up(self, *, verbose: bool = False, max_level: int = 5):
        """Prepare level up for currently equipped weapon"""
        w = self.equipped_weapon
        level, progress = self.weapon_levels.get(w)
        if level < max_level:
            index = LEVELED_WEAPONS.index(w) * 2
            self._weapon_levels[index] = 0xFF
            if verbose:
                print(f"{w} ready to level up ({level}:99)")

    @action(group='characters', short_name=None)
    def dog_attack_level_up(self, *, verbose: bool = False, max_level: int = 9):
        """Prepare level up for the dogs attack skill"""
        level, progress = self._dog_attack
        if level < max_level:
            self._dog_attack[0] = 0xFF
            if verbose:
                print(f"dog attack ready to level up ({level}:99)")

    @action(group='alchemy', short_name=None)
    def add_ingredient(self, ingredient: str, *, count: int = 10):
        """Adds 10 (or value of --count) of INGREDIENT for alchemy"""
        if ingredient not in INGREDIENTS:
            raise ValueError('unknown ingredient: ' + ingredient)
        self.ingredients[ingredient] = self.ingredients.get(ingredient, 0) + count

    @action(group='inventory', short_name=None)
    def give_trading_good(self, name: str, *, verbose=False):
        """Gives 99 of the trading good NAME"""
        if name not in TRADING_GOODS:
            raise ValueError(f"unknown trading good: {name}")
        current = self.trade_goods.get(name, 0)
        if current < 99:
            self.trade_goods[name] = 99
            if verbose:
                print(f"added {99 - current} x {name}")

    @action(group='alchemy')
    def refill(self, *, ingredient_cap: int = DEFAULT_ING_CAP):
        """
        refills all alchemy ingredients of which at least one is in the inventory,
        capped at the value of --ingredient-cap
        """
        for item, count in self.ingredients.items():
            if count < ingredient_cap and item not in RARE:
                self.ingredients[item] = ingredient_cap

    @action(group='alchemy', shortName=None)
    def alchemy_level_up(self, *, max_level: int = 9, formula: str = None, verbose: bool = False):
        """level up any learned formula by setting level progress to X:99 unless level X is already at --max-level"""
        formulas = self.formulas
        if formula is not None:
            if formula not in ALCHEMY:
                raise ValueError(f"unknown formula: {formula}")
            if formula not in self.formulas:
                raise ValueError(f"formula {formula} has not yet been learned, add using --add-formula")
            formulas = [formula]
        for spell in formulas:
            index = ALCHEMY.index(spell)
            level = self._alchemy_levels[index]
            if level < max_level:
                self._alchemy_progress[index] = 99
                if verbose:
                    print(f"{spell} ready to level up ({level}:99)")

    @action(group='money')
    def money(self, amount: int, *, currency: str = None):
        """
        adds AMOUNT of money of the most advanced currency that you have at least one of.
        """

        def _get_currency():
            if currency is not None:
                if currency.lower() not in CURRENCIES:
                    raise ValueError(f'unknown currency: {currency}')
                return currency
            for c in reversed(CURRENCIES):
                if getattr(self, c):
                    return c

        currency_type = _get_currency()
        current = getattr(self, currency_type)
        setattr(self, currency_type, current + amount)


if __name__ == '__main__':
    state = SoEState.main()
