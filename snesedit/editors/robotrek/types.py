from decimal import Decimal
from typing import List

from snesedit.codecs import pad, chunk_bytes, SBCDCodec
from snesedit.editors.robotrek.data import Item
from snesedit.fields import SimpleField, T


class RobotItems(SimpleField[List[Item]]):

    def __init__(self):
        super().__init__(offset=0x4192, size=142, codec=None)

    def encode_to_bytes(self, value: List[Item]) -> bytes:
        def _iter():
            for robot in value:
                yield robot.item_id
                yield robot.level

        return pad(list(_iter()), size=self.size, padding_value=0)

    def decode_from_bytes(self, source: bytes) -> List[Item]:
        return [Item(id_, level) for id_, level in chunk_bytes(source, 2) if id_]


class RoboString(SimpleField[str]):

    def __init__(self, offset: int):
        super().__init__(offset=offset, size=7, codec=None)

    def decode_from_bytes(self, source: bytes) -> T:
        return source.decode(encoding='ascii').strip()

    def encode_to_bytes(self, value: T) -> bytes:
        raise NotImplementedError()


class RoboInt(SimpleField[int]):

    def __init__(self, offset: int, size: int = 3):
        super().__init__(offset, size, codec=SBCDCodec(length=size))

    def encode_to_bytes(self, value: T) -> bytes:
        result = super().encode_to_bytes(value)
        return result


class RoboDecimal(RoboInt):

    def encode_to_bytes(self, value: Decimal) -> bytes:
        result = super().encode_to_bytes(int(value * 10))
        return result

    def decode_from_bytes(self, source: bytes) -> Decimal:
        return Decimal(super().decode_from_bytes(source)) / 10