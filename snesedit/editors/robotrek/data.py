import re
from dataclasses import dataclass

from snesedit.util import invert

ROBOT_ITEM_NAMES = [
    None, 'Sword 1', 'Sword 2', 'Sword 3', 'Sword 4', 'Axe 1', 'Axe 2', 'Axe 3',
    'Blade 1', 'Blade 2', 'Blade 3', 'Blade 4', 'Hammer 1', 'Hammer 2', 'Hammer 3', 'Celtis 1',
    'Celtis 2', 'Celtis 3', 'Punch 1', 'Punch 2', 'Punch 3', 'Blow 1', 'Blow 2', 'Blow 3',
    'Shot 1', 'Shot 2', 'Shot 3', 'Laser 1', 'Laser 2', 'Laser 3', 'Bomb 1', 'Bomb 2',
    'Bomb 3', 'Bomb 4', 'Shield 1', 'Shield 2', 'Shield 3', 'Shield 4', 'Shield 5', 'Empty Pack',
    'Power Pack', 'Shield Pack', 'Turbo Pack', 'Solar Pack', 'Quick Pack', 'Boots 1', 'Boots 2', 'Boots 3',
    'Boots 4', 'Boots 5', 'Boots 6'
]

# "Transceiver": 0x49
HUMAN_ITEM_NAMES = [
    None, 'Transceiver', 'Horn', 'Drill', 'Vanish', 'Change', 'Glasses', 'Key',
    'Little Robot', 'Light', 'Seed', 'Blimp', 'S/Ship', 'Warp System', 'Relay', 'Cyber Jack',
    'Red Jar', 'Blue Jar', 'Yel Jar', 'Jewel', 'Letter', 'Litho', "Leader's Badge", 'Badge 2',
    'Stone 1', 'Stone 2', 'Stone 3', 'Red Flower', 'Sphere', 'Rusty Drill', 'Scrap A', 'Scrap B',
    'Scrap 1', 'Scrap 2', 'Scrap 3', 'Scrap 4', 'Scrap 5', 'Scrap 6', 'Scrap 7', 'Scrap 8',
    'Scrap 9', 'Scrap 10', '_dummy 1', '_dummy 2', 'Smoke', 'Cure', 'Clean', 'Repair', 'Big Bomb',
    'Weather', 'Trash Can'
]

LEVELED_ITEMS = {"Sword", "Axe", "Blade", "Hammer", "Punch", "Celtis", "Shot", "Laser", "Blow", "Bomb"}
RANKED_ITEMS = LEVELED_ITEMS | {"Boots", "Shield"}

ROBOT_IDS = {index: name for index, name, in enumerate(ROBOT_ITEM_NAMES)}
HUMAN_IDS = {index + 0x48: name for index, name, in enumerate(HUMAN_ITEM_NAMES)}
ROBOT_ITEMS = invert(ROBOT_IDS)
HUMAN_ITEMS = invert(HUMAN_IDS)
ITEM_IDS = ROBOT_IDS | HUMAN_IDS
ALL_ITEMS = {name: id_ for id_, name in ITEM_IDS.items()}

ITEM_PATTERN = re.compile(r'(.+?)( L(\d))?$')


@dataclass(frozen=True)
class Item:
    item_id: int
    level: int

    def __post_init__(self):
        name = ITEM_IDS.get(self.item_id, None)
        if not name:
            raise ValueError("unknown item id: %x" % self.item_id)
        if self.level < 0:
            raise ValueError("level must be a non-negative integer")
        if name in HUMAN_ITEM_NAMES and self.level > 0:
            raise ValueError("human items cannot have a level")
        if name in ROBOT_ITEM_NAMES and self.level > 9:
            raise ValueError("max item level: 9")

    @classmethod
    def value_of(cls, name: str) -> 'Item':
        name, _, lvl = ITEM_PATTERN.match(name).groups()
        level = int(lvl) if lvl is not None else 0
        id_ = ALL_ITEMS.get(name)
        if not id_:
            raise ValueError("unknown item: %s" % name)
        return cls(item_id=id_, level=level)

    @property
    def name(self) -> str:
        base_name = ITEM_IDS[self.item_id]
        return self.level and "%s L%d" % (base_name, self.level) or base_name

    @property
    def kind(self) -> str:
        return "robot" if self.item_id in ROBOT_IDS else "human"

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"Item({self.item_id}, {self.level})"
