from collections import defaultdict
from decimal import Decimal
from typing import List, Sequence, Mapping, Set, Any

from snesedit.cli import action, CommandLineError
from snesedit.editors.robotrek.data import Item, HUMAN_ITEMS, ROBOT_ITEMS, ROBOT_ITEM_NAMES, HUMAN_ITEM_NAMES
from snesedit.editors.robotrek.types import RobotItems, RoboString, RoboInt, RoboDecimal
from snesedit.fields import ArrayElement, Short, SimpleTable, Byte, BitSet
from snesedit.savestate import SaveState


def count(things: Sequence[Any]) -> Mapping[Any, int]:
    d = defaultdict(int)
    for thing in things:
        d[thing] += 1
    return d


class Robot(ArrayElement):
    energy: int = Short(offset=0x68a)
    max_energy: int = Short(offset=0x690)
    power: int = Short(offset=0x696)
    guard: int = Short(offset=0x69c)
    speed: int = Short(offset=0x6a2)
    charge: int = Short(offset=0x6a8)
    _name: str = RoboString(offset=0x64e)  # seek: 12 bytes apart

    @property
    def name(self) -> str:
        return self._name

    def display(self):
        print(f"{self.name}: {self.energy}/{self.max_energy} HP")

    def __seek__(self, offset: int, attribute_name: str = None) -> int:
        if attribute_name == "_name":
            return offset + self.index * 12
        return super().__seek__(offset, attribute_name=attribute_name)

    def heal(self):
        self.energy = self.max_energy


class RoboState(SaveState):
    rom_name = "Robotrek"

    gold: int = RoboInt(offset=0x6e6)
    level: int = Byte(offset=0x686)
    program_points: int = Short(offset=0x688)
    _name: str = RoboString(offset=0x642)
    xp: Decimal = RoboDecimal(offset=0x6de, size=3)
    _to_next_level: Decimal = RoboDecimal(offset=0x6e2, size=3)
    _robots: List[Robot] = Robot.array(step=2, length=3)

    human_recipes: Set[str] = BitSet(offset=0x7b9, elements=HUMAN_ITEM_NAMES)
    robot_recipes: Set[str] = BitSet(offset=0x7b0, elements=ROBOT_ITEM_NAMES)

    human_items: List[str] = SimpleTable(offset=0x4102, length=71, elements=HUMAN_ITEMS, empty_slot=0)
    robot_items: List[Item] = RobotItems()

    _level_up: bool = False

    @property
    def name(self) -> str:
        return self._name

    @property
    def robots(self) -> list[Robot]:
        return [r for r in self._robots if r.max_energy > 0]

    @property
    def to_next_level(self) -> Decimal:
        if self._level_up:
            return Decimal('0.1')
        return self._to_next_level

    def display(self, *, verbose: bool = False):
        print(f"{self.name} Level {self.level}, {self.xp} megs XP, "
              f"{self.gold} gold, {self.program_points} PP")
        if verbose:
            print("Next Level at", self.to_next_level, "megs")
            for r in self.robots:
                r.display()
            print("human recipes:", ", ".join(self.human_recipes))
            print("robot recipes:", ", ".join(self.robot_recipes))

    @action(modifies=False)
    def inventory(self):
        """show human and robot items"""
        hi = count(self.human_items)
        ri = count(self.robot_items)
        print(f"Human Items [{len(hi)}/71]:")
        for item, num in hi.items():
            print(f" {num:02} x {item}")
        print(f"Robot Items [{len(ri)}/71]:")
        for item, num in ri.items():
            print(f" {num:02} x {item}")

    @action
    def heal(self):
        """restore all robots to full energy"""
        for r in self.robots:
            r.heal()

    @action(name='gold')
    def add_gold(self, gold: int):
        """adds GOLD to money"""
        self.gold += gold

    @action(name='points')
    def add_pp(self, pp: int):
        """adds PP program points to spend on robots"""
        self.program_points += pp

    @action(name='add')
    def add_item(self, item: str):
        """adds ITEM to appropriate inventory"""
        try:
            item = Item.value_of(item)
        except ValueError as ve:
            raise CommandLineError(ve.args[0])
        getattr(self, f"{item.kind}_items").append(item)

    @action
    def level_up(self, *, verbose: bool = False):
        """Adds experience so next battle will lead to a level up"""
        to_add = self._to_next_level - Decimal('0.1')
        if verbose:
            print("adding", to_add, "megs XP to reach level", self.level + 1)
        self.xp = self.xp + to_add
        self._level_up = True
        # writing this value fucks up the game
        # self._to_next_level = Decimal('0.1')

    @action(short_name=None, modifies=True)
    def learn(self, item: str, *, verbose: bool = False):
        """adds recipe to craft ITEM on workbench"""
        if item in HUMAN_ITEMS:
            self.human_recipes.add(item)
        elif item in ROBOT_ITEMS:
            self.robot_recipes.add(item)
        else:
            raise CommandLineError('unknown item: ' + item)
        if verbose:
            print("learned to create", item)


if __name__ == "__main__":
    RoboState.main()
