from snesedit.fields import SimpleField, Mapped
from .data import ITEMS


class StarString(SimpleField[str]):

    def __init__(self, offset: int, length=8):
        super().__init__(offset, size=length, codec=None)

    def encode_to_bytes(self, value: str) -> bytes:
        if len(value) > self.size - 1:
            raise ValueError(f'max string length : {self.size - 1}')
        encoded = [translate_char(c) for c in value]
        while len(encoded) < self.size:
            encoded.append(0xff)
        return bytes(encoded)

    def decode_from_bytes(self, source: bytes) -> str:
        return "".join([translate_byte(b) for b in source if b not in (0x00, 0xff)])


def translate_byte(b: int) -> str:
    # '/'
    if b == 65:
        return '/'
    # '♪'
    if b == 3:
        return '♪'
    # 'A- Z'
    if b < 66:
        return chr(b + 0x20)
    # 'a-z1-9' + some specials
    return chr(b + 0x1f)


def translate_char(c: str) -> int:
    if c == '♪':
        return 3
    if c == '/':
        return 65
    if 'A' <= c <= 'Z':
        return ord(c) - 0x20
    return ord(c) - 0x1f


class StarItem(Mapped[str]):

    def __init__(self, offset):
        super(StarItem, self).__init__(offset=offset, elements=ITEMS, bytes_per_slot=2)
