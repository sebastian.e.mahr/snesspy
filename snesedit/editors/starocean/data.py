from snesedit.data import load_editor_data

_DATA = load_editor_data('starocean')

TALENTS = _DATA['talents']
# Map of skill name -> SP required per level
_SKILLS = _DATA['skills']
SKILLS = {skill: list(map(int, scale.strip().split('/'))) for skill, scale in _SKILLS.items()}
SKILL_NAMES = list(_SKILLS.keys())
SECRET_SKILLS = {skill for skill, scale in _SKILLS.items() if scale == '-1'}
SCHOOL_SKILLS = _DATA['school-skills']
BASE_NAMES = _DATA['base-names']
ITEMS = _DATA['items']

JUNK_ITEMS = _DATA['junk-items']
JUNK_IDS = [ITEMS.index(item) for item in JUNK_ITEMS]

TECHS = _DATA['techs']