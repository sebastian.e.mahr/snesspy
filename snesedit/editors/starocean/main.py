from typing import Dict, List, Set

from snesedit.cli import action, subcommand, GroupedParser, CommandLineError
from snesedit.fields import Int24, Short, Integer, Byte, SquareEnixInventory, PositionalInventory, BitSet
from snesedit.savestate import SaveState, Character, format_equipment
from .data import BASE_NAMES, ITEMS, SKILLS, TALENTS, SKILL_NAMES, SECRET_SKILLS, SCHOOL_SKILLS, JUNK_ITEMS
from .types import StarString, StarItem

STATS = "strength", "intelligence", "agility", "constitution", "guts", "evade",
STATUS = ["Dead", "Paralyzed", "Stoned", "Poisoned"]
EQUIP_SLOTS = 'weapon', 'armor', 'shield', 'helmet', 'pants', 'jewelry1', 'jewelry2'

NON_RESETTABLE = SCHOOL_SKILLS | SECRET_SKILLS
SKILLS_OFFSET = 0x84e1


def status_str(status_flags: int) -> str:
    if not status_flags:
        return "Normal"
    ailments = [STATUS[i] for i in range(len(STATUS)) if status_flags & (1 << i)]
    return ", ".join(ailments)


class StarCharacter(Character):
    base_hp = Short(0x702f)
    max_hp = Short(0x7031)
    cur_hp = Short(0x7033)
    base_mp = Short(0x7035)
    max_mp = Short(0x7037)
    cur_mp = Short(0x7039)
    stamina = Short(0x70bb)

    experience = Integer(0x708b)
    skill_points = Short(0x70b9)
    level = Byte(0x703b)
    _status = Byte(0x7003)

    _archetype = Byte(0x6fe2)

    # Attributes:
    strength = Short(0x703c)
    constitution = Short(0x7042)
    evade = Short(0x7048)
    agility = Short(0x704e)
    guts = Byte(0x7054)
    intelligence = Short(0x705a)

    name: str = StarString(0x708f)

    # Equipment:
    weapon: str = StarItem(0x707d)
    armor: str = StarItem(0x707f)
    shield: str = StarItem(0x7081)
    helmet: str = StarItem(0x7083)
    pants: str = StarItem(0x7085)
    jewelry1: str = StarItem(0x7087)
    jewelry2: str = StarItem(0x7089)

    talents: Set[str] = BitSet(0x70b7, elements=TALENTS)

    # offset managed by __seek__
    skills: Dict[str, int] = PositionalInventory(offset=0, keys=SKILL_NAMES)

    def __seek__(self, offset: int, attribute_name: str = None) -> int:
        if attribute_name == 'skills':
            return SKILLS_OFFSET + self.index * 0x40
        return super().__seek__(offset, attribute_name)

    def __str__(self):
        return f"StarCharacter({self.index})"

    @property
    def display_name(self) -> str:
        if self.name == self.base_name:
            return self.name
        return f"{self.name} ({self.base_name})"

    @property
    def base_name(self) -> str:
        return BASE_NAMES[self.id]

    @property
    def id(self) -> int:
        return self._archetype & 0x0f

    @property
    def active(self):
        return self.id and not self._archetype & 0xf0

    @property
    def status(self) -> str:
        return status_str(self._status)

    @property
    def equipment(self) -> dict[str, str]:
        return {slot.capitalize(): getattr(self, slot) for slot in EQUIP_SLOTS}

    @action
    def heal(self):
        """heal selected character"""
        super().heal()
        self._status = 0

    @action(name='skill-points', short_name=None)
    def add_skill_points(self, n: int):
        """Add N skill points to selected character"""
        self.skill_points += n

    @action(short_name='T')
    def add_talent(self, talent: str):
        """add TALENT to character."""
        if talent not in TALENTS:
            raise CommandLineError('unknown talent: ' + talent)
        self.talents.add(talent)

    @action(short_name=None)
    def remove_talent(self, talent: str):
        """remove TALENT from character"""
        self.talents.remove(talent)

    @action(name='experience')
    def add_experience(self, xp: int):
        """add XP experience points"""
        self.experience += xp

    @action(short_name=None)
    def reset_skills(self, *, verbose=True):
        """
        Resets all skills except for battle school and secret skills, then restores the skill points invested.
        Does not account for Effort or skill books, so probably awards a bit too much.
        Then again, this IS a cheating tool after all
        """
        invested = skills = 0
        resettable = [skill for skill, level in self.skills.items() if level and skill not in NON_RESETTABLE]
        for skill in resettable:
            level = self.skills[skill]
            scale = SKILLS[skill]
            invested += sum(scale[:level])
            if verbose:
                print(f"resetting skill {skill} (Level {level}): {invested} SP")
            del self.skills[skill]
        self.skill_points += invested
        if verbose:
            print(f"restored {invested} SP from {skills} skills")

    @action(name='dump', modifies=False, default=True)
    def display(self, *, verbose=False):
        """show character status"""
        print(
            f"{self.display_name}: L{self.level}, {self.cur_hp}/{self.max_hp} HP, "
            f"{self.cur_mp}/{self.max_mp} MP, {self.stamina} Stamina, {self.experience} XP, "
            f"{self.skill_points} SP, Status: {self.status}"
        )
        if verbose:
            #  print("Name:", self._name.hex(sep=' ', bytes_per_sep=1), str(self._name))
            print("-", ", ".join((f"{stat.capitalize()}: {getattr(self, stat)}" for stat in STATS)))
            print("- Skills:", ", ".join((f"{skill} {level}" for skill, level in self.skills.items())))
            print("- Talents:", ", ".join(self.talents))
            print("-", format_equipment(self.equipment))
        print()


class StarState(SaveState):
    rom_name = 'Star Ocean'
    init_options = ['all_characters']

    # this is the game's currency
    fol: int = Int24(0x273)
    battles: int = Short(0x2db)
    _characters: List[StarCharacter] = StarCharacter.array(length=8, step=0x100)
    inventory: Dict[str, int] = SquareEnixInventory(keys_offset=0x7fe1, values_offset=0x81e1, elements=ITEMS,
                                                    capacity=511)
    all_characters: bool

    list_options = {
        "items": ITEMS,
        "talents": TALENTS,
        "base names": BASE_NAMES
    }

    @classmethod
    def configure_parser(cls, parser: GroupedParser):
        parser.add_argument('-a', '--all-characters', action='store_true', dest='all_characters',
                            help='display and modify inactive characters as well')

    @property
    def characters(self):
        chars = self._characters[4:]
        if self.all_characters:
            chars += self._characters[:3]
        return [c for c in chars if c.active]

    @subcommand(actions=True, help={'name': 'the name of the character to edit'})
    def character(self, name: str) -> StarCharacter:
        """Allows editing stats and details of a single character"""
        for c in self._characters:
            if c.name == name or c.base_name == name:
                return c
        raise CommandLineError(f"unknown character: '{name}'")

    @subcommand(name='add', modifies=True, help={
        'item': 'item name to add',
        'count': 'add count items instead of just one'
    })
    def add_item(self, item: str, count: int = 1):
        """add count x ITEM to inventory"""
        if item not in ITEMS:
            raise CommandLineError(f"unknown item: {item}")
        self.inventory[item] = min(self.inventory.get(item, 0) + count, 20)

    @action(short_name='F', name='fol')
    def add_fol(self, amount: int):
        """add AMOUNT fol"""
        self.fol += amount

    @action
    def heal(self):
        """heal active characters"""
        for c in self.characters:
            c.heal()

    @action(name='experience')
    def add_experience(self, xp: int, *, verbose=False):
        """add XP experience points to active characters"""
        for c in self.characters:
            c.experience += xp
            if verbose:
                print(f"Added {xp} XP to {c.name}")

    def display(self, verbose=False):
        print(f"Star Ocean state {self._current_slot}: {self.fol} Fol, {self.battles} Battles")
        print("Characters:")
        for c in self.characters:
            c.display(verbose=verbose)

    @action(name='inventory', modifies=False)
    def display_inventory(self):
        """display the party's inventory"""
        for item, amount in self.inventory.items():
            print(f"{amount:-2} x {item} ")

    @action(short_name=None)
    def remove_junk(self, *, verbose=False):
        """remove all junk items from inventory"""
        for item in filter(lambda i: i in self.inventory, JUNK_ITEMS):
            if verbose:
                count = self.inventory.get(item)
                print(f"removing {count:-2} x {item}")
            del self.inventory[item]

    @action(short_name=None)
    def encounters(self, fights: int, *, verbose=False):
        """Set the number of encounters for the party to FIGHTS"""
        if fights < 1:
            raise CommandLineError('FIGHTS: must be a positive integer')
        if fights == self.battles:
            return
        previous = self.battles
        self.battles = fights
        if verbose:
            sign = "inc" if fights > self.previous else "dec"
            print(f"{sign}reased encounters from {previous} to {fights}")


if __name__ == '__main__':
    StarState.main()
