from typing import List, Mapping

from snesedit.cli import CommandLineError, action, GroupedParser
from snesedit.fields import Short, ParentField, Byte, ByteArray, SimpleTable, Mapped
from snesedit.savestate import SaveState, ArrayElement
from .items import ITEMS, EQUIPMENT, count

MAX_ITEMS = 29
MAX_NAME_LENGTH = 8


# sources:
# https://datacrystal.romhacking.net/wiki/Super_Mario_RPG:_Legend_of_the_Seven_Stars:RAM_map
# https://gamefaqs.gamespot.com/snes/588739-super-mario-rpg-legend-of-the-seven-stars/faqs/6234
# https://gamefaqs.gamespot.com/snes/588739-super-mario-rpg-legend-of-the-seven-stars/faqs/26315

# essentially a copy of a character, always modify both
class ActivePartyMember(ArrayElement):
    alive: int = Byte(0xFA80)
    character_id: int = Byte(0xFA81)
    level: int = Byte(0xFA90)
    cur_hp: int = Short(0xFA91)
    max_hp: int = Short(0xFA93)
    _status: int = Byte(0xFAC0)

    def __repr__(self):
        return f"ActivePartyMember(index={self.index}, id={self.character_id}, {self.cur_hp}/{self.max_hp} HP"


class MarioCharacter(ArrayElement):
    default_name: str = ParentField()
    parent: "MarioRPGState"

    level: int = Byte(0x1f800)
    cur_hp: int = Short(0x1f801)
    max_hp: int = Short(0x1f803)

    speed: int = Byte(0x1f805)
    attack: int = Byte(0x1f806)
    defense: int = Byte(0x1f807)
    magic_attack: int = Byte(0x1f808)
    magic_defense: int = Byte(0x1f809)

    experience: int = Short(0x1f80a)

    weapon: str = Mapped(0x1f80c, EQUIPMENT)
    armor: int = Mapped(0x1f80d, EQUIPMENT)
    accessory: int = Mapped(0x1f80e, EQUIPMENT)

    _skills: bytearray = ByteArray(0x20423 - 0xc13, length=3)

    @property
    def name(self):
        return self.default_name if self.index else self.parent.player_name

    def display(self, *, verbose=False):
        print(f"{self.name}: L{self.level}, {self.cur_hp}/{self.max_hp} HP, {self.experience} XP")
        if verbose:
            print(f" - Equipment:  {self.weapon}, {self.armor}, {self.accessory}")
            print(f" - Attributes: {self.speed} SPD, {self.attack} ATK, {self.defense} DEF"
                  f" {self.magic_attack} MAT, {self.magic_defense} MDF")


class MarioRPGState(SaveState):
    rom_name = "Super Mario RPG"
    default_names = ["Mario", "Toadstool", "Bowser", "Geno", "Mallow"]
    argument_groups = {
        'characters': 'character stats management',
        'items': 'item and equipment management',
        'money': 'currency management'
    }
    init_options = 'all_characters',

    all_characters: bool

    cur_fp = Byte(0x1f8b1)
    max_fp = Byte(0x1f8b2)
    coins: int = Short(0x1f8af)
    frog_coins: int = Byte(0x1f8b3)
    jump_record: int = Byte(0x1f8c0)
    _player_name: bytearray = ByteArray(0x1f8b5, MAX_NAME_LENGTH)
    _characters: List[MarioCharacter] = MarioCharacter.array(length=5, step=20)
    _active_members: List[ActivePartyMember] = ActivePartyMember.array(length=3, step=0x80)
    _items: List[str] = SimpleTable(offset=0x1f882, length=MAX_ITEMS, elements=ITEMS, empty_slot=0xff)
    _equipments: List[str] = SimpleTable(offset=0x1F864, length=MAX_ITEMS, elements=EQUIPMENT, empty_slot=0xff)

    @property
    def player_name(self) -> str:
        return self._player_name.decode('ascii').strip()

    @player_name.setter
    def player_name(self, value: str):
        encoded_value = value.encode('ascii')
        if len(encoded_value) > MAX_NAME_LENGTH:
            raise ValueError(f"max length for player name is {MAX_NAME_LENGTH}")
        if len(encoded_value) < MAX_NAME_LENGTH:
            diff = MAX_NAME_LENGTH - len(encoded_value)
            padding = ' ' * diff
            encoded_value += padding
        self._player_name = bytearray(encoded_value)

    @property
    def items(self) -> Mapping[str, int]:
        return count(self._items)

    @property
    def equipment(self) -> Mapping[str, int]:
        return count(self._equipments)

    @property
    def characters(self) -> List[MarioCharacter]:
        if self.all_characters:
            return self._characters
        return [self._characters[m.character_id] for m in reversed(self._active_members) if m.character_id != 0xff]

    @classmethod
    def configure_parser(cls, parser: GroupedParser):
        parser.add_argument('-c', '--count', type=int, default=1, group='items',
                            help='add COUNT items with --add-item')
        parser.add_argument('-a', '--all-characters', action='store_true', group='characters',
                            help="display and modify all characters")

    # noinspection PyShadowingNames
    @action(group='items')
    def add_item(self, item: str, *, count: int = 1, verbose: bool = False):
        """adds item(s) to the inventory"""
        self._add_thing('item', ITEMS, item, count, verbose)

    # noinspection PyShadowingNames
    @action(group='items', short_name=None)
    def add_equipment(self, item: str, *, count: int = 1, verbose: bool = False):
        """adds item(s) to the equipment storage"""
        self._add_thing('equipment', EQUIPMENT, item, count, verbose)

    # noinspection PyShadowingNames
    def _add_thing(self, kind: str, elements: Mapping[str, int], thing: str, count: int, verbose: bool):
        storage = getattr(self, f"_{kind}s")
        if len(storage) + count > MAX_ITEMS:
            raise CommandLineError('inventory is full')
        if count < 1:
            raise CommandLineError('count must be positive')
        if thing not in elements:
            raise CommandLineError(f"unknown {kind}: {thing}")
        if verbose:
            print(f"adding {count} x {thing}")
        storage.extend([thing] * count)

    @action(group='items', name='items', modifies=False)
    def show_items(self):
        """displays items in storage"""
        for item, num in self.items.items():
            print(f"{num:2} x {item}")

    @action(group='items', name='equipment', modifies=False)
    def show_equipment(self):
        """displays extra equipment in storage"""
        for item, num in self.equipment.items():
            print(f"{num:2} x {item}")

    @action(modifies=True, group='characters')
    def heal(self, *, verbose: bool = False):
        """Heal characters and restore flower points"""
        self.cur_fp = self.max_fp
        if verbose:
            print("restored flower points")
        for c in self.characters:
            c.cur_hp = c.max_hp
            if verbose:
                print("healed", c.name)
        for m in self._active_members:
            if m.character_id < 0xff:
                m.cur_hp = m.max_hp

    @action(name='experience', group='characters')
    def add_experience(self, xp: int):
        """adds XP experience points to characters"""
        for c in self.characters:
            c.experience += xp

    @action(name='frog-coins', group='money')
    def add_frog_coins(self, coins: int):
        """adds COINS frog coins"""
        self.frog_coins += coins

    @action(name='coins', group='money')
    def add_coins(self, coins: int):
        """adds COINS coins"""
        self.coins += coins

    def display(self, verbose=False):
        print(f"Super Mario RPG: {self.coins} coins, {self.frog_coins} frog coins, {self.cur_fp}/{self.max_fp} FP")
        for c in self.characters:
            c.display(verbose=verbose)


if __name__ == '__main__':
    MarioRPGState.main()
