from typing import TypeVar, Sequence, Mapping

EQUIPMENT_IDS = {
    0x00: 'Weapon*',
    0x01: 'Armor*',
    0x02: 'Accessory*',
    0x05: 'Hammer',
    0x06: 'Froggie Stick',
    0x07: 'Nok Nok Shell',
    0x08: 'Punch Glove',
    0x09: 'Finger Shot',
    0x0A: 'Cymbals',
    0x0B: 'Chomp',
    0x0C: 'Masher',
    0x0D: 'Chomp Shell',
    0x0E: 'Super Hammer',
    0x0F: 'Hand Gun',
    0x10: 'Whomp Glove',
    0x11: 'Slap Glove',
    0x12: 'Troopa Shell',
    0x13: 'Parasol',
    0x14: 'Hurly Glove',
    0x15: 'Double Punch',
    0x16: 'Ribbit Stick',
    0x17: 'Spiked Link',
    0x18: 'Mega Glove',
    0x19: 'War Fan',
    0x1A: 'Hand Cannon',
    0x1B: 'Sticky Glove',
    0x1C: 'Ultra Hammer',
    0x1D: 'Super Slap',
    0x1E: 'Drill Claw',
    0x1F: 'Star Gun',
    0x20: 'Sonic Cymbal',
    0x21: 'Lazy Shell',
    0x22: 'Frying Pan',
    0x25: 'Shirt',
    0x26: 'Pants',
    0x27: 'Thick Shirt',
    0x28: 'Thick Pants',
    0x29: 'Mega Shirt',
    0x2A: 'Mega Pants',
    0x2B: 'Work Pants',
    0x2C: 'Mega Cape',
    0x2D: 'Happy Shirt',
    0x2E: 'Happy Pants',
    0x2F: 'Happy Cape',
    0x30: 'Happy Shell',
    0x31: 'Polka Dress',
    0x32: 'Sailor Shirt',
    0x33: 'Sailor Pants',
    0x34: 'Sailor Cape',
    0x35: 'Nautical Dress',
    0x36: 'Courage Shell',
    0x37: 'Fuzzy Shirt',
    0x38: 'Fuzzy Pants',
    0x39: 'Fuzzy Cape',
    0x3A: 'Fuzzy Dress',
    0x3B: 'Fire Shirt',
    0x3C: 'Fire Pants',
    0x3D: 'Fire Cape',
    0x3E: 'Fire Shell',
    0x3F: 'Fire Dress',
    0x40: 'Hero Shirt',
    0x41: 'Prince Pants',
    0x42: 'Star Cape',
    0x43: 'Heal Shell',
    0x44: 'Royal Dress',
    0x45: 'Super Shirt',
    0x46: 'Lazy Shell 2',
    0x4A: 'Zoom Shoes',
    0x4B: 'Safety Badge',
    0x4C: 'Jump Shoes',
    0x4D: 'Safety Ring',
    0x4E: 'Amulet',
    0x4F: 'Scrooge Ring',
    0x50: 'Exp Booster',
    0x51: 'Attack Scarf',
    0x52: 'Rare Scarf',
    0x53: "B'Tub Ring",
    0x54: 'Antidote Pin',
    0x55: 'Wake Up Pin',
    0x56: 'Fearless Pin',
    0x57: 'Trueform Pin',
    0x58: 'Coin Trick',
    0x59: 'Ghost Medal',
    0x5A: 'Jinx Belt',
    0x5B: 'Feather',
    0x5C: 'Troopa Pin',
    0x5D: 'Signal Ring',
    0x5E: 'Quartz Charm'
}

ITEM_IDS = {
    0x60: 'Mushroom',
    0x61: 'Mid Mushroom',
    0x62: 'Max Mushroom',
    0x63: 'Honey Syrup',
    0x64: 'Maple Syrup',
    0x65: 'Royal Syrup',
    0x66: 'Pick Me Up',
    0x67: 'Able Juice',
    0x68: 'Bracer',
    0x69: 'Energizer',
    0x6A: 'Yoshi-Ade',
    0x6B: 'Red Essence',
    0x6C: 'KeroKero Cola',
    0x6D: 'Yoshi Cookie',
    0x6E: 'Pure Water',
    0x6F: 'Sleepy Bomb',
    0x70: 'Bad Mushroom',
    0x71: 'Fire Bomb',
    0x72: 'Ice Bomb',
    0x73: 'Flower Tab',
    0x74: 'Flower Jar',
    0x75: 'Flower Box',
    0x76: 'Yoshi Candy',
    0x77: 'Froggie Drink',
    0x78: 'Muku Cookie',
    0x79: 'Elixer',
    0x7A: 'Megaelixer',
    0x7B: 'See Ya',
    0x7C: 'Temple Key',
    0x7D: 'Goodie Bag',
    0x7E: 'Earlier Times',
    0x7F: 'Freshen Up',
    0x80: 'Rare Frog Coin',
    0x81: 'Wallet',
    0x82: 'Cricket Pie',
    0x83: 'Rock Candy',
    0x84: 'Castle Key 1',
    0x86: 'Castle Key 2',
    0x87: 'Bambino Bomb',
    0x88: 'Sheep Attack',
    0x89: 'Carbo Cookie',
    0x8A: 'Shiny Stone',
    0x8C: 'Room Key',
    0x8D: 'Elder Key',
    0x8E: 'Shed Key',
    0x8F: "Lamb's Lure",
    0x90: 'Fright Bomb',
    0x91: 'Mystery Egg',
    0x92: 'Beetle Box 1',
    0x93: 'Beetle Box 2',
    0x94: 'Luck Jewel',
    0x96: 'Soprano Card',
    0x97: 'Alto Card',
    0x98: 'Tenor Card',
    0x99: 'Cystalline',
    0x9A: 'Power Blast',
    0x9B: 'Wilted Shroom',
    0x9C: 'Rotten Mush',
    0x9D: 'Moldy Mush',
    0x9E: 'Seed',
    0x9F: 'Fertilizer',
    0xA0: 'Waste Basket',
    0xA1: 'Big Boo Flag',
    0xA2: 'Dry Bones Flag',
    0xA3: 'Greaper Flag',
    0xA4: 'Secret Game',
    0xA6: 'Cricket Jam',
    0xAC: 'Fireworks',
    0xAE: 'Bright Card',
    0xAF: 'Mushroom X',
    0xB0: 'Star Egg',
    0xFF: 'Nothing'  # (clears any equipment/item in that slot)'
}

EQUIPMENT = {name: item_id for item_id, name in EQUIPMENT_IDS.items()}
ITEMS = {name: item_id for item_id, name in ITEM_IDS.items()}

E = TypeVar('E')


def count(things: Sequence[E]) -> Mapping[E, int]:
    d = {}
    for thing in things:
        d[thing] = d.get(thing, 0) + 1
    return d
