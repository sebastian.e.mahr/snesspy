import argparse
import logging
from typing import Sequence, Set, Tuple, Mapping

from snesedit import codecs
from snesedit.cli import action, CommandLineError, GroupedParser, option
from snesedit.editors.lufia2 import data
from snesedit.fields import ByteArray, Int24, Byte, Short, SimpleTable, String, PairwiseInventory, T, E, Array
from snesedit.savestate import SaveState, Character

OPT_GROUP_ITEMS = 'inventory management'
OPT_GROUP_PARTY = 'party and characters'
OPT_GROUP_MISC = 'other stuff'
DEFAULT_XP_RATIO = _dx = 85
MATCH_LEVELS_HELP = f"makes all characters XP catch up to {_dx}%% (or --ratio) of highest leveled character."

AC_STARTER_PACK = {
    'items': {
        'Dekar Blade': 1,
        'Undead Ring': 1,
        'Curselifter': 3,
        'Ex-Magic': 10,
        'Myth Blade': 1,
        'Hidora Rock': 1,
        'Speedy Ring': 1
    },
    'spells': ['Absorb', 'Strong', 'Stronger', 'Champion', 'Destroy', 'Trick']
}

log = logging.getLogger(__name__)


class Inventory(PairwiseInventory):
    CAPACITY = 96

    def __init__(self, offset: int):
        super().__init__(offset=offset, elements=data.ITEMS, capacity=Inventory.CAPACITY)

    def decode_entry(self, element_id: int, value: int) -> Tuple[E, int]:
        if value % 2:
            element_id += 256
        return self._elements[element_id], value // 2

    def encode_entry(self, element: T, value: int) -> Tuple[int, int]:
        b1 = element_id = data.ITEMS.index(element)
        b2 = value << 1
        if element_id & 0x100:
            b1 = element_id & 0xff
            b2 += 1
        return b1, b2


class Lufia2Character(Character):
    cur_hp = Short(0x0bbe)
    cur_mp = Short(0x0bc0)
    max_hp = Short(0x0bd2)
    max_mp = Short(0x0bd4)

    level = Byte(0x0bbb)
    experience = Int24(0x0c0c)

    spells: Set[str] = SimpleTable(0x0c43, elements=data.SPELLS, length=32, empty_slot=0xff, factory=set)

    _ip_value = Byte(0x0c69)
    name: str = String(0x0bad, size=6)

    parent: 'Lufia2State'

    @property
    def default_name(self) -> str:
        return data.DEFAULT_NAMES[self.index]

    @property
    def display_name(self) -> str:
        return self.name if self.name == self.default_name else f"{self.name} [{self.default_name}]"

    @property
    def ip_percent(self) -> int:
        return int((self._ip_value / 255) * 100)

    def heal(self):
        if self.cur_hp:  # if character is dead, setting full HP will crash
            self.cur_hp = self.max_hp
            self.cur_mp = self.max_mp
            self._ip_value = 0xff

    def display(self, *, verbose: bool = False):
        name = self.display_name if verbose else self.name
        print(f"{name}: Lvl {self.level}, {self.cur_hp}/{self.max_hp} HP, "
              f"{self.cur_mp}/{self.max_mp} MP, {self.experience} XP, IP: {self.ip_percent} %")
        if verbose and self.spells:
            print(" - Spells:", ", ".join(self.spells))

    def add_spell(self, spell: str) -> bool:
        """
        adds spell to this characters spell list, if the spell can be cast by this character
        :param spell: name of the spell to add
        :return: True, if the spell list was modified
        :raise: KeyError, if spell is not a valid spell name
        """
        character_flag = data.CHARACTER_FLAGS[self.index]
        users = data.SPELL_USERS.get(spell)
        if users & character_flag and spell not in self.spells:
            self.spells.add(spell)
            return True
        return False


class Lufia2State(SaveState):
    rom_name = "Lufia 2"
    argument_groups = {OPT_GROUP_PARTY: None, OPT_GROUP_ITEMS: None, OPT_GROUP_MISC: None}
    list_options = {
        ('item names', OPT_GROUP_ITEMS): data.NON_DUMMY_ITEMS,
        ('spell names', OPT_GROUP_PARTY): data.SPELL_NAMES
    }

    gold: int = Int24(0xa8a)
    casino_coins: int = Int24(0xb55)

    _next_ac_floor: int = Byte(0x1e696)
    active_character_ids: bytearray = ByteArray(0xa7b, 4)
    _characters = Lufia2Character.array(length=7, step=190)
    _retry_enabled: int = Byte(0x1559)
    inventory: dict[str, int] = Inventory(0x0a8d)

    # capsule monster info:
    # active CM is copied to lower memory, apparently compressed elsewhere
    # active CM:
    # EXP: 0x113E
    # cur_mp: 0x10F0 ?
    # max_mp: 0x1104 AND 0x1130
    # attack: 0x1108
    # level: 0x10ED
    _cm_id: int = Byte(0x11a3)
    _cm_level: int = Byte(0x10ed)
    _cm_xp: int = Int24(0x113E)
    _cm_next_1 = Int24(0x002d)
    _cm_next_2 = Int24(0x1F408)
    _cm_names: list[str] = Array(0x1f180, length=7, chunk_size=5, codec=codecs.FixedLengthString(length=5))

    @property
    def characters(self) -> Sequence[Lufia2Character]:
        return [self._characters[i] for i in self.active_character_ids if i != 0xff]

    @property
    def retry_enabled(self) -> bool:
        return bool(self._retry_enabled)

    @property
    def junk_items(self) -> Sequence[str]:
        keep = data.SPECIAL | data.SCENARIO | data.RING | data.GEM | data.BLUE_CHEST | data.VALUABLE | data.CONSUMABLE
        return [item for item in self.inventory if not data.get_item_flags(item) & keep]

    @property
    def cm_name(self) -> str:
        return self._cm_names[self._cm_id] if self._cm_xp else 'No CM'

    @classmethod
    def configure_parser(cls, parser: GroupedParser):
        parser.add_argument('-c', '--count', type=int, group=OPT_GROUP_ITEMS, default=1, metavar='N',
                            help='when adding items with --add-item, add N items instead of only 1')
        parser.add_argument('--ratio', type=int, group=OPT_GROUP_PARTY,
                            default=DEFAULT_XP_RATIO, metavar='R',
                            help='when matching levels, raise EXP to R%% of highest '
                                 'leveled character [default: %(default)s]')

    def display(self, verbose=False):
        print("Lufia 2:", self.gold, "Gold,", self.casino_coins, "Coins")
        for c in self.characters:
            c.display(verbose=verbose)
        if self._cm_xp:
            print(f"Capsule Monster: {self.cm_name} - Lvl {self._cm_level}, {self._cm_xp} XP,",
                  f"{self._cm_next_1} for next level")
        if verbose:
            print(self.inventory)

    @action(name='inventory', modifies=False, group=OPT_GROUP_ITEMS)
    def show_inventory(self):
        """shows the party inventory"""
        for item, amount in self.inventory.items():
            print(f"{amount:02} x {item}")

    @action(group=OPT_GROUP_PARTY)
    def heal(self):
        """heals all characters"""
        for c in self.characters:
            c.heal()

    @action(name='experience', group=OPT_GROUP_PARTY)
    def add_experience(self, xp: int):
        """adds XP to all characters' experience"""
        for c in self.characters:
            c.experience += xp

    @action(name='gold', group=OPT_GROUP_PARTY)
    def add_gold(self, gold: int):
        """adds GOLD gold to the party"""
        self.gold += gold

    @action(name='coins', group=OPT_GROUP_PARTY, short_name=None)
    def add_gold(self, coins: int):
        """adds COINS casino coins to the party"""
        self.casino_coins += coins

    @action(short_name=None, group=OPT_GROUP_PARTY)
    def add_spell(self, spell: str, *, verbose=False):
        """adds SPELL to all party members who can use it"""
        if spell not in data.SPELL_NAMES:
            raise CommandLineError('unknown spell: ' + spell)
        recipients = ', '.join([c.name for c in self.characters if c.add_spell(spell)])
        if verbose and recipients:
            print(f"added {spell} to {recipients}")

    @action(short_name=None, group=OPT_GROUP_ITEMS)
    def add_item(self, item: str, *, count: int = 1, verbose: bool = False):
        """
        adds ITEM to the inventory
        """
        if item not in data.ITEMS:
            raise CommandLineError('unknown item: ' + item)
        if len(self.inventory) >= Inventory.CAPACITY and item not in self.inventory:
            raise CommandLineError('inventory is full')
        old_count = self.inventory.get(item, 0)
        new_count = min(old_count + count, 99)
        self.inventory[item] = new_count
        if verbose:
            print(f"added {new_count - old_count} x {item}")

    @action(short_name=None, group=OPT_GROUP_MISC)
    def ac_starter_pack(self, *, verbose: bool = False):
        """
        adds some essential red-chest items and spells for the Ancient Cave
        """
        for item, count in AC_STARTER_PACK['items'].items():
            self.add_item(item, count=count, verbose=verbose)
        for spell in AC_STARTER_PACK['spells']:
            self.add_spell(spell, verbose=verbose)

    @action(short_name=None, group=OPT_GROUP_PARTY, help=MATCH_LEVELS_HELP)
    def match_levels(self, *, verbose: bool = False, ratio: int = DEFAULT_XP_RATIO):
        matched_xp = int(max([c.experience for c in self.characters]) * ratio / 100.0)
        for c in self.characters:
            added = matched_xp - c.experience
            if added > 0:
                c.experience = matched_xp
                if verbose:
                    print(f"{c.name}: added {added} XP")

    @action(short_name=None, group=OPT_GROUP_MISC)
    def cm_level_up(self, *, verbose: bool = False):
        """active capsule monster levels up after next battle"""
        added = self._cm_next_1 - 1
        self._cm_xp += added
        self._cm_next_1 = 1
        self._cm_next_2 = 1
        if verbose:
            print('added', added, 'XP to capsule monster, new value:', self._cm_xp)

    @action(group=OPT_GROUP_ITEMS, name='junk', modifies=False)
    def show_junk_items(self):
        """shows what items would be removed by --remove-junk"""
        for item in self.junk_items:
            amount = self.inventory[item]
            print(f"{amount:-2} x {item}")

    @action(group=OPT_GROUP_ITEMS, short_name=None)
    def remove_junk_items(self, *, verbose: bool = False):
        """removes non-essential items from the inventory, including generic equipment"""
        for item in self.junk_items:
            if verbose:
                amount = self.inventory[item]
                print(f"removing {amount:-2} x {item}")
            del self.inventory[item]

    @action(group=OPT_GROUP_MISC, short_name=None)
    def next_ac_floor(self, floor: int, *, verbose: bool = False):
        """Set the next floor in ancient cave when you use the steps"""
        if 1 > floor or 99 < floor:
            raise CommandLineError('floor must be between 1 and 99')
        if verbose:
            print(f"setting next AC floor: {floor}, (currently: {self._next_ac_floor})")
        self._next_ac_floor = floor

    @option(short_name=None, help=argparse.SUPPRESS)
    def ac_floor(self):
        print(f"Next AC Floor: {self._next_ac_floor}")


if __name__ == '__main__':
    Lufia2State.main()
