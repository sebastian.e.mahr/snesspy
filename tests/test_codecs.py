from typing import get_type_hints, get_args, get_origin, List, Tuple

import pytest
import snesedit.codecs as mut

T = mut.T


def test_chunk_bytes():
    _bytes = bytes(list(range(8)))

    assert list(mut.chunk_bytes(_bytes, 4)) == [b'\x00\x01\x02\x03', b'\x04\x05\x06\x07']
    with pytest.raises(ValueError, match='.*multiple of 5'):
        mut.chunk_bytes(_bytes, 5)


def test_pad():
    assert mut.pad(bytes.fromhex('11 22 33 44'), size=8) == bytes.fromhex('11 22 33 44 00 00 00 00')
    assert mut.pad([1, 2, 3, 4], size=5, padding_value=0xff) == bytes([1, 2, 3, 4, 0xff])
    assert mut.pad([1, 2, 3], size=2) == bytes.fromhex('01 02 03')
    assert mut.pad([], size=4) == bytes(4)


def _test_decoder(wrapper: mut.NumericCodec, hex_str: str, value: int):
    _bytes = bytes.fromhex(hex_str)
    assert wrapper.decode_from_bytes(_bytes) == value
    assert wrapper.encode_to_bytes(value) == _bytes


def test_numeric_codecs():
    _test_codec_hex(mut.byte,
                    ('10', 16),
                    ('20', 32),
                    ('00', 0))
    _test_codec_hex(mut.short, ('06 01', 262), ('56 83', 33622), ('00 00', 0))

    _test_codec_hex(mut.int24, ('02 00 02', 131074))
    # short path for single bytes:
    _test_codec_hex(mut.byte, ('35', 53))


def test_numeric_value_range():
    with pytest.raises(ValueError):
        mut.short.encode_to_bytes(0xffff + 1)
    with pytest.raises(ValueError):
        mut.integer.encode_to_bytes(-4)


class Foo:
    x: List[int]
    bars: list[str]

    def __init__(self, x: int, bars: list[str]):
        self.x = x
        self.bars = bars

    def baz(self):
        return self.bars * self.x


def test_annotation():
    annotations = get_type_hints(Foo)
    assert get_origin(annotations['bars']) == list
    assert get_args(annotations['bars']) == (str,)
    assert get_origin(annotations['x']) == list
    assert get_args(annotations['x']) == (int,)


def test_array_codec():
    array_value = [27, 2121, 33333]
    codec = mut.NumericArrayCodec(width=2)
    array_bites = codec.encode_to_bytes(array_value)
    assert len(array_bites) == 6
    single = [mut.short.encode_to_bytes(b) for b in array_value]
    assert array_bites[0:2] == single[0]
    assert array_bites[2:4] == single[1] == b'I\x08'
    assert array_bites[4:] == single[2] == b'5\x82'

    assert codec.decode_from_bytes(array_bites) == array_value

    _test_codec_hex(mut.NumericArrayCodec(width=1), ('35 12', [53, 18]))


def test_sbcd_codec():
    _test_codec_hex(mut.SBCDCodec(length=2),
                    ('22 01', 122),
                    ('00 00', 0))


def test_halfbyte_codec():
    _test_codec(mut.HalfByteCodec(),
                (bytes([0x12, 0x34, 0x56]), [1, 2, 3, 4, 5, 6]),
                (bytes([0x54, 0x32, 0x10]), [5, 4, 3, 2, 1, 0]),
                (bytes([0xf4, 0xe2, 0x1b]), [15, 4, 14, 2, 1, 11]),
                (bytes(0), []))
    with pytest.raises(ValueError, match='.*lists of even length'):
        mut.HalfByteCodec().encode_to_bytes([4, 6, 12])
    with pytest.raises(ValueError, match='max.*15'):
        mut.HalfByteCodec().encode_to_bytes([22, 44])


def test_get_array_codec():
    with pytest.raises(ValueError):
        mut.get_array_codec(0.4)
    with pytest.raises(ValueError):
        mut.get_array_codec(-1)
    with pytest.raises(ValueError):
        mut.get_array_codec("half bytes")
    assert isinstance(mut.get_array_codec(0.5), mut.HalfByteCodec)
    assert mut.get_array_codec(4) == mut.NumericArrayCodec(width=4)


def _test_codec_hex(codec: mut.Codec[T], *values: Tuple[str, T]):
    values = ((bytes.fromhex(enc), dec) for enc, dec in values)
    _test_codec(codec, *values)


def _test_codec(codec: mut.Codec[T], *values: Tuple[bytes, T]):
    for encoded, decoded in values:
        assert codec.decode_from_bytes(encoded) == decoded
        assert codec.encode_to_bytes(decoded) == encoded


def test_string_codec():
    _test_codec(mut.StringCodec(), (b'hello world', 'hello world'))
