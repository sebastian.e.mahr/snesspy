import sys

import pytest
from pytest_mock import MockerFixture

from snesedit import cli as mut


@pytest.fixture
def parser() -> mut.GroupedParser:
    return mut.GroupedParser()


def test_grouped_parser(parser):
    parser.add_argument('spam', type=int, nargs='?')
    specials = parser.add_argument_group(title='specials')
    parser.add_argument('--add-eggs', action='store_true', group='specials')
    specials.add_argument('--add-bacon', action='store_true')

    # test that add-egs was added to the specials group
    assert specials.get_default('add_eggs') == False

    assert parser.get_argument_group('specials') == specials
    assert parser.actions.keys() == {'help', 'spam', 'add_eggs', 'add_bacon'}

    with pytest.raises(ValueError, match='.*no argument group baz'):
        parser.add_argument('-f', '--foo', type=int, group='baz')

    args = parser.parse_args(['--add-bacon', '22'])
    assert args.spam == 22
    assert args.add_bacon


def test_list_action(capsys, mocker: MockerFixture, parser):
    parser.add_argument('name', type=str, nargs='?')
    ingredients = ['spam', 'eggs', 'bacon']
    parser.add_list_option('ingredients', items=ingredients)
    assert 'list_ingredients' in parser.actions
    mocker.patch('sys.exit')
    parser.parse_args(['--list-ingredients'])

    sys.exit.assert_called_once_with(0)

    # output is only our list entries
    output = capsys.readouterr().out
    assert output == '\n'.join(ingredients) + '\n'

    with pytest.raises(ValueError):
        parser.add_list_option(name='foo', items=None)


def test_subcommand_validation():
    with pytest.raises(mut.AnnotationError, match='.*require a type annotation'):
        class Foo:

            @mut.subcommand
            def some_method(self, no_type_anno, foo: bool):
                pass

    with pytest.raises(mut.AnnotationError, match='options.* require a default value'):
        class Bar:

            @mut.subcommand(actions=True)
            def handle_spam(self, spam: str, *, bacon: bool) -> object:
                pass

    with pytest.raises(mut.AnnotationError, match='.*must define a class as return annotation'):
        class Baz:

            @mut.subcommand(actions=True)
            def sub_command(self, foo: str, *, baz: int = 42):
                pass


def test_callbacks(parser, capsys, mocker: MockerFixture):
    class SomeState:

        def __init__(self):
            self.v_bacon = False
            self.v_spam = 0
            self.v_eggs = True

        @mut.action
        def spam(self, spam: int, *, verbose: bool = True):
            """Test defaults"""
            self.v_spam = spam

        @mut.action(modifies=False)
        def bacon(self, *, verbose=True, spam: int = 0):
            """make sure this is non-modifying"""
            self.v_bacon = True
            self.v_spam = spam

        @mut.action(short_name=None, default=True)
        def eggs(self):
            self.v_eggs = True

    parser.get_argument_group('subgroup')
    callback_list = mut.get_action_callbacks(SomeState, parser)
    callbacks = {c.name: c for c in callback_list}
    callback_names = [c.name for c in callback_list]

    # test sorting: non-modifyinf first, than alphabetically by long name
    assert callback_names == ['bacon', 'eggs', 'spam']

    spam: mut.Callback = callbacks.get('spam')
    assert spam.name == 'spam'
    assert spam.method == SomeState.spam
    assert spam.has_arg
    assert spam.options == ['verbose']
    assert not spam.is_default

    bacon: mut.Callback = callbacks.get('bacon')

    assert not bacon.modifies
    assert bacon.options == ['verbose', 'spam']

    # test short options: non modifying are lower case
    assert parser.parse_args(['-b']).bacon
    assert parser.parse_args(['--eggs']).eggs

    call_args = parser.parse_args(['--bacon', '--spam=20'])
    call_instance = SomeState()

    assert not call_instance.v_bacon
    assert call_instance.v_spam == 0

    bacon.execute(call_instance, call_args)
    assert call_instance.v_spam == 20
    assert call_instance.v_bacon

    mocker.patch('sys.exit')

    parser.parse_args(['-s', '12'])
    sys.exit.assert_called_once_with(2)

    call_instance.v_spam = 1
    call_instance.v_bacon = False
    call_instance.v_eggs = False

    # test that the default is only run when no others are called
    mut.execute_callbacks(callback_list, call_instance, call_args, execute_default=True)
    assert not call_instance.v_eggs
    assert call_instance.v_spam == 20
    assert call_instance.v_bacon

    # test we run the default call back only if no explicit callback
    call_args = parser.parse_args([])
    mut.execute_callbacks(callback_list, call_instance, call_args, execute_default=True)
    assert call_instance.v_eggs


def test_action_with_unknown_group():
    class BadState:

        @mut.action(group='baz')
        def foo(self):
            """this action specifies an unknown arg group"""

    with pytest.raises(ValueError, match='annotation on method .*foo.*unknown.*group baz'):
        mut.get_action_callbacks(BadState, mut.GroupedParser())


def test_only_one_default_callback():
    class BadState:

        @mut.action(default=True)
        def foo(self):
            pass

        @mut.action(default=True, short_name=None)
        def bar(self):
            pass

    with pytest.raises(mut.AnnotationError, match='.*multiple default callbacks.*'):
        mut.get_action_callbacks(BadState, mut.GroupedParser())


def test_inherited_args(parser: mut.GroupedParser):
    parser.add_argument('-v', '--verbose', action='store_true', inherit=True)
    subs = parser.add_subparsers()

    spam_parser: mut.GroupedParser = subs.add_parser('spam')
    spam_parser.add_argument('bacon', nargs=1)
    assert spam_parser.parent == parser
    assert spam_parser.global_args == {'verbose'}


def test_only_one_positional_argument(parser):
    class AnotherFailedState:

        def bad_action_method(self, x: int, y: int):
            return y * x

    metadata = dict(modifies=False, short_name=None)
    with pytest.raises(ValueError, match='.*at most one positional argument.*'):
        mut.Callback.for_method(AnotherFailedState.bad_action_method, metadata, parser)


def test_no_subcomand_varargs(parser):
    class SomeState3:

        @mut.subcommand(name='fooStuff')
        def foo_stuff(self, foo: int = 1, *args: str):
            """does stuff FOO times"""

    with pytest.raises(mut.AnnotationError, match='.*variadic.*'):
        dict(mut.find_subcommands(SomeState3, parser))


def test_subcommands(parser):
    class SubEntity:
        @mut.action
        def verbose(self):
            pass

    class SomeState2:

        @mut.subcommand(name='doStuff', actions=True, help={
            'foo': 'bars the foos',
            'silent': 'foos no bars'
        }, short_opts={'foo': 'f', 'baz': 'b'})
        def do_stuff(self, foo: int = 1, *, silent: bool = True, bar: bool = True, baz: str = 'BAZ') -> SubEntity:
            """does stuff FOO times"""
            return SubEntity()

    parser.add_argument('--silent', action='store_true', inherit=True)
    sub_cmds = dict(mut.find_subcommands(SomeState2, parser))

    sc = sub_cmds.get('doStuff')
    assert sc.method == SomeState2.do_stuff
    assert sc.parameter_names == ['foo', 'silent', 'bar', 'baz']
    assert sc.callbacks[0].name == 'verbose'
    assert len(sc.callbacks) == 1


def test_command_line_error(mocker: MockerFixture):
    exit_mock = mocker.patch('sys.exit')
    err = mut.CommandLineError('bad stuff happened', exit_code=127)
    err.terminate()
    exit_mock.assert_called_once_with(127)


def test_varargs_action(parser: mut.GroupedParser):
    class SomeState:

        def __init__(self):
            self.things = None

        @mut.action(name='things', short_name=None, modifies=True)
        def do_things(self, things: list[str], *, shout: bool = False):
            verb = "DOING" if shout else "doing"
            self.things = [verb + " " + thing for thing in things]

    parser.add_argument('--shout', action='store_true')
    callbacks = mut.get_action_callbacks(SomeState, parser)
    args = parser.parse_args(['--things', 'laundry', 'work', 'shopping', '--shout'])
    instance = SomeState()
    mut.execute_callbacks(callbacks, instance, args)
    assert instance.things == ["DOING laundry", "DOING work", "DOING shopping"]


class BadState:

    def fail(self):
        raise mut.CommandLineError('fail!')


def test_command_line_error_in_action(parser: mut.GroupedParser, mocker: MockerFixture):
    action = mut.Callback.for_method(BadState.fail, dict(modifies=False), parser)
    spy = mocker.patch('snesedit.cli.CommandLineError.terminate')
    action.execute(BadState(), parser.parse_args([]))
    spy.assert_called_once()


def test_command_line_error_in_subcommand(parser: mut.GroupedParser, mocker: MockerFixture):
    cmd = mut.SubCommand('fail', BadState.fail, parameter_names=[], modifies=False, callbacks=[], nested={})
    spy = mocker.patch('snesedit.cli.CommandLineError.terminate')
    cmd.run(BadState())
    spy.assert_called_once()

