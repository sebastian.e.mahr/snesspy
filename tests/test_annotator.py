import pytest

from snesedit import annotator as mut


def test_function_annotation_plain():
    foo = mut.Annotator('foo', baz=42)

    @foo
    def double(x):
        return x * 2

    # make sure the function still does what it is supposed to do
    assert double(4) == 8
    assert double('mu') == 'mumu'

    assert mut.get_annotations(double, 'foo') == {'baz': 42}


def test_function_annotation_with_args():
    bar = mut.Annotator('bar', {'spam', 'eggs'}, lobster=False)

    with pytest.raises(ValueError, match=r".*missing argument\(s\) \['eggs'\].*"):
        @bar(spam='ham')
        def func():
            pass

    @bar(spam='ham', eggs=5)
    def triple(x):
        """triple all the things"""
        return x * 3

    # make sure name and docstring are kept intact
    assert triple.__doc__ == 'triple all the things'
    assert triple.__name__ == 'triple'

    assert triple('spam ') == 'spam spam spam '
    expected = dict(spam='ham', eggs=5, lobster=False)
    assert mut.get_annotations(triple, 'bar') == expected
    assert bar.get(triple) == expected

    assert bar.annotated == [triple]
    assert list(bar.iter_annotations()) == [(triple, expected)]


def test_class_annotation():
    pets = mut.Annotator('Pets', {'amount'}, parrot='blue')
    assert pets.required == {'amount'}
    assert pets.key == 'Pets'

    @pets(dog='brown', cat='black', amount=3)
    class Shop:
        pass

    assert pets.get(Shop) == {'dog': 'brown', 'cat': 'black', 'parrot': 'blue', 'amount': 3}


def test_method_annotations():
    spam = mut.Annotator('spam', eggs=4)

    class Food:

        @spam
        def foo(self):
            return 'foo'

        @spam(eggs=5, bacon=2)
        def bar(self):
            return 'bar'

        def baz(self):
            raise NotImplementedError()

    class Mail:

        @spam(weight=3)
        def viagra(self):
            return "get cheap stuff here"

    annotations = dict(spam.methods_of(Food))
    assert Mail.viagra not in annotations
    assert Food.baz not in annotations
    assert annotations[Food.foo] == {'eggs': 4}
    assert annotations[Food.bar] == {'eggs': 5, 'bacon': 2}

    assert spam.methods_of(Mail) == [(Mail.viagra, {'eggs': 4, 'weight': 3})]
