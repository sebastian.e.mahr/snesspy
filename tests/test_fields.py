from argparse import Namespace
from typing import Type, Tuple, Union

import pytest
from pytest import fixture

import snesedit.fields as mut
import snesedit.util


@fixture
def some_data() -> bytearray:
    return bytearray.fromhex("""
    01 00 16 1F 20 1A 7C B9 91 00 00 01
    02 24 5A 18 7C 33 01 20 FE 0F 0F 02
    01 02 03 00 0A 09 12 20 ED 7C 63 01
    """)


def test_simple_fields(some_data: bytearray):
    class SomeClass:
        fiz: int = mut.Byte(offset=0)
        foo: int = mut.Short(offset=1)

    instance = SomeClass()

    fiz = mut.get_fields(SomeClass).get("fiz")
    foo = mut.get_fields(SomeClass).get("foo")

    assert foo == SomeClass.foo

    assert fiz.load(some_data, instance) == 0x01
    assert foo.load(some_data, instance) == 0x1600
    assert SomeClass.foo.annotation == int
    assert SomeClass.foo.owner == SomeClass
    assert isinstance(SomeClass.foo.codec, mut.codecs.NumericCodec)

    mut.load_instance(some_data, instance)
    assert instance.fiz == 0x01
    assert instance.foo == 0x1600
    new_data = bytearray(4)
    expected = bytearray.fromhex("02 01 16 00")
    instance.fiz += 1
    instance.foo += 1
    assert new_data != expected
    SomeClass.fiz.save(new_data, instance)
    SomeClass.foo.save(new_data, instance)
    mut.save_instance(new_data, instance)
    assert new_data == expected

    # unchanged data should not modify
    SomeClass.foo.save(bytes(new_data), instance)
    assert new_data == expected


def test_square_enix_int(some_data):
    class Square:
        size: int = mut.SquareEnixInt(0)

    assert Square.size.codec is Square.size

    square = mut.load_instance(some_data, Square())
    assert square.size == 57958

    square.size = 192918
    target = bytearray(5)
    mut.save_instance(target, square)
    assert target == bytearray.fromhex('03 00 a6 a7 00')


def test_complex_array(some_data: bytearray):
    class Repeated(mut.ArrayElement):
        x: int = mut.Byte(0)
        y: int = mut.Int24(1)
        z: list[int] = mut.ShortArray(offset=4, length=4)
        foo: int = mut.ParentField()

    class Container:
        foos: list[int] = mut.ShortArray(offset=22, length=3)
        repeated: list[Repeated] = Repeated.array(step=12, length=3)

    container = Container()
    mut.load_instance(some_data, container)
    assert len(container.repeated) == 3
    assert Container.repeated.element_type == Repeated

    assert container.repeated[0].x == 0x01
    assert container.repeated[0].y == 0x1F1600
    #  20 1A 7C B9 91 00 00 01
    assert container.repeated[0].z == [0x1A20, 0xB97C, 0x0091, 0x0100]

    # test hash
    r0 = container.repeated[0]
    assert hash(r0) == hash(r0)
    assert r0 != 'foo'
    assert r0.index == 0
    assert r0.parent == container

    new_data = bytearray(3 * 12)
    mut.save_instance(new_data, container)
    assert new_data == some_data

    # test ParentField
    assert isinstance(Repeated.foo, mut.ParentField)
    assert container.foos == [527, 513, 3]
    for i in range(3):
        assert container.foos[i] == container.repeated[i].foo

        # make sure assignment writes through to parent
        container.repeated[i].foo = 42 + i
        assert container.foos[i] == 42 + i


def test_complex_array_element_type():
    class Repeated(mut.ArrayElement):
        x: mut.Byte(offset=12)

    class InferredArrayType:
        repeated: list[Repeated] = mut.ComplexArray(12, 1)

    assert InferredArrayType.repeated.element_type == Repeated

    class MissingAnnotation:
        repeated = mut.ComplexArray(12, 2)

    with pytest.raises(TypeError, match='.*must be annotated as list.*'):
        MissingAnnotation.repeated.load(bytearray(12), object())

    class BadElementTypeAnnotation:
        repeated: list[str] = mut.ComplexArray(12, 2)

    with pytest.raises(TypeError, match='.*must be annotated.*subtype of ArrayElement'):
        BadElementTypeAnnotation.repeated.load(bytearray(12), object())


def test_bad_codec():
    class BadCodec(mut.codecs.Codec[int]):

        def decode_from_bytes(self, source: bytes) -> int:
            return 12

        def encode_to_bytes(self, value: int):
            return 12

    instance = Namespace()
    instance.foo = 42
    data = bytearray.fromhex('05 00 01')
    field = mut.SimpleField(offset=0, size=1, codec=BadCodec())
    field.__set_name__(Namespace, 'foo')
    assert field.load(data, instance) == 12
    assert instance.foo == 42

    # should not change anything:
    instance.foo = 12
    assert not field.save(data, instance)
    assert data == bytearray.fromhex('05 00 01')

    # changed value should trigger modification:
    instance.foo = 42
    with pytest.raises(TypeError):
        field.save(data, instance)


def _new_field_type(field: mut.Field[mut.T]) -> Type:
    type_name = field.__class__.__name__ + "Holder"
    return type(type_name, (), {'test_field': field})


def _test_field_with(field: mut.Field[mut.T], *values: Tuple[Union[bytearray, str], mut.T], compacts=False):
    holder_type = _new_field_type(field)
    instance = holder_type()
    for bites, value in values:
        if isinstance(bites, str):
            bites = bytearray.fromhex(bites)
        mut.load_instance(bites, instance)
        # load
        assert getattr(instance, 'test_field') == value
        new_data = bytearray(len(bites))
        mut.save_instance(new_data, instance)
        if not compacts:
            assert new_data == bites
        else:
            mut.load_instance(new_data, instance)
            assert getattr(instance, 'test_field') == value


def test_half_byte_array():
    with pytest.raises(ValueError, match='length of HalfByteArray must be even'):
        mut.HalfByteArray(0, 3)

    field = mut.HalfByteArray(0, 4)
    _test_field_with(field,
                     ('01 23', [0, 1, 2, 3]),
                     ('AB CD 00', [10, 11, 12, 13]))


def test_byte_array():
    field = mut.ByteArray(offset=0, length=3)
    assert isinstance(field.codec, mut.codecs.ByteArray)
    assert len(field) == 3

    _test_field_with(field, ('00 01 02', bytearray([0, 1, 2])))


def test_array():
    with pytest.raises(ValueError, match='codec is required.*'):
        # noinspection PyTypeChecker
        mut.Array(offset=0, chunk_size=2, length=3, codec=None)

    field = mut.Array(offset=0, chunk_size=2, length=3, codec=mut.codecs.short)
    _test_field_with(field, ('00 CB E7 45 E0 00', [51968, 17895, 224]))


def test_bitset():
    field = mut.BitSet(offset=0, elements=['spam', 'bacon', 'eggs', 'lobster', 'extra spam', 'beer', 'gin'])
    assert field.size == 1
    _test_field_with(field,
                     ('21', {'beer', 'spam'}),
                     ('07 00', {'spam', 'eggs', 'bacon'}))


def test_simple_list():
    ingredients = ['spam', 'bacon', 'eggs', 'lobster', 'extra spam', 'beer', 'gin']
    _test_field_with(mut.SimpleList(offset=0, elements=ingredients),
                     ('01 03', ['bacon', 'lobster']))

    with pytest.raises(ValueError, match='.*salmon not found.*'):
        mut.SimpleList(0, elements=ingredients, length=2, nothing='salmon')


def test_simple_table():
    ingredients = ['nothing', 'spam', 'bacon', 'eggs', 'lobster', 'extra spam', 'beer', 'gin']
    elements = {v: i for i, v in enumerate(ingredients)}
    with pytest.raises(ValueError, match='salmon not found'):
        mut.SimpleTable(0, length=5, elements=elements, nothing='salmon', empty_slot=None)

    field = mut.SimpleTable(offset=0, bytes_per_slot=1, elements=elements)
    assert field.bytes_per_slot == 1
    _test_field_with(field,
                     ('01 02 07 00', ['spam', 'bacon', 'gin']),
                     compacts=True)


def test_simple_table_no_empty_value():
    with pytest.raises(ValueError, match='.*nothing or empty slot.*'):
        mut.SimpleTable(0, elements={'spam': 2, 'bacon': 3, 'eggs': 4}, empty_slot=None)


def test_mapped():
    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        mut.Mapped(0, elements=True)

    _test_field_with(mut.Mapped(offset=0, elements={'willi': 1, 'hugo': 3, 'egon': 5, 'nada': 0}),
                     ('01', 'willi'),
                     ('FF', None))

    _test_field_with(mut.Mapped(offset=0, elements=[None, 'spam', 'bacon', 'eggs'], nothing=0),
                     ('00', None),
                     ('01', 'spam'),
                     ('03', 'eggs'))


def test_non_bijective_mapping():
    with pytest.raises(ValueError, match='.*must be a bijective mapping.*'):
        mut.Mapped(offset=0, elements=['foo', 'bar', 'foo'])

    with pytest.raises(ValueError, match='.*not a bijective mapping.*'):
        mut.Mapped(offset=0, elements={'foo': 1, 'bar': 2, 'baz': 1})


def test_positional_inventory():
    ingredients = ['spam', 'bacon', 'eggs', 'lobster', 'extra spam', 'beer', 'gin', 'snake oil']

    field = mut.PositionalInventory(offset=0, keys=ingredients, bytes_per_slot=0.5)
    assert field.bytes_per_slot == 0.5
    assert field.size == 4
    _test_field_with(field, ('01 23 00 20', {'bacon': 1, 'eggs': 2, 'lobster': 3, 'gin': 2}))

    with pytest.raises(ValueError, match='keys must be unique'):
        mut.PositionalInventory(offset=0xdead, keys=['spam', 'eggs', 'bacon', 'spam', 'spam'])


def test_parallel_arrays_inventory():
    ingredients = ['spam', 'bacon', 'eggs', 'lobster']

    field = mut.ParallelArraysInventory(keys_offset=0, values_offset=4, capacity=4,
                                        elements=ingredients, empty_value=0)
    _test_field_with(field,
                     ('03 01 02 00 0A 14 02 ff', {'lobster': 10, 'bacon': 20, 'eggs': 2}),
                     compacts=True)
    with pytest.raises(ValueError, match='not a bijective mapping.*'):
        assert mut.ParallelArraysInventory(0, 0, 4, {'a': 4, 'b': 4})


def test_unchanged_inventory_does_not_save():
    ingredients = ['spam', 'bacon', 'eggs', 'lobster']

    data = bytearray.fromhex('03 01 02 00 0A 14 02 ff')
    field = mut.ParallelArraysInventory(keys_offset=0, values_offset=4, capacity=4,
                                        elements=ingredients, empty_value=0)

    holder = _new_field_type(field)
    instance = holder()
    mut.load_instance(data, instance)
    assert instance.test_field == {'lobster': 10, 'bacon': 20, 'eggs': 2}

    # unmodified instance will not save:
    field.save(bytes(data), instance)
    # modified instance will:
    instance.test_field['lobster'] = 99
    with pytest.raises(TypeError, match="'bytes' object does not support item assignment"):
        field.save(bytes(data), instance)


def test_pairwise_inventory():
    ingredients = ['spam', 'bacon', 'eggs', 'lobster']
    field = mut.PairwiseInventory(offset=0, capacity=4, elements=ingredients, empty_value=0xff, empty_element=0xff)
    _test_field_with(field, ('00 02 03 0A 02 0F 01 22',
                             {'spam': 2, 'lobster': 10, 'bacon': 34, 'eggs': 15}),
                     compacts=True)

    _test_field_with(field, ('00 02 01 03 FF FF FF FF', {'spam': 2, 'bacon': 3}))


def test_pairwise_inventory_capacity():
    ingredients = ['spam', 'bacon', 'eggs', 'lobster']
    field = mut.PairwiseInventory(offset=0, capacity=2, elements=ingredients, empty_value=0xff, empty_element=0xff)
    holder = _new_field_type(field)
    instance = holder()

    # set to mapping with len > capacity (2)
    setattr(instance, field.name, {'spam': 99, 'bacon': 2, 'lobster': 7})
    with pytest.raises(ValueError, match='capacity.*exceeded'):
        field.save(bytearray(8), instance)


def test_string_field():
    field = mut.String(offset=0, size=8)
    assert field.encoding == 'ascii'
    _test_field_with(field,
                     ('42 61 63 6f 6e 00 00 00', 'Bacon'),
                     ('6c 6f 62 73 74 65 72 00', 'lobster'))


def test_square_enix_inventory():
    ingredients = {0: '<Nothing>', 1: 'spam', 2: 'bacon', 257: 'eggs', 258: 'lobster'}

    field = mut.SquareEnixInventory(keys_offset=0, values_offset=4, elements=ingredients, capacity=4)
    _test_field_with(field, ('01 02 01 02 0c 0f 8e 87', {'bacon': 15, 'eggs': 14, 'lobster': 7, 'spam': 12}))

    assert field.decode_entry(0x01, 0x85) == ('eggs', 5)
    with pytest.raises(ValueError):
        field.decode_entry(0x22, 10)


def test_ensure_byte():
    snesedit.util.ensure_byte(5)
    snesedit.util.ensure_byte(255)
    snesedit.util.ensure_byte(0)

    with pytest.raises(ValueError, match='foo must be a single byte.*'):
        snesedit.util.ensure_byte(-4, 'foo')

    with pytest.raises(ValueError, match='power_level must be a single byte.*'):
        snesedit.util.ensure_byte(9000, 'power_level')


def test_sparse_counters():
    labels = {'spam': 0, 'eggs': 2, 'bacon': 3}
    field = mut.SparseCounters(0, labels=labels)
    assert field.labels == labels
    assert field.length == 4

    _test_field_with(field, ('0A 00 01 12', {'spam': 10, 'eggs': 1, 'bacon': 18}))

    instance = _new_field_type(field)()
    data = bytearray.fromhex('0A FE 01 12')
    mut.load_instance(data, instance)
    assert instance.test_field == {'spam': 10, 'eggs': 1, 'bacon': 18}
    instance.test_field['spam'] = 99
    field.save(data, instance)
    assert data == bytearray.fromhex('63 FE 01 12')
    data[1] = 0
    field.save(data, instance)
    assert data == bytearray.fromhex('63 00 01 12')

    # make sure labels are bijective
    with pytest.raises(ValueError, match='.*bijective.*'):
        mut.SparseCounters(0, labels={'first': 0, 'second': 1, 'prime': 0})

    # make sure we find illegal labels
    instance.test_field['lobster'] = 32
    with pytest.raises(ValueError, match='unknown label: lobster'):
        field.save(data, instance)



def test_byte_set():
    field = mut.ByteSet(0, elements=['spam', 'bacon', 'eggs', 'lobster'])

    _test_field_with(field,
                     ('01 00 00 01', {'spam', 'lobster'}),
                     ('00 00 01 00', {'eggs'}),
                     ('00 00 00 00', set()))


def test_bcd():
    field = mut.BinaryCodedDecimal(0, size=2)
    _test_field_with(field, ('99 09', 999), ('01 11', 1101))
