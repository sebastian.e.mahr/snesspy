from snesedit.editors.top import data


def test_combo_names():
    for combo, (t1, t2) in data.TECH_COMBOS.items():
        assert combo in data.TECHNIQUES
        assert t1 in data.TECHNIQUES
        assert t2 in data.TECHNIQUES


def test_tech_levels():
    for tech in data.TECH_LEVELS:
        assert tech in data.TECHNIQUES
