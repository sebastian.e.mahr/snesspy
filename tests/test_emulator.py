import gzip
import shutil
from pathlib import Path

import pytest
import snesedit.emulator as mut
from snesedit import config


@pytest.fixture
def snes9x_state_data() -> bytearray:
    with gzip.open('tests/test.000') as f:
        return bytearray(f.read())


@pytest.mark.parametrize(('emulator_name, data_filename'), [('snes9x', 'test.000'), ('zsnes', 'test.zst')])
def test_emulator_io(monkeypatch, emulator_name: str, data_filename: str, tmp_path: Path):
    def get_save_dir(_):
        return tmp_path

    monkeypatch.setattr(config, 'get_savestate_directory', get_save_dir)
    emulator = mut.Emulator.by_name(emulator_name)
    template = Path('tests') / data_filename
    src = emulator.get_savestate_file('test', 0)
    dst = emulator.get_savestate_file('test', 1)

    shutil.copy(template, src)
    shutil.copy(template, dst)

    ram = emulator.load_slot('test', 0)
    assert len(ram) == mut.SNES_RAM_SIZE
    emulator.save_slot('test', 0, ram)
    ram = emulator.load_ram(src)
    ram2 = emulator.load_ram(dst)

    assert ram == ram2


def test_get_snes9x_block(snes9x_state_data: bytearray):
    name_start, name_length = mut.get_snes9x_block_offset(snes9x_state_data, block_name='NAM')
    filename = snes9x_state_data[name_start:name_start + name_length - 1]
    assert filename == b'C:\\Games\\SNES\\roms\\Star Ocean.smc'

    with pytest.raises(ValueError, match='block XXX not found in data'):
        mut.get_snes9x_block_offset(snes9x_state_data, 'XXX')


def test_zsnes_ext():
    assert mut.get_zsnes_ext(0) == 'zst'
    assert mut.get_zsnes_ext(1) == 'zs1'
    assert mut.get_zsnes_ext(9) == 'zs9'
    assert mut.get_zsnes_ext(66) == 'z66'
    with pytest.raises(ValueError):
        mut.get_zsnes_ext(100)
    with pytest.raises(ValueError):
        mut.get_zsnes_ext(-3)


def test_get_snes9x_slot():
    dir = Path('/etc/saves')
    assert mut.get_snes9x_savestate(dir, 'supermario', 0).name == 'supermario.000'
    assert mut.get_snes9x_savestate(dir, 'supermario', 55).name == 'supermario.055'


def test_get_sram():
    zsnes = mut.Emulator.by_name('zsnes')
    save_dir = config.get_savestate_directory('zsnes')
    assert zsnes.get_sram_file('robotrek') == save_dir / 'Robotrek.srm'


def test_get_zsnes_slot():
    zsnes = mut.Emulator.by_name('zsnes')
    assert zsnes.get_savestate_filename('supermario', 0) == 'supermario.zst'
    assert zsnes.get_savestate_filename('supermario', 55) == 'supermario.z55'
    assert zsnes.get_savestate_filename('supermario', 4) == 'supermario.zs4'


def test_for_rom(monkeypatch):
    rom_cfg = {
        'Metroid': 'zsnes',
        'Star Ocean': 'snes9x'
    }

    def mock_get_emulator(rom_name):
        return rom_cfg.get(rom_name, 'zsnes')

    monkeypatch.setattr(config, 'get_emulator', mock_get_emulator)

    assert mut.Emulator.for_rom('Metroid').name == 'zsnes'
    assert mut.Emulator.for_rom('Star Ocean').name == 'snes9x'
    assert mut.Emulator.for_rom('Yoshis Island').name == 'zsnes'
