import shutil
from pathlib import Path
from typing import Dict, Sequence
from unittest.mock import ANY

import pytest
from pytest_mock import MockerFixture

from snesedit import config
from snesedit import savestate as mut
from snesedit.cli import action, subcommand, GroupedParser
from snesedit.fields import String, PositionalInventory, Int24, Byte, ParentField

SOE_ITEMS = ['Petal', 'Nectar', 'Honey', 'Dog Biscuit', 'Wings', 'Essence', 'Pixie Dust', 'Call Bead']


class SoeCharacter(mut.Character):
    level = Byte(0x0a50)
    experience: int = Int24(0xa49)
    name: str = ParentField()

    @action(name='add-experience')
    def add_xp(self, xp: int):
        self.experience += xp


class SoEState(mut.SaveState):
    # actually testing with a savestate from Secret of Evermore
    rom_name = 'test'
    init_options = 'experimental',
    experimental: bool

    boys_name: str = String(offset=0x2210, size=0x20)  # 'ZSNES Save State'
    dogs_name: str = String(offset=0x2234, size=0x20)
    items: Dict[str, int] = PositionalInventory(0x2315, SOE_ITEMS)
    coins: int = Int24(0x0acc)

    characters: Sequence[SoeCharacter] = SoeCharacter.array(length=2, step=0x4a)

    list_options = {'items': SOE_ITEMS, ('ingredients', 'alchemy'): ['Water', 'Oil', '...']}
    argument_groups = {'alchemy': 'Alchemy Stuff'}

    @classmethod
    def configure_parser(cls, parser: GroupedParser):
        parser.add_argument('--experimental', action='store_true', help='enable experimental features')

    @property
    def names(self) -> Sequence[str]:
        return [self.boys_name, self.dogs_name]

    @action
    def fill_inventory(self):
        for item in self.items:
            self.items[item] = 99

    @subcommand(name='coins', modifies=True)
    def add_coins(self, amount: int):
        self.coins += amount

    @subcommand(actions=True)
    def character(self, name: str) -> SoeCharacter:
        for c in self.characters:
            if name == c.name:
                return c
        raise ValueError(f'unknown character: {name}')


@pytest.fixture(autouse=True)
def mock_config(monkeypatch, tmp_path):
    """
    Sets the savestate directory to a temporary path and provide a file for testing
    """

    def _mock_savestate_dir(_):
        return tmp_path

    def _mock_get_emulator(_):
        return 'zsnes'

    test_file = Path(__file__).parent / 'test.zst'
    shutil.copy(test_file, tmp_path)
    monkeypatch.setattr(config, 'get_savestate_directory', _mock_savestate_dir)
    monkeypatch.setattr(config, 'get_emulator', _mock_get_emulator)
    return tmp_path


def test_format_inventory():
    assert mut.format_inventory({}) == ''
    assert mut.format_inventory({'Sword': 2, 'Axe': 1}) == 'Sword x 2, Axe x 1'


def test_format_equipment():
    assert mut.format_equipment({}) == ''
    assert mut.format_equipment({'Bacon': 'Nice', 'Spam': 'A lot'}) == 'Bacon: Nice, Spam: A lot'


def test_heal_character():
    char = mut.Character(0, 1, None)
    char.cur_hp = 12
    char.max_hp = 99
    char.cur_mp = 1
    char.max_mp = 99
    char.heal()
    assert char.cur_mp == char.cur_hp == 99


def test_savestate_class_attributes():
    assert SoEState.get_argument_groups() == {'alchemy': 'Alchemy Stuff'}
    assert SoEState.get_list_options()['items'] == SOE_ITEMS
    assert ('ingredients', 'alchemy') in SoEState.get_list_options()


def test_savestate_load_and_save():
    state = SoEState()
    state.load(0)
    state.save(0)

    assert str(state) == 'test state 0'
    assert state.dogs_name == 'Barkspawn'
    assert state.boys_name == 'Pupsi'
    assert state.coins == 14780
    assert state.items == {'Call Bead': 20,
                           'Dog Biscuit': 5,
                           'Essence': 6,
                           'Honey': 6,
                           'Nectar': 6,
                           'Petal': 6,
                           'Pixie Dust': 5,
                           'Wings': 5}

    state.items['Call Bead'] = 10
    state.items.pop('Honey')

    state.save(slot=1, emulator_name='zsnes')

    modified_state = SoEState()
    modified_state.load(slot=1, emulator_name='zsnes')
    assert modified_state.items['Call Bead'] == 10
    assert 'Honey' not in modified_state.items


def test_cannot_save_without_loading():
    state = SoEState()
    with pytest.raises(ValueError, match='no save data has been loaded'):
        state.save(2)


def test_main(monkeypatch):
    def _intercept(parser):
        assert 'help' in parser.actions

    monkeypatch.setattr(SoEState, 'configure_parser', _intercept)

    SoEState.main(["--verbose"])


def test_default_action(mocker: MockerFixture):
    mocker.patch('snesedit.savestate.SaveState.display')
    SoEState.main([])
    SoEState.display.assert_called_once_with(verbose=False)
    SoEState.display.reset_mock()
    SoEState.main(['--verbose'])
    SoEState.display.assert_called_once_with(verbose=True)


def test_init_options(mocker: MockerFixture):
    state = SoEState(experimental=False)
    assert not state.experimental

    state = SoEState(experimental=True, ignore=32)
    assert state.experimental

    spy = mocker.spy(mut.SaveState, '__init__')
    SoEState.main(['--experimental', '--verbose'])
    spy.assert_called_once_with(ANY, experimental=True)


def test_modifying_action():
    state = SoEState()
    state.load(0)
    assert state.items['Honey'] == 6
    SoEState.main(['--fill-inventory'])
    state.load(0)
    assert state.items['Honey'] == 99


def test_subcommand():
    state = SoEState()
    state.load(0)
    assert state.coins == 14780
    SoEState.main(['coins', '220'])
    state.load(0)
    assert state.coins == 15000


def test_list_option_group_exists():
    class BadlyConfigured(mut.SaveState):
        rom_name = 'test'
        list_options = {('food', 'XXX'): ['Bacon', 'Spam', 'Eggs']}

    with pytest.raises(ValueError, match='.*unknown argument group XXX'):
        BadlyConfigured.main([])


def test_subcommand_actions():
    state = SoEState()
    state.load(0)
    assert state.character('Barkspawn').index == 1
    assert state.characters[1].experience == 66765

    SoEState.main(['character', 'Barkspawn', '--add-experience', '235'])

    state.load(0)
    assert state.character('Barkspawn').experience == 67000
