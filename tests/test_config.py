from pathlib import Path

import pytest
import snesedit.config as mut
from pytest import MonkeyPatch


@pytest.fixture
def bundled_cfg(monkeypatch: MonkeyPatch):
    def _get_bundled_cfg():
        return (Path(__file__).parent / 'snesedit.ini').absolute()

    monkeypatch.setattr(mut, 'get_config_file', _get_bundled_cfg)


def test_get_cfg_file_unix(monkeypatch):
    monkeypatch.delenv('APPDATA', raising=False)
    monkeypatch.setenv('USERPROFILE', '/home/pytest')
    monkeypatch.setenv('HOME', '/home/pytest')
    assert mut.get_config_file() == Path('/home/pytest/.config/snesedit/snesedit.ini')


def test_get_cfg_file_windows(monkeypatch):
    monkeypatch.setenv('APPDATA', 'H:/pytest/AppData/Local')
    monkeypatch.setenv('HOME', '/home/pytest')

    assert mut.get_config_file() == Path('H:/pytest/AppData/Local/snesedit/snesedit.ini')


def test_basic_config(bundled_cfg):
    assert mut.get_savestate_directory('zsnes') == Path('tests') == mut.get_savestate_directory('snes9x')
    assert mut.get_sram_directory('zsnes') == mut.get_savestate_directory('zsnes')
    assert mut.get_sram_directory('snes9x') == Path('tests') / 'sram'
    assert mut.get_default_slot() == 0
    assert mut.get_default_emulator() == 'snes9x'
    # test filename override
    assert mut.get_savestate_basename('ff3') == 'Final Fantasy III'

    # test filename default
    assert mut.get_savestate_basename('No Such Game') == 'No Such Game'

    # test emulator override
    assert mut.get_emulator('Star Ocean') == 'snes9x'
    assert mut.get_savestate_basename('Star Ocean') == 'StarOcean'

    # test emulator default
    assert mut.get_emulator('Super Mario Cart') == mut.get_default_emulator()
