import pytest

import snesedit.util as mut


def test_highest_bit():
    assert mut.highest_bit(13) == 3
    assert mut.highest_bit(255) == 7
    assert mut.highest_bit(0) == 0
    with pytest.raises(ValueError):
        mut.highest_bit(-3)


def test_count_bits_set():
    assert mut.count_bits_set(0) == 0
    assert mut.count_bits_set(0x01) == 1
    assert mut.count_bits_set(0x11) == 2


def test_ensure_byte():
    mut.ensure_byte(0)
    mut.ensure_byte(100)
    mut.ensure_byte(255)
    with pytest.raises(ValueError):
        mut.ensure_byte(256)
    with pytest.raises(ValueError):
        mut.ensure_byte(1000)
    with pytest.raises(ValueError):
        mut.ensure_byte(-1)


def test_invert():
    bijective = {'spam': 'foo', 'eggs': 'bar'}
    injective = bijective | {'bacon': 'foo'}
    assert mut.invert(bijective) == {'bar': 'eggs', 'foo': 'spam'}
    assert mut.invert(injective, bijective=False) == {'bar': 'eggs', 'foo': 'bacon'}
    with pytest.raises(ValueError):
        mut.invert(injective, bijective=True)
