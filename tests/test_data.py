from io import StringIO

import yaml

from snesedit import data as mut


def test_load_data():
    data = mut.load_editor_data('terranigma')
    assert 'rings' in data


def test_offset_mapping():
    data = mut.load_editor_data('terranigma')
    weapons = data.get('weapons')
    assert weapons[0x80] == 'Hex Rod'
    assert weapons[0x96] == 'X-Spear'
    assert len(weapons) == 21
    assert 0x8A not in weapons
    assert 'DUMMY' not in weapons.values()


def test_load_inverted():
    yaml_data = """items: !Inverted
    Hero Armor: 0x80
    Pro Armor: 0x81
    Sea Mail: 0x82
    Elle Cape: 0x83    
    """
    data = yaml.safe_load(yaml_data)
    items = data['items']
    assert items == {
        0x80: 'Hero Armor',
        0x81: 'Pro Armor',
        0x82: 'Sea Mail',
        0x83: 'Elle Cape'
    }


def test_dump_inverted():
    items = {'spam': 42, 'lobster': 23, 'bacon': 5}
    inverted_items = mut.Inverted(items)
    output = yaml.safe_dump(inverted_items, sort_keys=False)
    assert output == "\n".join(["!Inverted", "42: spam", "23: lobster", "5: bacon", ""])


def test_dump_module():
    import somedata
    stream = StringIO()
    mut.dump_module(somedata, 'ages', 'names', stream=stream)
    result = stream.getvalue()
    assert result == """ages:
  willi: 12
  hansi: 13
names:
- willi
- hansi
- susi
"""
