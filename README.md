# savestate editors made easy

This project was started long ago as a way to learn more about the python data model,
specifically data descriptors. It has developed into a library to develop CLI
savestate editors for emulators, although you could use it for any case where 
you have to deal with only a few certain bytes of a binary file.

## The SaveState class and offset fields

This class handles attributes that map to addresses in the consoles memory
and provides an entry point for CLI scripts:

```
from snesedit.fields import Byte, Short
from snesedit.cli import action
from snesedit.savestate import SaveState


class MyGameState(Savestate):

    rom_name = 'mygame'  # look for savestates in the emulator with this base name
    
    # define fields to display and modify

    lives: int = Byte(0x0F12)  # specifiy memory adress for lives
    score: int = Short(0x0F16)

    
    # add mutator methods that map to CLI options; 
    # this one maps to -A / --add-lives
    @action(metavar='N')
    def add_lives(lives: int):
        """
        add N lives to player
        """
        self.lives += lives


if __name__ == '__main__':
    MyGameState.main()
```

You can invoke the scripts without arguments to display the lives and score fields for the default savestate

### Module Contents
* Emulator and Savestate Path Configuration
* Access to binary fields by memory location
* conversion to and from different data types including complex structures, see fields module
* a CLI helper to define modifiers that get invoked by passing command line options
* game data definition in YAML
* probably more


